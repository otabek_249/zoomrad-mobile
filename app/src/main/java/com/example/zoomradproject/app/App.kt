package com.example.zoomradproject.app

import android.app.Application
import com.example.zoomradproject.data.pref.MyPref
import dagger.hilt.android.HiltAndroidApp


@HiltAndroidApp
class App :Application(){
    override fun onCreate() {
        super.onCreate()
        MyPref.init(this)
    }
}