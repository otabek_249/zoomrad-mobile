package com.example.zoomradproject.di


import com.example.zoomradproject.ui.screen.add_card.AddCardDirection
import com.example.zoomradproject.ui.screen.add_card.AddCardDirectionImpl
import com.example.zoomradproject.ui.screen.auth.log_in.LogInDirection
import com.example.zoomradproject.ui.screen.auth.log_in.LogInDirectionImpl
import com.example.zoomradproject.ui.screen.auth.password.PasswordDirection
import com.example.zoomradproject.ui.screen.auth.password.PasswordDirectionImpl
import com.example.zoomradproject.ui.screen.auth.password2.Password2Direction
import com.example.zoomradproject.ui.screen.auth.password2.Password2DirectionImpl
import com.example.zoomradproject.ui.screen.auth.sign_up.SignUpDirection
import com.example.zoomradproject.ui.screen.auth.sign_up.SignUpDirectionImpl
import com.example.zoomradproject.ui.screen.auth.verify.VerifyDirection
import com.example.zoomradproject.ui.screen.auth.verify.VerifyDirectionImpl
import com.example.zoomradproject.ui.screen.card_vefication.VerifyCardDirection
import com.example.zoomradproject.ui.screen.card_vefication.VerifyCardDirectionImpl
import com.example.zoomradproject.ui.screen.cards.CardsDirection
import com.example.zoomradproject.ui.screen.cards.CardsDirectionImpl
import com.example.zoomradproject.ui.screen.cardsettings.CardSettingsDirection
import com.example.zoomradproject.ui.screen.cardsettings.CardSettingsDirectionImpl
import com.example.zoomradproject.ui.screen.language.LanguageDirection
import com.example.zoomradproject.ui.screen.language.LanguageDirectionImpl
import com.example.zoomradproject.ui.screen.main.pages.help.HelpDirection
import com.example.zoomradproject.ui.screen.main.pages.help.HelpDirectionImpl
import com.example.zoomradproject.ui.screen.main.pages.home.HomeDirection
import com.example.zoomradproject.ui.screen.main.pages.home.HomeDirectionImpl
import com.example.zoomradproject.ui.screen.main.pages.more.MoreDirection
import com.example.zoomradproject.ui.screen.main.pages.more.MoreDirectionImpl
import com.example.zoomradproject.ui.screen.main.pages.payment.PaymentDirection
import com.example.zoomradproject.ui.screen.main.pages.payment.PaymentDirectionImpl
import com.example.zoomradproject.ui.screen.main.pages.transfer.TransferDirection
import com.example.zoomradproject.ui.screen.main.pages.transfer.TransferDirectionImpl
import com.example.zoomradproject.ui.screen.map.branch.BranchDirection
import com.example.zoomradproject.ui.screen.map.branch.BranchDirectionImpl
import com.example.zoomradproject.ui.screen.monitoring.MonitoringDirection
import com.example.zoomradproject.ui.screen.monitoring.MonitoringDirectionImpl
import com.example.zoomradproject.ui.screen.monitoring.TestPaginationSource
import com.example.zoomradproject.ui.screen.profile.ProfileDirection
import com.example.zoomradproject.ui.screen.profile.ProfileDirectionImpl

import com.example.zoomradproject.ui.screen.settings.main.SettingsDirection
import com.example.zoomradproject.ui.screen.settings.main.SettingsDirectionImpl
import com.example.zoomradproject.ui.screen.splash.SplashDirection
import com.example.zoomradproject.ui.screen.splash.SplashDirectionImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface DirectionModule {

    @[Binds]
    fun singUpDirection(impl: SignUpDirectionImpl): SignUpDirection

    @[Binds]
    fun passwordDirection(impl: PasswordDirectionImpl): PasswordDirection

    @[Binds]
    fun logInDirection(impl: LogInDirectionImpl): LogInDirection

    @[Binds]
    fun splashDirection(impl: SplashDirectionImpl): SplashDirection


    @[Binds]
    fun verifyDirection(impl: VerifyDirectionImpl): VerifyDirection

    @[Binds]
    fun languageDirection(impl: LanguageDirectionImpl): LanguageDirection


    @[Binds]
    fun helpPageDirection(impl: HelpDirectionImpl): HelpDirection


    @[Binds]
    fun mapBranchDirection(impl: BranchDirectionImpl): BranchDirection

    @[Binds]
    fun homeBranchDirection(impl: HomeDirectionImpl): HomeDirection

    @[Binds]
    fun addCardDirection(impl: AddCardDirectionImpl): AddCardDirection

    @[Binds]
    fun moreDirection(impl: MoreDirectionImpl): MoreDirection


    @[Binds]
    fun transferDirection(impl: TransferDirectionImpl): TransferDirection


    @[Binds]
    fun paymentDirection(impl: PaymentDirectionImpl): PaymentDirection
    @[Binds]
    fun cardsDirection(impl:CardsDirectionImpl):CardsDirection

    @[Binds]
    fun settingsDirection(impl:SettingsDirectionImpl):SettingsDirection

    @[Binds]
    fun verifyCardDirection(impl:VerifyCardDirectionImpl):VerifyCardDirection


    @[Binds]
    fun monitoringDirection(impl:MonitoringDirectionImpl):MonitoringDirection

    @[Binds]
    fun settingCardDirection(impl:CardSettingsDirectionImpl):CardSettingsDirection

    @[Binds]
    fun profileDirection(iml:ProfileDirectionImpl):ProfileDirection

   






}