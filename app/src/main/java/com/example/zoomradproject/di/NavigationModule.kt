package com.example.zoomradproject.di

import com.example.zoomradproject.utils.navigation.AppNavigator
import com.example.zoomradproject.utils.navigation.NavigationDispatcher
import com.example.zoomradproject.utils.navigation.NavigationHandler
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface NavigationModule {

    @Binds
    fun bindAppNavigator(impl: NavigationDispatcher): AppNavigator

    @Binds
    fun bindNavigationHandler(impl: NavigationDispatcher): NavigationHandler
}


