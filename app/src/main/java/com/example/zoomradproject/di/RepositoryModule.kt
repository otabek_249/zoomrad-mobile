package com.example.zoomradproject.di

import com.example.zoomradproject.repository.auth.AuthRepository
import com.example.zoomradproject.repository.auth.impl.AuthRepositoryImpl
import com.example.zoomradproject.repository.main.AppRepository
import com.example.zoomradproject.repository.main.impl.AppRepositoryImpl
import com.example.zoomradproject.ui.screen.monitoring.MonitoringPagingSource
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent



@Module
@InstallIn(SingletonComponent::class)
interface RepositoryModule {

    @Binds
    fun getAuthRepository(impl: AuthRepositoryImpl) : AuthRepository

    @Binds
    fun getAppRepository(impl:AppRepositoryImpl):AppRepository

}
