package com.example.zoomradproject.repository.auth.impl

import com.example.zoomradproject.data.pref.MyPref
import com.example.zoomradproject.data.remote.auth_api.MobileApi
import com.example.zoomradproject.data.remote.request.LogInRequest
import com.example.zoomradproject.data.remote.request.SignUpData
import com.example.zoomradproject.data.remote.request.VerificationData
import com.example.zoomradproject.data.remote.response.ErrorResponse
import com.example.zoomradproject.repository.auth.AuthRepository
import com.example.zoomradproject.utils.myLog
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject


class AuthRepositoryImpl @Inject constructor(
    private val mobileApi: MobileApi,
    private val gson: Gson
) : AuthRepository {
    private val myPref  = MyPref.getInstance()


    override fun logIn(logInRequest: LogInRequest): Flow<Result<String>> = flow {
        val response = mobileApi.logIn(logInRequest)

        if (response.isSuccessful && response.body() != null) {
            emit(Result.success(response.body().toString()))
            response.body().toString().myLog()
            response.body()?.token?.myLog()
            myPref.setTokeFirst(response.body()!!.token)


        } else {
            val data = gson.fromJson(response.errorBody()!!.string(), ErrorResponse::class.java)
            emit(Result.failure(Exception(data.error)))
            data.error.myLog()
        }
    }.flowOn(Dispatchers.IO)
        .catch {
            emit(Result.failure(Exception("Unknown exception try catch")))

        }

    override fun signUp(signUpData: SignUpData): Flow<Result<String>> = flow<Result<String>> {
        val response = mobileApi.signUp(signUpData)
        if (response.isSuccessful && response.body() != null) {
            emit(Result.success(response.body().toString()))
            response.body()?.token?.myLog()
            myPref.setTokeFirst(response.body()!!.token)
            myPref.setFirstName(signUpData.firstName)
            myPref.setLastName(signUpData.lastName)
        } else {
            val data = gson.fromJson(response.errorBody()!!.string(), ErrorResponse::class.java)
            emit(Result.failure(Exception(data.error)))
            data.error.myLog()
        }
    }.flowOn(Dispatchers.IO)
        .catch {
            emit(Result.failure(Exception("Unknown exception try catch")))

        }

    override fun verification(verificationData: VerificationData): Flow<Result<String>> = flow<Result<String>> {
        val response = mobileApi.verification(verificationData)
        if(response.isSuccessful && response.body()!=null){
            emit(Result.success(response.body().toString()))
            myPref.setAccessToke(response.body()!!.accessToken)
            myPref.setRefreshToken(response.body()!!.refreshToken)
        }
        else{
            val data = gson.fromJson(response.errorBody()!!.string(), ErrorResponse::class.java)
            emit(Result.failure(Exception(data.error)))
            data.error.myLog()
        }
    }.flowOn(Dispatchers.IO)
        .catch {
            emit(Result.failure(Exception("Unknown exception try catch")))

        }

    override fun verificationSignUp(verificationData: VerificationData): Flow<Result<String>> = flow<Result<String>> {
        val response = mobileApi.verificationSignUp(verificationData)
        if(response.isSuccessful && response.body()!=null){
            emit(Result.success(response.body().toString()))
            myPref.setAccessToke(response.body()!!.accessToken)
            myPref.setRefreshToken(response.body()!!.refreshToken)
        }
        else{
            val data = gson.fromJson(response.errorBody()!!.string(), ErrorResponse::class.java)
            emit(Result.failure(Exception(data.error)))
            data.error.myLog()
        }
    }.flowOn(Dispatchers.IO)
        .catch {
            emit(Result.failure(Exception("Unknown exception try catch")))
        }
}