package com.example.zoomradproject.repository.auth

import com.example.zoomradproject.data.remote.request.LogInRequest
import com.example.zoomradproject.data.remote.request.SignUpData
import com.example.zoomradproject.data.remote.request.VerificationData
import kotlinx.coroutines.flow.Flow

interface AuthRepository {
    fun  logIn(logInRequest: LogInRequest) :Flow<Result<String>>

    fun signUp(signUpData: SignUpData) :Flow<Result<String>>
    fun verification(verificationData: VerificationData):Flow<Result<String>>
    fun verificationSignUp(verificationData: VerificationData):Flow<Result<String>>


}