package com.example.zoomradproject.repository.refreshtoken

import com.example.zoomradproject.data.pref.MyPref
import com.example.zoomradproject.data.remote.auth_api.MobileApi
import com.example.zoomradproject.data.remote.request.BodyRefresh
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import javax.inject.Provider

class AuthInterceptor (
    private val apiProvider: Provider<MobileApi>
) : Interceptor {

    private val pref = MyPref.getInstance()
    private fun refreshToken(chain: Interceptor.Chain, request: Request): Response {
        val response = apiProvider.get().refreshAccess(BodyRefresh(pref.getRefreshToken())).execute()
        if (response.isSuccessful) {
            val access = response.body()?.accessToken
            val refresh = response.body()?.refreshToken
            pref.setRefreshToken(refresh.toString())
            pref.setAccessToke(access.toString())

            val newRequest = request.newBuilder()
                .removeHeader("Authorization")
                .addHeader("Authorization", "Bearer $access")
                .build()
            return chain.proceed(newRequest)
        }
        return chain.proceed(request)
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val token = pref.getAccessToken()
        val request = chain.request().newBuilder()
            .addHeader("Authorization", "Bearer $token")
            .build()

        val response = chain.proceed(request)

        if (response.code == 401) {
            response.close()
            return refreshToken(chain, request)
        }
        return response
    }
}
