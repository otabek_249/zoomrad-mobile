package com.example.zoomradproject.repository.main

import androidx.paging.PagingData
import com.example.zoomradproject.data.model.MarkerData
import com.example.zoomradproject.data.model.Transaction
import com.example.zoomradproject.data.remote.request.AddCardData
import com.example.zoomradproject.data.remote.request.GetOwnerByPanRequest
import com.example.zoomradproject.data.remote.request.TransferDataRequest
import com.example.zoomradproject.data.remote.request.UpDateCard
import com.example.zoomradproject.data.remote.request.VerificationData
import com.example.zoomradproject.data.remote.response.CardsResponse
import com.example.zoomradproject.data.remote.response.ErrorResponse
import com.example.zoomradproject.data.remote.response.GetOwnerByPanResponse
import com.example.zoomradproject.data.remote.response.MonitoringResponse
import com.example.zoomradproject.data.remote.response.TokenInResponse
import com.example.zoomradproject.data.remote.response.VerifyTransferResponse
import kotlinx.coroutines.flow.Flow

interface AppRepository {

    fun getMapsData(): Flow<Result<Map<Int, List<MarkerData>>>>

    fun addCardData(cardData: AddCardData): Flow<Result<String>>

    fun getCards(): Flow<Result<List<CardsResponse>>>

    fun getOwnerByPan(data: GetOwnerByPanRequest): Flow<Result<GetOwnerByPanResponse>>

    fun transferCardByPan(data: TransferDataRequest): Flow<Result<TokenInResponse>>

    fun transferCardByPanVerify(data: VerificationData): Flow<Result<VerifyTransferResponse>>

    fun getHistory(): Flow<Result<MonitoringResponse>>

    suspend fun getHistories(page: Int, limit: Int): MonitoringResponse


  fun getTransferHistory(size: Int, currentPage: Int): Flow<PagingData<Transaction>>

  fun deleteCard(id:String):Flow<Result<String>>


  fun update(card:UpDateCard):Flow<Result<ErrorResponse>>



}