package com.example.zoomradproject.repository.main.impl

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.example.zoomradproject.data.model.MarkerData
import com.example.zoomradproject.data.model.Transaction
import com.example.zoomradproject.data.remote.auth_api.MobileApi
import com.example.zoomradproject.data.remote.request.AddCardData
import com.example.zoomradproject.data.remote.request.GetOwnerByPanRequest
import com.example.zoomradproject.data.remote.request.TransferDataRequest
import com.example.zoomradproject.data.remote.request.UpDateCard
import com.example.zoomradproject.data.remote.request.VerificationData
import com.example.zoomradproject.data.remote.response.CardsResponse
import com.example.zoomradproject.data.remote.response.ErrorResponse
import com.example.zoomradproject.data.remote.response.GetOwnerByPanResponse
import com.example.zoomradproject.data.remote.response.MonitoringResponse
import com.example.zoomradproject.data.remote.response.TokenInResponse
import com.example.zoomradproject.data.remote.response.VerifyTransferResponse
import com.example.zoomradproject.repository.main.AppRepository
import com.example.zoomradproject.ui.screen.monitoring.TestPaginationSource
import com.example.zoomradproject.utils.myLog
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class AppRepositoryImpl @Inject constructor(
    private val mobileApi: MobileApi,
    private val gson: Gson
): AppRepository {

    val mapList  = HashMap<Int,List<MarkerData>>()

    init {
        mapList[0] = branchList
        mapList[1] = centersKHK
        mapList[2] = atmList
        mapList[3] = separatePoint
    }

    override fun getMapsData(): Flow<Result<Map<Int, List<MarkerData>>>> = flow {
        emit(Result.success(mapList))
    }




    override fun addCardData(cardData: AddCardData): Flow<Result<String>> = flow {
        "reposotory add card is work".myLog()
        val response = mobileApi.addCard(cardData)
        if(response.isSuccessful && response.body()!= null){
            emit(Result.success(response.body()!!.msg))
        }
        else{
            val  data = gson.fromJson(response.errorBody().toString(),ErrorResponse::class.java)
            emit(Result.failure(Exception(data.error)))
        }
    }.flowOn(Dispatchers.IO)
        .catch {

            emit(Result.failure(Exception("Unknown exception try catch")))
        }
    override fun deleteCard(id: String): Flow<Result<String>> = flow {
        val response = mobileApi.deleteCard(id)
        if(response.isSuccessful && response.body()!=null){
            emit(Result.success(response.body()!!.error))
        }
        else{
            val  data = gson.fromJson(response.errorBody().toString(),ErrorResponse::class.java)
            emit(Result.failure(Exception(data.error)))
        }
    }.flowOn(Dispatchers.IO)
        .catch {
            emit(Result.failure(Exception("Unknown exception try catch")))
        }

    override fun getCards(): Flow<Result<List<CardsResponse>>> = flow {
        "app repository is work ".myLog()
        val response  = mobileApi.getAllCards()
        response.body()!!.size.toString().myLog()

        if(response.isSuccessful && response.body()!=null){
            emit(Result.success(response.body()!!))
            response.body()!!.size.toString().myLog()

        }
        else{
            val data = gson.fromJson(response.errorBody().toString(),ErrorResponse::class.java)
            emit(Result.failure(Exception(data.error)))
        }

    }.flowOn(Dispatchers.IO)
        .catch {
            emit(Result.failure(Exception("Unknown exception try catch")))
        }

    override fun getOwnerByPan(data: GetOwnerByPanRequest): Flow<Result<GetOwnerByPanResponse>> = flow{
        val response = mobileApi.getOwnerByPan(data)
        if(response.isSuccessful && response.body()!=null){
            emit(Result.success(response.body()!!))
        }
        else{
            val data = gson.fromJson(response.errorBody().toString(),ErrorResponse::class.java)
            emit(Result.failure(Exception(data.error)))
        }
    }.flowOn(Dispatchers.IO)
        .catch {
            emit(Result.failure(Exception("Unknown exception try catch")))
        }

    override fun transferCardByPan(data: TransferDataRequest): Flow<Result<TokenInResponse>>  = flow{
        val response = mobileApi.transferMoney(data)
        if(response.isSuccessful && response.body()!=null){
            emit(Result.success(response.body()!!))
        }
        else{
            val data = gson.fromJson(response.errorBody().toString(),ErrorResponse::class.java)
            emit(Result.failure(Exception(data.error)))
        }
    }.flowOn(Dispatchers.IO)
        .catch {
            emit(Result.failure(Exception("Unknown exception try catch")))
        }

    override fun transferCardByPanVerify(data: VerificationData): Flow<Result<VerifyTransferResponse>>  = flow{
        val response = mobileApi.transferVerify(data)
        if(response.isSuccessful && response.body()!=null){
            emit(Result.success(response.body()!!))
        }
        else{
            val data = gson.fromJson(response.errorBody().toString(),ErrorResponse::class.java)
            emit(Result.failure(Exception(data.error)))
        }
    }.flowOn(Dispatchers.IO)
        .catch {
            emit(Result.failure(Exception("Unknown exception try catch")))
        }

    override fun getHistory(): Flow<Result<MonitoringResponse>> = flow {
        val response=mobileApi.getHistory(size = 10, currentPage = 1)
        if(response.isSuccessful && response.body()!=null){
            emit(Result.success(response.body()!!))
        }
        else{
            val data = gson.fromJson(response.errorBody().toString(),ErrorResponse::class.java)
            emit(Result.failure(Exception(data.error)))
        }
    }.flowOn(Dispatchers.IO)
        .catch {
            emit(Result.failure(Exception("Unknown exception try catch")))
        }

    override suspend fun getHistories(page: Int, limit: Int): MonitoringResponse =mobileApi.getTransactions(size = limit, page = page)


    override fun getTransferHistory(size: Int, currentPage: Int): Flow<PagingData<Transaction>> =
        Pager(
            config = PagingConfig(size), pagingSourceFactory = { TestPaginationSource(api = mobileApi) },
        ).flow


    override fun update(card: UpDateCard): Flow<Result<ErrorResponse>>  = flow{
        val response = mobileApi.updateCard(card)
        if(response.isSuccessful && response.body()!= null){
            emit(Result.success(response.body()!!))
        }
        else{
            val data = gson.fromJson(response.errorBody().toString(),ErrorResponse::class.java)
            emit(Result.failure(Exception(data.error)))
        }
    }.flowOn(Dispatchers.IO)
        .catch {
            emit(Result.failure(Exception("Unknown exception try catch")))
        }



}

















private val branchList = arrayListOf<MarkerData>(
    MarkerData(name = "Amliyot boshqarmasi", location = "Toshkent shahri, Mirobod tumani,Amir Temur shoh ko'chasi" ,lat= 41.31136895954227, lng =69.27787744866806),
    MarkerData(lat =41.29565600570065, lng=69.28219468905212, location = "Toshkent shahri, Shayxontohur tumani,Navoiy  ko'chasi ,27-uy", name = "Mirobod filiali"),
    MarkerData(lat=41.32348757453901, lng =69.2404902841117, location = "Shayxontohur tumani,A.Navoiy  ko'chasi ,40-uy", name = "Shayxontoxur"),
    MarkerData(lat=41.32348757453901, lng =69.2404902841117, location = "Toshkent shari,Mirzo Ulug'bek tumani,Muminov  ko'chasi ", name = "IT-Park"),
    MarkerData(lat=37.23784519551908, lng=67.29301889891771, location = "Surxondaryo viloyati,Termiz shahri,Alisher Navoiy ko'chasi ", name = "Surdandaryo filiali"),
    MarkerData(lat=38.83849247499948, lng =65.80218783945786, location = "Qashqadaryo viloyati,Qarshi shahri,Mustaqillik shohko'chasi ", name = "Qashqadaryo filiali"),
)
private val centersKHK = arrayListOf(
    MarkerData(name = "Ortachirchiq KXKM", location = "Toshkent viloyati, O'rtachirchiq tumani,Bektemir shoh ko'chasi,217-uy" ,lat =41.220472, lng =69.334760),
    MarkerData(name = "Ortachirchiq KXKM", location = "Toshkent viloyati, O'rtachirchiq tumani,Bektemir shoh ko'chasi,217-uy" ,lat =41.220472, lng =69.334760),
    MarkerData(name = "Ortachirchiq KXKM", location = "Toshkent viloyati, O'rtachirchiq tumani,Bektemir shoh ko'chasi,217-uy" ,lat =41.220472, lng =69.334760),
    MarkerData(name = "Ortachirchiq KXKM", location = "Toshkent viloyati, O'rtachirchiq tumani,Bektemir shoh ko'chasi,217-uy" ,lat =41.220472, lng =69.334760),
    MarkerData(name = "Ortachirchiq KXKM", location = "Toshkent viloyati, O'rtachirchiq tumani,Bektemir shoh ko'chasi,217-uy" ,lat =41.220472, lng =69.334760),
)
private val atmTime ="Ish vaqti:Sutka davomida \nDam olish kunlari:Dam olish kunisiz"
private val atmList = arrayListOf(
    MarkerData(location = "Toshkent viloyati A373 Oxangaron shossesi 24/7 'AloqaBank'butka,", time = atmTime , lat = 41.32293060354723, lng =  69.31899660790015),
    MarkerData( location = "Mirzo Ulug'bek shoh ko'chasi 19, Тоshkent,  Узбекистан" , time = atmTime,lat =41.220472, lng =69.334760),
    MarkerData( location = "Амир Темур шоҳ кўчаси 4, 100047, Тоshkent, Узбекистан" , time = atmTime, lat = 41.314069167175866, lng =  69.27647670980224),
    MarkerData(location = "21, Farkhad Street 10, 100126, Tashkent, Узбекистан", time = atmTime ,lat =41.29876672365141, lng = 69.17871149502766),
)
private val separatePoint = arrayListOf(
    MarkerData(location = "Toshkent viloyati A373 Oxangaron shossesi 24/7 'AloqaBank'butka,", time = atmTime , lat = 41.32293060354723, lng =  69.31899660790015),
    MarkerData( location = "Mirzo Ulug'bek shoh ko'chasi 19, Тоshkent,  Узбекистан" , time = atmTime,lat =41.220472, lng =69.334760),
    MarkerData( location = "Амир Темур шоҳ кўчаси 4, 100047, Тоshkent, Узбекистан" , time = atmTime, lat = 41.314069167175866, lng =  69.27647670980224),
    MarkerData(location = "21, Farkhad Street 10, 100126, Tashkent, Узбекистан", time = atmTime ,lat =41.29876672365141, lng = 69.17871149502766),
)
