package com.example.zoomradproject.repository

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.example.zoomradproject.data.pref.MyPref
import com.example.zoomradproject.data.remote.auth_api.MobileApi
import com.example.zoomradproject.data.remote.request.BodyRefresh
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.runBlocking
import okhttp3.Authenticator
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Provider

class AuthInterceptor2
@Inject constructor(
    @ApplicationContext context: Context,
) : Authenticator {
    private val retrofit = Retrofit.Builder().baseUrl("http://195.158.16.140/mobile-bank/")
        .addConverterFactory(GsonConverterFactory.create()).client(
            OkHttpClient.Builder().addInterceptor(ChuckerInterceptor(context = context)).build()
        ).build()
    private val myPref = MyPref.getInstance()
    private val refreshApi = retrofit.create(MobileApi::class.java)


    @SuppressLint("LogNotTimber")
    override fun authenticate(route: Route?, response: Response): Request? {

        Log.d("TTT", "islaydimi  token${response.code}")

        return runBlocking {
            val responseRefresh = refreshApi.refreshToken(
                BodyRefresh(myPref.getRefreshToken().toString())
            )
            Log.d("TTT", "islaydimi${responseRefresh}")

            if (responseRefresh.isSuccessful && responseRefresh.code() == 200) {
                Log.d("TTT", "islaydimi  ifni ichi${response.code}")
                val data = responseRefresh.body()!!
                myPref.setAccessToke(data.accessToken)
                myPref.setRefreshToken(data.refreshToken)
                return@runBlocking response.request.newBuilder().removeHeader("Authorization")
                    .header(
                        "Authorization", "Bearer ${myPref.getAccessToken()}"
                    ).build()
            } else null
        }
    }
}
