package com.example.zoomradproject.data.remote.response

import com.example.zoomradproject.data.model.Transaction
import com.google.gson.annotations.SerializedName

data class MonitoringResponse(
    @SerializedName("total-elements")
    val totalElements: Int,
    @SerializedName("total-pages")
    val totalPages: Int,
    @SerializedName("current-page")
    val currentPage: Int,
    @SerializedName("child")
    val child: List<Transaction>
)