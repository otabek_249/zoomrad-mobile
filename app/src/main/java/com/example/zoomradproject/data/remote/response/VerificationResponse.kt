package com.example.zoomradproject.data.remote.response

import com.google.gson.annotations.SerializedName

data class VerificationResponse (
    @SerializedName("access-token")
    val accessToken:String,
    @SerializedName("refresh-token")
    val refreshToken:String
)