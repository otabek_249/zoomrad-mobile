package com.example.zoomradproject.data.model

data class Transaction(
    val type: String,
    val from: String,
    val to: String,
    val amount: Double,
    val time: Long
)