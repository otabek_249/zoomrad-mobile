package com.example.zoomradproject.data.model

data class CardData (
    val cardName: String,
    val cardNumber :String
)