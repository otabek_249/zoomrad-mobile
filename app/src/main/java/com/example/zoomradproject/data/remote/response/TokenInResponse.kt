package com.example.zoomradproject.data.remote.response

data class TokenInResponse(
    val token:String
)