package com.example.zoomradproject.data.remote.response

data class SignUpResponse(
    val token:String
)