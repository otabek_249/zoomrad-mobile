package com.example.zoomradproject.data.remote.request

import com.google.gson.annotations.SerializedName

data class UpDateCard(
    @SerializedName("id")
    val id:Int,
    @SerializedName("name")
    val name:String,
    @SerializedName("theme-type")
    val themeType:Int,
    @SerializedName("is-visible")
    val isVisible:Boolean

)
