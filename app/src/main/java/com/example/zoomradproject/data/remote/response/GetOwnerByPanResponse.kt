package com.example.zoomradproject.data.remote.response

import com.google.gson.annotations.SerializedName

data class GetOwnerByPanResponse (
    @SerializedName("pan")
    val name:String
)