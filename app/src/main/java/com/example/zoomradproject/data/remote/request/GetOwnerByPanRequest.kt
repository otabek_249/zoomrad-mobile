package com.example.zoomradproject.data.remote.request

import com.google.gson.annotations.SerializedName

data class GetOwnerByPanRequest (

    @SerializedName("pan")
    val pan:String
)