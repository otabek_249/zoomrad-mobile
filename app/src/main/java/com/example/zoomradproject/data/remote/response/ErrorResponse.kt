package com.example.zoomradproject.data.remote.response

import com.google.gson.annotations.SerializedName

data class ErrorResponse(

    @SerializedName("message")
    val error:String
)
