package com.example.zoomradproject.data.remote.request

import com.google.gson.annotations.SerializedName

data class TransferDataRequest (
    /*
    "type": "third-card",
    "sender-id": "7",
    "receiver-pan": "1234567898765432",
    "amount": 100100*/
    @SerializedName("type")
    val type :String,
    @SerializedName("sender-id")
    val senderId:String,
    @SerializedName("receiver-pan")
    val receiverPan:String,
    @SerializedName("amount")
    val amount:Long
)