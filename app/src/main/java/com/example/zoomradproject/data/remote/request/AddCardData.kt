package com.example.zoomradproject.data.remote.request

import com.google.gson.annotations.SerializedName

data class AddCardData (
    /*{
    "pan": "1234567898765432",
    "expired-year": "2023",
    "expired-month": "1",
    "name": "Personal"
}
    * */
    @SerializedName("pan")
    val pan:String,
    @SerializedName("expired-year")
    val expiredYear :String,
    @SerializedName("expired-month")
    val expiredMonths:String,
    @SerializedName("name")
    val name:String
)