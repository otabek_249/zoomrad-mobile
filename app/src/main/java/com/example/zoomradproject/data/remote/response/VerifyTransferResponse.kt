package com.example.zoomradproject.data.remote.response

data class VerifyTransferResponse (
    val success:String
)