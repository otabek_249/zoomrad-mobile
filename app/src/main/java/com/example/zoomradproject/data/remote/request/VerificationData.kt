package com.example.zoomradproject.data.remote.request

import com.google.gson.annotations.SerializedName

data  class VerificationData (

     @SerializedName("token")
     val token:String,
     @SerializedName("code")
     val code :String
 )