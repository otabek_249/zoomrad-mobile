package com.example.zoomradproject.data.remote.request

import com.google.gson.annotations.SerializedName

data class LogInRequest(
    @SerializedName("phone")
    val phone:String,

    @SerializedName("password")
    val password:String
)
