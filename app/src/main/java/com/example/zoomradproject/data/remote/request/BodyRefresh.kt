package com.example.zoomradproject.data.remote.request

import com.google.gson.annotations.SerializedName

data class BodyRefresh(
    @SerializedName("refresh-token")
    val accessToken :String
)
