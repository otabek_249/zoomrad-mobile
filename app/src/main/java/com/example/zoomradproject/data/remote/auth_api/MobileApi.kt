package com.example.zoomradproject.data.remote.auth_api

import com.example.zoomradproject.data.model.Transaction
import com.example.zoomradproject.data.pref.MyPref
import com.example.zoomradproject.data.remote.request.AddCardData
import com.example.zoomradproject.data.remote.request.BodyRefresh
import com.example.zoomradproject.data.remote.request.GetOwnerByPanRequest
import com.example.zoomradproject.data.remote.request.LogInRequest
import com.example.zoomradproject.data.remote.request.SignUpData
import com.example.zoomradproject.data.remote.request.TransferDataRequest
import com.example.zoomradproject.data.remote.request.UpDateCard
import com.example.zoomradproject.data.remote.request.VerificationData
import com.example.zoomradproject.data.remote.response.AddCardResponse
import com.example.zoomradproject.data.remote.response.CardsResponse
import com.example.zoomradproject.data.remote.response.ErrorResponse
import com.example.zoomradproject.data.remote.response.GetOwnerByPanResponse
import com.example.zoomradproject.data.remote.response.MonitoringResponse
import com.example.zoomradproject.data.remote.response.TokenInResponse
import com.example.zoomradproject.data.remote.response.SignUpResponse
import com.example.zoomradproject.data.remote.response.VerificationResponse
import com.example.zoomradproject.data.remote.response.VerifyTransferResponse
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path
import retrofit2.http.Query

interface MobileApi {


    @POST("mobile-bank/v1/auth/sign-in")
    suspend fun logIn(@Body data: LogInRequest): Response<TokenInResponse>

    @POST("mobile-bank/v1/auth/sign-up")
    suspend fun signUp(@Body data: SignUpData): Response<SignUpResponse>

    @POST("mobile-bank/v1/auth/sign-in/verify")
    suspend fun verification(@Body data: VerificationData): Response<VerificationResponse>
    @POST("mobile-bank/v1/auth/sign-up/verify")
    suspend fun verificationSignUp(@Body data: VerificationData): Response<VerificationResponse>

    @POST("mobile-bank/v1/card")
    suspend fun addCard(@Body data: AddCardData) :Response<AddCardResponse>

    @POST("mobile-bank/v1/auth/update-token")
    fun refreshAccess(@Body refresh: BodyRefresh): Call<VerificationResponse>

    @GET("mobile-bank/v1/card")
    suspend fun getAllCards() :Response<List<CardsResponse>>


    @POST("mobile-bank/v1/transfer/card-owner")
    suspend fun getOwnerByPan(@Body data:GetOwnerByPanRequest):Response<GetOwnerByPanResponse>

    @DELETE("mobile-bank/v1/card/3")
    suspend fun delete()

    @POST("mobile-bank/v1/transfer/transfer")
    suspend fun transferMoney(@Body transferDataRequest: TransferDataRequest):Response<TokenInResponse>

    @POST("mobile-bank/v1/transfer/transfer/verify")
    suspend fun transferVerify(@Body verificationData: VerificationData):Response<VerifyTransferResponse>

    @GET("mobile-bank/v1/transfer/history")
    suspend fun getHistory(
        @Query("size")size:Int,
        @Query("current-page") currentPage: Int,
    ):Response<MonitoringResponse>
    @GET("mobile-bank/v1/transfer/history")
    suspend fun getTransactions(
        @Query("page") page: Int,
        @Query("size") size: Int
    ): MonitoringResponse

    /*
    * @POST("mobile-bank/v1/auth/update-token")
    fun refreshAccess(@Body refresh: BodyRefresh): Call<VerificationResponse>*/

    suspend fun refreshToken(@Body data: BodyRefresh):Response<VerificationResponse>


    @PUT("mobile-bank/v1/card")
    suspend fun updateCard(@Body upDateCard: UpDateCard):Response<ErrorResponse>


    @DELETE("mobile-bank/v1/card/{id}")
    suspend fun deleteCard(@Path("id")id :String):Response<ErrorResponse>
}