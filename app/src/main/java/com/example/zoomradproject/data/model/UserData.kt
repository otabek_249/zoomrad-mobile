package com.example.zoomradproject.data.model

data class UserData(
    val firstName:String,
    val lastName:String,
    val year:String,
    val day :String,
    val months:String
)
