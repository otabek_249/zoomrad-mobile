package com.example.zoomradproject.data.remote.request

import com.google.gson.annotations.SerializedName

data class SignUpData(
    @SerializedName("phone")
    val phone:String,
    @SerializedName("password")
    val password:String,
    @SerializedName("first-name")
    val firstName:String,

    @SerializedName("last-name")
    val lastName:String,
    @SerializedName("born-date")
    val bornDate:String ="12345678",
    @SerializedName("gender")
    val gender:String="2"

)
