package com.example.zoomradproject.data.model

import java.io.Serializable

data class MarkerData(
    val  lat:Double,
    val  lng:Double,
    val location:String,
    val time :String="Ish vaqti:9:00 dab 17:00 gacha \nDam olish kunlari:Shanba-Yakshanba",
    val name:String=" "
):Serializable
