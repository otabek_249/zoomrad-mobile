package com.example.zoomradproject.data.model

data class MapCategoryData (
    val id :Int,
    val name:String
)