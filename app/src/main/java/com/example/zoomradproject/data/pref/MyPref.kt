package com.example.zoomradproject.data.pref

import android.content.Context
import android.content.SharedPreferences

class MyPref {
    companion object {
        private var instance: MyPref? = null
        private lateinit var sharedPref: SharedPreferences


        fun init(context: Context) {
            if (instance == null) {
                instance = MyPref()
                sharedPref = context.getSharedPreferences("SHAREDPREF", Context.MODE_PRIVATE)
            }
        }

        fun getInstance() = instance!!
    }

    fun setEnterFirst(boolean: Boolean){
        sharedPref.edit().putBoolean("ENTER",boolean).apply()
    }

    fun setFirstName(name:String){
        sharedPref.edit().putString("FIRSTNAME",name).apply()
    }
    fun setLastName(name:String){
        sharedPref.edit().putString("LASTNAME",name).apply()
    }
    fun getFirstName():String{
        return sharedPref.getString("FIRSTNAME","")?:""
    }
    fun getFLastName():String{
        return sharedPref.getString("LASTNAME","")?:""
    }

    fun getEnterFirst():Boolean{
        return sharedPref.getBoolean("ENTER",false)
    }

    fun setSignUp(boolean: Boolean){
        sharedPref.edit().putBoolean("SIGN",boolean).apply()
    }
    fun getSIngUp():Boolean{
        return sharedPref.getBoolean("SIGN",false)
    }

    fun setPassword(password: String){
        sharedPref.edit().putString("PASS",password).apply()
    }

    fun getPassword(): String {
        return sharedPref.getString("PASS","")?:""
    }

    fun setTokeFirst(string: String){
        sharedPref.edit().putString("TOKENFIRST",string).apply()
    }
    fun getTokenFirst():String{
        return sharedPref.getString("TOKENFIRST","")?:""
    }

    fun setAccessToke(string: String){
        sharedPref.edit().putString("ACESS",string).apply()
    }
    fun getAccessToken():String{
        return sharedPref.getString("ACESS","otabek")?:"otabek"
    }

    fun setRefreshToken(string: String){
        sharedPref.edit().putString("REFRESH",string).apply()

    }
    fun getRefreshToken():String{
        return sharedPref.getString("REFRESH","")?:""
    }

    fun setCardIndex(int: Int){
        sharedPref.edit().putInt("INDEX",int).apply()
    }
    fun getCardIndex():Int{
        return sharedPref.getInt("INDEX",0)
    }
    fun setPhone(phone:String){
        sharedPref.edit().putString("PHONENUMBER",phone).apply()
    }
    fun getPhone():String{
        return sharedPref.getString("PHONENUMBER","")?:""
    }









}