package com.example.zoomradproject.data.remote.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class CardsResponse(
    /*"id": 21,
        "name": "Personal",
        "amount": 20000000,
        "owner": "Muhammadali",
        "pan": "5420",
        "expired-year": 2023,
        "expired-month": 9,
        "theme-type": 4,
        "is-visible": true*/

    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("amount")
    val amount: String,
    @SerializedName("owner")
    val owner: String,
    @SerializedName("pan")
    val pan: String,
    @SerializedName("expired-year")
    val expiredYear: String,
    @SerializedName("expired-month")
    val expiredMonth: String,
    @SerializedName("theme-type")
    val themeType: Int,
    @SerializedName("is-visible")
    val isVisible: Boolean
):Serializable