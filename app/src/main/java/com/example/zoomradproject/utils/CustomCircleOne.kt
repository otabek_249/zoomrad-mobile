package com.example.zoomradproject.utils

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.zoomradproject.ui.theme.GreenColor
import com.example.zoomradproject.ui.theme.MainBgColor


@Composable
fun CustomCircleOne() {
    Box(
        modifier = Modifier
            .padding(end = 4.dp)
            .width(18.dp)
            .height(18.dp)
            .border(width = 1.dp, Color.Gray, shape = CircleShape)
            .background(color = MainBgColor, shape = CircleShape),


        )
}

@Composable
fun CustomCircleTwo() {
    Box(
        modifier = Modifier
            .padding(end = 4.dp)
            .size(18.dp)
            .background(color = GreenColor, shape = CircleShape),
        )
}


@Composable
@Preview
fun CustomCirclePreview() {
    CustomCircleOne()
}
