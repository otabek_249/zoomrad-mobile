package com.example.zoomradproject.utils

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.zoomradproject.ui.theme.ItemBgColor


@Composable
fun MyEditText(
    text: String,
    onTextChanged: (String) -> Unit // Add this parameter for handling text changes
) {
    Box(
        modifier = Modifier
            .padding(24.dp)
            .height(56.dp)
            .fillMaxWidth()
            .background(ItemBgColor, shape = RoundedCornerShape(16.dp)),
        contentAlignment = Alignment.CenterStart
    ) {
        BasicTextField(
            value = text,
            onValueChange = { newText ->
                onTextChanged(newText)
            },
            modifier = Modifier
                .padding(start = 4.dp)
                .fillMaxSize()
                .align(Alignment.CenterStart)
        )
    }
}



@Preview
@Composable
fun MyEditTextPreview() {
    MyEditText(text = "", onTextChanged = {

    })
}
