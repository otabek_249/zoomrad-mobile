package com.example.zoomradproject.utils

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.zoomradproject.R
import com.example.zoomradproject.ui.theme.ItemBgColor
import com.example.zoomradproject.ui.theme.MainBgColor


@Composable
fun ItemBranchInfo(location:String,timeWork:String,name:String,click:(Unit)->Unit) {
    Column(
        modifier = Modifier
            .clickable {
                click.invoke(Unit)
            }
            .padding(start = 24.dp, top = 24.dp, bottom = 16.dp)
            .fillMaxWidth()

    ) {
        Text(
            modifier = Modifier,
            text = name,
            fontSize = 18.sp,
            fontFamily = FontFamily(Font(R.font.nata_semibold))
            )
        Row(modifier = Modifier
            .padding(top = 16.dp),
            verticalAlignment = Alignment.CenterVertically) {
            Box(
                modifier = Modifier
                    .background(color = ItemBgColor, shape = RoundedCornerShape(12.dp))
            ) {
                Image(
                    modifier = Modifier
                        .padding(start = 8.dp, top = 8.dp, bottom = 8.dp, end = 8.dp)
                        .height(36.dp)
                        .width(36.dp)
                        .padding(6.dp),
                    painter = painterResource(id = R.drawable.ic_location_green),
                    contentDescription =""
                )

            }
            Text(
                modifier = Modifier
                    .padding(start = 8.dp, end = 32.dp),
                text = location,
                fontSize = 14.sp,
                maxLines = 2
            )
        }
        Row(
            modifier = Modifier.padding(top = 16.dp),
        ) {
            Box(
                modifier = Modifier
                    .background(color = ItemBgColor, shape = RoundedCornerShape(12.dp))
            ) {
                Image(
                    modifier = Modifier
                        .padding(start = 8.dp, top = 8.dp, bottom = 8.dp, end = 8.dp)
                        .height(36.dp)
                        .width(36.dp)
                        .padding(8.dp),
                    painter = painterResource(id = R.drawable.ic_clock_green),
                    contentDescription =""
                )

            }
            Text(
                modifier = Modifier
                    .padding(start = 8.dp, end = 32.dp),
                text =timeWork,
                fontSize = 14.sp,
                maxLines = 2
            )
        }

    }
}


@Preview
@Composable
fun ItemBranchPreview() {
    Column(
        modifier = Modifier
            .background(MainBgColor)
            .fillMaxSize()
    ) {

    }

}