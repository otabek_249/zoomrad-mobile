package com.example.zoomradproject.utils

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.zoomradproject.R
import com.example.zoomradproject.ui.theme.GreenColor


@Composable
fun Item2(image: Int, msg: String) {

    Card(
        modifier = Modifier
            .padding(start = 16.dp)
            .width(90.dp)
            .height(130.dp),
        colors = CardDefaults.cardColors(GreenColor),
        shape = RoundedCornerShape(12.dp)
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
        ) {
            Box(
                modifier = Modifier
                    .weight(1f)
            )
            {
                Image(
                    modifier = Modifier
                        .padding(6.dp)
                        .fillMaxSize(),

                    painter = painterResource(id = image),
                    contentDescription = ""
                )

            }
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .weight(0.4f),
                contentAlignment = Alignment.Center
            ) {
                Text(
                    modifier = Modifier
                        .padding(horizontal = 8.dp),
                    overflow = TextOverflow.Ellipsis,
                    textAlign = TextAlign.Center,
                    maxLines = 2,
                    text = msg,
                    fontSize = 11.sp,
                    fontFamily = FontFamily(Font(R.font.nata_semibold)),
                    color = Color.White
                )
            }


        }

    }
}


@Preview
@Composable
fun Item2Preview() {

}