package com.example.zoomradproject.utils

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.zoomradproject.ui.theme.ItemBgColor


@Composable
fun ItemBranchCategory(textColor: Color, background:Color,text:String,click:(Unit)->Unit){
    Box(modifier = Modifier
        .clickable {
            click.invoke(Unit)
        }
        .padding(8.dp)
        .shadow(elevation = 2.dp, shape = RoundedCornerShape(10.dp))
        .background(color = background, shape = RoundedCornerShape(10.dp))
        .height(46.dp),
        contentAlignment = Alignment.Center
        ){
        Text(
            modifier = Modifier
                .padding(start = 24.dp, end =24.dp, top = 4.dp, bottom = 4.dp),
            text = text,
            fontSize = 16.sp,
            color = textColor)

    }

}
