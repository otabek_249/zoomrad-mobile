package com.example.zoomradproject.utils

import android.icu.util.Currency
import androidx.annotation.ColorRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.zoomradproject.R
import com.example.zoomradproject.ui.theme.GreenColor


@Composable
fun Item1(modifier: Modifier,name:String,number:String,currency: String,dotsClick:(Unit)->Unit,
          imageClick:(Unit)->Unit,image:Int) {
    Card(
        modifier =modifier,
        backgroundColor = Color.White,
        elevation = 4.dp,
        shape = RoundedCornerShape(8.dp)
    ) {
        Row(
            modifier = Modifier
                .fillMaxSize()
        ) {
            Spacer(modifier = Modifier.width(10.dp))
            Box(
                modifier = Modifier

                    .fillMaxHeight()
                    .weight(1f),
                contentAlignment = Alignment.CenterStart
            ) {
                Image(
                    modifier = Modifier
                        .clickable {

                        }
                        .height(48.dp)
                        .width(48.dp)
                        .background(color = GreenColor, shape = RoundedCornerShape(8.dp))
                        .padding(10.dp),
                    painter = painterResource(id = image),

                    colorFilter = ColorFilter.tint(Color.White),
                    contentDescription = "",
                    
                )
            }
            Spacer(modifier = Modifier.width(2.dp))
            Column(
                modifier = Modifier
                    .weight(1f),
            ) {
                Spacer(modifier = Modifier.height(8.dp))
                Text(
                    text ="Keshbek",
                    fontFamily = FontFamily(Font(R.font.nata_semibold)),
                    fontSize = 9.sp

                )
                Spacer(modifier = Modifier.weight(1f))

                Text(text ="0",
                    fontSize = 11.sp,
                    fontFamily = FontFamily(Font(R.font.nata_bold)))
                Spacer(modifier = Modifier.weight(1f))
                Text(
                    text ="ball",
                    fontFamily = FontFamily(Font(R.font.nata_regular)),
                    fontSize = 9.sp
                    )
                Spacer(modifier = Modifier.height(8.dp))

            }
            Box(
                modifier = Modifier

                    .padding(top = 4.dp)
                    .fillMaxHeight()
            ) {
                Image(
                    modifier = Modifier
                        .clickable {

                        }
                        .height(18.dp)
                        .width(18.dp),
                    painter = painterResource(id = R.drawable.ic_three_dots),
                    contentDescription = ""
                )

            }

        }

    }

}

@Composable
@Preview
fun Item1Preview() {

}