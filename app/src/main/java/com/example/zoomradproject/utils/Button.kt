package com.example.zoomradproject.utils

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.zoomradproject.R
import com.example.zoomradproject.ui.theme.ButtonBgColor


@Composable
fun ButtonLanguage(language: Int, horizontalPadding: Int, click: () -> Unit) {
    Button(
        elevation =ButtonDefaults.elevatedButtonElevation(4.dp),
        onClick = click,
        shape = RoundedCornerShape(12.dp),
        modifier = Modifier
            .padding(horizontal = horizontalPadding.dp)
            .fillMaxWidth()
            .height(48.dp),
        colors = ButtonDefaults.buttonColors(ButtonBgColor)
    ) {
        Text(
            text = stringResource(id = language),
            fontSize = 18.sp,
            fontFamily = FontFamily(Font(R.font.nata_semibold)),
            color = Color.Black,
            fontWeight = FontWeight(600)
        )
    }

}

@Composable
@Preview
fun ButtonLanguagePreview() {
    ButtonLanguage(R.string.uzbek, 24){

    }
}