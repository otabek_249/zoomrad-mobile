package com.example.zoomradproject.utils

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.zoomradproject.R


@Composable
fun NumberText(number: String, click: (String) -> Unit) {
    Box(
        modifier = Modifier
            .height(42.dp)
            .width(42.dp)
            .clickable(

            ) {
                click(number)


            },
        contentAlignment = Alignment.Center
    ) {
        Text(
            text = number,
            fontSize = 24.sp,
            fontFamily = FontFamily(Font(R.font.nata_semibold))
        )

    }

}

@Preview
@Composable
fun NumberTextPreview() {

}
