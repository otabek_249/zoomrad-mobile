package com.example.zoomradproject.utils

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.material3.Icon
import androidx.compose.material3.ModalDrawerSheet
import androidx.compose.material3.NavigationDrawerItem
import androidx.compose.material3.NavigationDrawerItemDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.zoomradproject.R
import com.example.zoomradproject.ui.theme.GreenColor

@Composable
fun MyDrawer( selectedItem:(Int)->Unit){
    data class NavigationItems(val title: String, val selectedIcon: Painter)

    val items = listOf(
        NavigationItems(
            title = "Profil",
            selectedIcon = painterResource(id = R.drawable.ic_profile)
        ),
        NavigationItems(
            title = "Sozlamalar",
            selectedIcon = painterResource(id = R.drawable.ic_settings_green)
        ),
        NavigationItems(
            title = "Operatsiya tarixi",
            selectedIcon = painterResource(id = R.drawable.ic_journal_green)
        ),
        NavigationItems(
            title = "Monitoring",
            selectedIcon = painterResource(id = R.drawable.ic_monitoring_green)
        ),
        NavigationItems(
            title = "Mening aizalarim",
            selectedIcon = painterResource(id = R.drawable.ic_my_application_green)
        ),
        NavigationItems(
            title = "Yordam",
            selectedIcon = painterResource(id = R.drawable.ic_chat)
        ),
        NavigationItems(
            title = "Dasturni ulashing",
            selectedIcon = painterResource(id = R.drawable.ic_share_green)
        ),
        NavigationItems(
            title = "Chiqish",
            selectedIcon = painterResource(id = R.drawable.ic_logout_green)
        )
    )
    ModalDrawerSheet {
        Column(

            modifier = Modifier
                .fillMaxSize()
        ) {
            Box(
                modifier = Modifier
                    .weight(1f)
            ) {
                Image(
                    modifier = Modifier.fillMaxSize(),
                    contentScale = ContentScale.Crop,
                    painter = painterResource(id = R.drawable.image_card_bacground),
                    contentDescription = ""
                )
                Column(
                    modifier = Modifier.fillMaxSize()
                ) {
                    Spacer(modifier = Modifier.weight(1f))
                    Image(
                        painter = painterResource(R.drawable.image_avatar),
                        contentDescription = "avatar",
                        contentScale = ContentScale.Crop,
                        modifier = Modifier
                            .padding(start = 24.dp, top = 16.dp)
                            .size(74.dp)
                            .clip(RoundedCornerShape(60.dp))
                    )
                    Text(
                        modifier = Modifier.padding(start = 24.dp, top = 16.dp),
                        text = "Otabek Chorshanbiyev",
                        fontSize = 18.sp,
                        fontFamily = FontFamily(Font(R.font.nata_semibold)),
                        color = Color.White,
                    )
                    Row(
                        modifier = Modifier
                            .padding(start = 24.dp, top = 12.dp)
                            .height(26.dp)
                            .width(200.dp)
                            .clip(shape = RoundedCornerShape(16.dp))
                            .background(Color.White),
                        verticalAlignment = Alignment.CenterVertically

                    ) {

                        Text(
                            modifier = Modifier.padding(start = 16.dp),
                            fontSize = 14.sp,
                            fontFamily = FontFamily(Font(R.font.nata_bold)),
                            text = "Tasdiqlagan mijoz",
                            color = GreenColor
                        )

                        Image(
                            modifier = Modifier
                                .padding(4.dp)
                                .height(24.dp)
                                .width(24.dp)
                                .padding(2.dp),
                            painter = painterResource(id = R.drawable.ic_checkmark),
                            contentDescription = ""
                        )

                    }
                    Text(
                        modifier = Modifier.padding(start = 24.dp, top =10.dp, bottom = 4.dp),
                        text = "+998930720405",
                        fontSize = 16.sp,
                        fontFamily = FontFamily(Font(R.font.nata_semibold)),
                        color = Color.White
                    )
                    Spacer(modifier = Modifier.weight(1f))

                }

            }
            Column(
                modifier = Modifier
                    .padding(bottom = 48.dp)
                    .weight(3f)
            ) {

                items.forEachIndexed { index, item ->
                    NavigationDrawerItem(
                        label = {
                            Text(
                                fontFamily = FontFamily(Font(R.font.nata_semibold)),
                                text = item.title
                            ) },
                        selected = false,
                        onClick = {
                            selectedItem.invoke(index)
                        },
                        icon = {
                            Icon(
                                modifier = Modifier
                                    .height(24.dp)
                                    .width(24.dp),
                                painter = item.selectedIcon,

                                contentDescription = item.title,
                                tint = GreenColor
                            )
                        },
                        badge = {  // Show Badge
                            /*   item.badgeCount?.let {
                                   androidx.compose.material3.Text(text = item.badgeCount.toString())
                               }*/
                        },
                        modifier = Modifier.padding(NavigationDrawerItemDefaults.ItemPadding) //padding between items
                    )
                }

            }
            Spacer(modifier = Modifier.height(8.dp)) //space (margin) from top


        }


    }
}