package com.example.zoomradproject.utils

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.zoomradproject.R
import com.example.zoomradproject.ui.theme.ItemBgColor

@Composable
fun CurrencyItem() {
    Card(
        modifier = Modifier
            .padding(start = 16.dp, bottom = 16.dp)
            .width(160.dp)
            .height(56.dp),
        backgroundColor = ItemBgColor,
    ) {
        Row(
            modifier = Modifier
                .fillMaxSize()
        ) {
            Spacer(modifier = Modifier.width(8.dp))
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .weight(1f),
                contentAlignment = Alignment.Center
            ) {
                Image(
                    modifier = Modifier
                        .padding(2.dp),
                    painter = painterResource(id = R.drawable.ic_usd_flag),
                    contentDescription = ""
                )

            }
            Spacer(modifier = Modifier.width(4.dp))
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .weight(1f),

            ) {
                Spacer(modifier = Modifier.height(10.dp))
                Text(
                    color = Color.Black,
                    text = "Valuyuta",
                    fontSize = 8.sp
                )
                Spacer(modifier = Modifier.height(10.dp))
                Text(
                    color = Color.Black,
                    text = "Valuyuta",
                    fontSize = 8.sp
                )

                Spacer(modifier = Modifier.height(8.dp))



            }
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .weight(1f)

            ) {
                Spacer(modifier = Modifier.height(10.dp))
                Text(
                    color = Color.Black,
                    text = "Valuyuta",
                    fontSize = 8.sp
                )
                Spacer(modifier = Modifier.height(10.dp))
                Text(
                    color = Color.Black,
                    text = "Valuyuta",
                    fontSize = 8.sp
                )

                Spacer(modifier = Modifier.height(8.dp))

            }
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .weight(1f)
            ) {
                Spacer(modifier = Modifier.height(10.dp))
                Text(
                    color = Color.Black,
                    text = "Valuyuta",
                    fontSize = 8.sp
                )
                Spacer(modifier = Modifier.height(10.dp))
                Text(
                    color = Color.Black,
                    text = "Valuyuta",
                    fontSize = 8.sp
                )

                Spacer(modifier = Modifier.height(8.dp))

            }
            Spacer(modifier = Modifier.width(4.dp))

        }

    }
}

@Composable
@Preview
fun CurrencyPreview() {
    CurrencyItem()
}