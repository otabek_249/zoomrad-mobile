package com.example.zoomradproject.utils

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.zoomradproject.R
import com.example.zoomradproject.ui.theme.GreenColor


@Composable
fun HelpItem(text:Int,image:Int,margin :Int,click:(Unit)->Unit) {
    Column (modifier = Modifier
        .padding(end = 24.dp)
        .width(64.dp),
        horizontalAlignment = Alignment.CenterHorizontally){
        Box(
            modifier = Modifier
                .clickable {
                    click.invoke(Unit)
                }
                .shadow(6.dp, shape = CircleShape)
                .background(color = Color(0xFFFEFEFE), shape = CircleShape)
                .height(64.dp)
                .width(64.dp)
        ) {
            Image(
                modifier = Modifier
                    .padding(margin.dp)
                    .fillMaxSize()
                    .padding(6.dp),
                painter = painterResource(id = image),
                contentDescription = ""
            )

        }
        Box(modifier = Modifier){

        }
        Text(
            modifier = Modifier
                .padding(top = 4.dp)
                .align(Alignment.CenterHorizontally),
            color = Color(0xFF1A1A1A),
            textAlign = TextAlign.Center,
            text = stringResource(text),
            fontSize = 12.sp,
            fontFamily = FontFamily(Font(R.font.nata_bold))
        )


    }

}
@Composable
@Preview
fun Preview(){

}