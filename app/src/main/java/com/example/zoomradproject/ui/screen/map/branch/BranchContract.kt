package com.example.zoomradproject.ui.screen.map.branch

import com.example.zoomradproject.data.model.MarkerData
import org.orbitmvi.orbit.ContainerHost

interface BranchContract {

    interface BranchViewModel : ContainerHost<UIState, SideEffect> {
        fun onEventDispatcher(intent: Intent)
    }

    data class UIState (
        var map: Map<Int, List<MarkerData>> = emptyMap()
    )

    sealed interface SideEffect {
        data class BottomSheetDialog(val markerData: MarkerData) : SideEffect
    }

    sealed interface Intent {
        data class ClickMarker(val markerData: MarkerData) : Intent
        data object BackClick : Intent
    }
}