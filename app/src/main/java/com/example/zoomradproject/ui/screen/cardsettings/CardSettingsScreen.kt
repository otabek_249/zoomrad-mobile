package com.example.zoomradproject.ui.screen.cardsettings

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.hilt.getViewModel
import com.example.zoomradproject.R
import com.example.zoomradproject.data.remote.request.UpDateCard
import com.example.zoomradproject.data.remote.response.CardsResponse
import com.example.zoomradproject.ui.activity.CustomDeleteDialog
import com.example.zoomradproject.ui.screen.add_card.DotIndicator
import com.example.zoomradproject.ui.screen.cards.CardsContract
import com.example.zoomradproject.ui.theme.EditTextBgColor
import com.example.zoomradproject.ui.theme.GreenColor
import com.example.zoomradproject.ui.theme.MainBgColor
import com.example.zoomradproject.ui.theme.TextColor
import org.orbitmvi.orbit.compose.collectSideEffect

class CardSettingsScreen(private val card: CardsResponse) : Screen {
    @Composable
    override fun Content() {


        val model :CardSettingsContract.CardSettingViewModel = getViewModel<CardSettingsViewModelImpl>()
        var id by remember {
            mutableStateOf("0")
        }
        var state by remember {
            mutableStateOf(false)
        }


        if (state) {
            CustomDeleteDialog(
                onConfirm = {
                    model.onEventDispatcher(
                        CardSettingsContract.Intent.DeleteCardButton(id.toString())

                    )
                    state = false
                },
                onDismiss = {
                    state = false
                })
        }
        model.collectSideEffect {

        }

        CardSettingsContent(card,model::onEventDispatcher)
    }
}
val data= arrayListOf(
    R.drawable.img_card1,
    R.drawable.img_card2,
    R.drawable.img_card3,
    R.drawable.img_card4,
    R.drawable.img_card5,
    R.drawable.img_card6,
    R.drawable.img_card7,
)
@OptIn(ExperimentalFoundationApi::class, ExperimentalMaterial3Api::class)
@Composable
fun CardSettingsContent(card: CardsResponse,onEventDispatcher:(CardSettingsContract.Intent)->Unit) {
    val pagerState = rememberPagerState(pageCount = { 7 })
    var cardName by remember { mutableStateOf(card.name) }
    var cardNumber by remember { mutableStateOf(card.pan) }
    val cardLifetime by remember { mutableStateOf(card.expiredMonth + card.expiredYear.substring(2)) }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
            .background(MainBgColor)
    ) {
        Row(
            modifier = Modifier
                .padding(top = 24.dp)
        ) {
            Image(
                modifier = Modifier
                    .clickable { }
                    .padding(start = 16.dp)
                    .height(24.dp)
                    .width(24.dp),
                painter = painterResource(id = R.drawable.ic_back_green),
                contentDescription = "OpenDrawer"
            )
            Spacer(modifier = Modifier.weight(1f))

            Text(
                fontSize = 20.sp,
                color = TextColor,
                fontFamily = FontFamily(Font(R.font.nata_bold)),
                text = "Card Settings"
            )
            Spacer(modifier = Modifier.weight(1f))
            Text(
                modifier = Modifier
                    .clickable {
                        onEventDispatcher.invoke(CardSettingsContract.Intent.Delete(card.id.toString()))
                    }
                    .padding(end = 4.dp),
                fontSize = 19.sp,
                fontFamily = FontFamily(Font(R.font.nata_bold)),
                color = Color.Red,
                text = stringResource(id = R.string.delete)
            )
        }

        HorizontalPager(state = pagerState) {
            Card(
                modifier = Modifier
                    .padding(start = 32.dp, end = 32.dp, top = 24.dp)
                    .height(190.dp)
                    .fillMaxWidth(),
                colors = CardDefaults.cardColors(GreenColor),
                shape = RoundedCornerShape(12.dp)
            ) {
                Box(modifier = Modifier.fillMaxSize()) {
                    Image(
                        modifier = Modifier.fillMaxSize(),
                        contentScale = ContentScale.Crop,
                        painter = painterResource(data[pagerState.currentPage]),
                        contentDescription = ""
                    )
                    Column(
                        modifier = Modifier.fillMaxSize()
                    ) {
                        Image(
                            modifier = Modifier
                                .padding(start = 18.dp, top = 24.dp)
                                .height(42.dp)
                                .width(42.dp),
                            painter = painterResource(R.drawable.ic_card_default),
                            contentDescription = ""
                        )

                        Text(
                            modifier = Modifier.padding(start = 18.dp, top = 16.dp),
                            text = cardName,
                            fontFamily = FontFamily(Font(R.font.nata_semibold)),
                            fontSize = 16.sp,
                            color = Color.White
                        )
                        Spacer(modifier = Modifier.weight(1f))
                        Row(
                            modifier = Modifier.padding(start = 18.dp)
                        ) {
                            Text(
                                text = card.pan,
                                fontFamily = FontFamily(Font(R.font.nata_semibold)),
                                fontSize = 14.sp,
                                color = Color.White
                            )
                            Text(
                                modifier = Modifier.padding(start = 8.dp),
                                text = cardLifetime,
                                fontFamily = FontFamily(Font(R.font.nata_semibold)),
                                fontSize = 14.sp,
                                color = Color.White
                            )
                        }
                        Spacer(modifier = Modifier.height(16.dp))
                    }
                }
            }
        }
        DotIndicator(
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .padding(top = 8.dp),
            totalDots = 4, selectedIndex = pagerState,
            dotSize = 16,
            activeColor = Color.Gray,
            padding = 6
        )

        Row(
            modifier = Modifier
                .padding(start = 24.dp, end = 24.dp)
                .height(24.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            Text(
                text = "",
                textAlign = TextAlign.Center,
                color = GreenColor,
                fontSize = 13.sp,
                fontFamily = FontFamily(Font(R.font.nata_semibold))
            )
        }

        Row(
            modifier = Modifier
                .padding(start = 24.dp, top = 8.dp, end = 24.dp)
                .fillMaxWidth()
                .height(48.dp)
                .background(color = EditTextBgColor, shape = RoundedCornerShape(12.dp)),
        ) {
            OutlinedTextField(
                modifier = Modifier
                    .padding(start = 4.dp, end = 24.dp)
                    .weight(1f)
                    .height(48.dp),
                shape = RoundedCornerShape(12.dp),
                value = cardName,
                onValueChange = {
                    if (it.length <= 16) {
                        cardName = it
                    }
                },
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    containerColor = EditTextBgColor,
                    focusedBorderColor = Color.Transparent,
                    unfocusedBorderColor = Color.Transparent,
                    cursorColor = Color.Black
                ),
                maxLines = 1,
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Text,
                    imeAction = ImeAction.Done
                ),
                keyboardActions = KeyboardActions.Default,
            )
            Spacer(modifier = Modifier.width(8.dp))
        }

        Box(modifier = Modifier.weight(1f))

        Column(
            modifier = Modifier
                .padding(bottom = 24.dp)
                .fillMaxWidth()
        ) {
            Button(
                enabled = cardName.length > 4,
                shape = RoundedCornerShape(12.dp),
                modifier = Modifier
                    .padding(start = 24.dp, end = 24.dp, top = 16.dp)
                    .height(48.dp)
                    .fillMaxWidth(),
                onClick = {
                          onEventDispatcher.invoke(
                              CardSettingsContract.Intent.UpDateCardButton(
                                  UpDateCard(
                                      id = card.id,
                                      name = cardName,
                                      themeType = pagerState.currentPage,
                                      isVisible = card.isVisible
                                  )
                              )
                          )
                    /* onEventDispatcher.invoke(
                        AddCardContract.Intent.AddButton(
                            AddCardData(
                                pan = cardNumber,
                                expiredYear = "20" + cardLifetime.substring(2),
                                expiredMonths = cardLifetime.substring(0, 2),
                                name = cardName
                            )
                        )
                    )*/
                },
                colors = ButtonDefaults.buttonColors(
                    containerColor = GreenColor,
                    disabledContainerColor = Color.Gray,
                )
            ) {
                Text(stringResource(id = R.string.add_card))
            }
        }
    }
}

@Composable
@Preview
fun CardSettingsPreview() {
    val card = CardsResponse(
        name = "Sample Card",
        pan = "1234 5678 9012 3456",
        expiredMonth = "12",
        expiredYear = "2025",
        isVisible = false,
        themeType = 1,
        amount = "122",
        id = 1,
        owner = ""
    )
    CardSettingsContent(card){}
}
