package com.example.zoomradproject.ui.screen.settings.main

import android.annotation.SuppressLint
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Slider
import androidx.compose.material3.SliderDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import cafe.adriel.voyager.core.screen.Screen
import com.example.zoomradproject.R
import com.example.zoomradproject.ui.screen.language.setLocate
import com.example.zoomradproject.ui.screen.settings.language.LanguageItem
import com.example.zoomradproject.ui.screen.settings.notification.NotificationItem
import com.example.zoomradproject.ui.screen.settings.theme.ThemeItem
import com.example.zoomradproject.ui.theme.GreenColor
import com.example.zoomradproject.ui.theme.MainBgColor

class SettingsScreen : Screen {
    @Composable
    override fun Content() {
    SettingsContent()
    }
}


@SuppressLint("UnrememberedMutableInteractionSource")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SettingsContent() {

    var sliderValue by remember {
        mutableFloatStateOf(0f)
    }
    val context = LocalContext.current
    val interactionSource = MutableInteractionSource()

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MainBgColor)
    ) {
        Row(
            modifier = Modifier.padding(start = 16.dp, end = 16.dp, top = 24.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Image(
                modifier = Modifier
                    .height(24.dp)
                    .width(24.dp)
                    .padding(3.dp),
                painter = painterResource(id = R.drawable.ic_back_green),
                contentDescription = "",
            )
            Spacer(modifier = Modifier.weight(1f))
            Text(
                text = "Setting",
                fontSize = 18.sp,
                color = Color.Gray,
                fontFamily = FontFamily(Font(R.font.nata_semibold))
            )
            Spacer(modifier = Modifier.weight(1f))

            Spacer(
                modifier = Modifier
                    .height(24.dp)
                    .width(24.dp)
                    .padding(3.dp),
            )
        }
        Spacer(modifier = Modifier.height(32.dp))

        Column(
            modifier = Modifier
                .padding(start = 32.dp, end = 24.dp)
        ) {
            LanguageItem {
                when(it){
                    1->{
                        setLocate(context,"uz")
                    }
                    2->{
                        setLocate(context,"ru")
                    }
                    3->{
                        setLocate(context,"eng")
                    }
                }
            }
            Box(
                modifier = Modifier
                    .padding(top = 16.dp, bottom = 16.dp)
                    .height(1.dp)
                    .background(Color.Gray)
                    .fillMaxWidth()
            )
            Row(
                modifier = Modifier
                    .padding()
                    .fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    modifier = Modifier
                        .padding(end = 8.dp)
                        .height(24.dp)
                        .width(24.dp),
                    painter = painterResource(id = R.drawable.ic_padlock),
                    contentDescription = ""
                )
                Text(
                    text = "Xafsizlik",
                    color = Color.Black,
                    modifier = Modifier.padding(start = 8.dp),
                    fontSize = 16.sp,
                    fontFamily = FontFamily(Font(R.font.nata_bold))
                )
                Spacer(modifier = Modifier.weight(1f))


                Image(
                    modifier = Modifier
                        .padding(end = 16.dp)
                        .height(24.dp)
                        .width(24.dp)
                        .padding(4.dp)
                        .rotate(180f)
                        .clickable {

                        },

                    painter = painterResource(id = R.drawable.ic_back_green),
                    contentDescription = ""
                )
            }
            Box(
                modifier = Modifier
                    .padding(top = 16.dp, bottom = 16.dp)
                    .height(1.dp)
                    .background(Color.Gray)
                    .fillMaxWidth()
            )
            Row(
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                Image(
                    modifier = Modifier
                        .padding(end = 8.dp)
                        .height(24.dp)
                        .width(24.dp),
                    painter = painterResource(id = R.drawable.ic_monitoring_green),
                    contentDescription = ""
                )

                Column {

                    Row {
                        Column {
                            Text(
                                text = "To'lov monitori",
                                color = Color.Black,
                                modifier = Modifier.padding(start = 8.dp),
                                fontSize = 16.sp,
                                fontFamily = FontFamily(Font(R.font.nata_semibold))
                            )
                            Text(
                                text = "Xizmat naxi 0 so'm",
                                color = Color.Gray,
                                modifier = Modifier.padding(start = 8.dp,top=4.dp),
                                fontSize = 10.sp,
                                fontFamily = FontFamily(Font(R.font.nata_semibold))
                            )


                        }
                        Spacer(modifier = Modifier.weight(1f))
                        Slider(
                            modifier = Modifier
                                .width(60.dp),
                            value =sliderValue, onValueChange ={
                                sliderValue = it
                            },
                            thumb = {


                                SliderDefaults.Thumb(
                                    colors = SliderDefaults.colors(GreenColor),
                                    interactionSource = interactionSource,
                                    thumbSize = DpSize(25.dp, 25.dp)
                                )
                            }
                        )
                    }


                    Text(
                        text = "Men ommaviy ofertaga shartlariga roziman",
                        color = Color.Black,
                        modifier = Modifier.padding(start = 8.dp),
                        fontSize = 13.sp,
                        fontFamily = FontFamily(Font(R.font.nata_semibold))
                    )


                }


                Spacer(modifier = Modifier.weight(1f))


            }
            Box(
                modifier = Modifier
                    .padding(top = 16.dp, bottom = 16.dp)
                    .height(1.dp)
                    .background(Color.Gray)
                    .fillMaxWidth()
            )
            NotificationItem {

            }
            Box(
                modifier = Modifier
                    .padding(top = 16.dp, bottom = 16.dp)
                    .height(1.dp)
                    .background(Color.Gray)
                    .fillMaxWidth()
            )

            Row(
                modifier = Modifier
                    .fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    modifier = Modifier
                        .padding(end = 8.dp)
                        .height(24.dp)
                        .width(24.dp),
                    painter = painterResource(id = R.drawable.ic_journal_green),
                    contentDescription = ""
                )
                Text(
                    text = "Ommaviy oferta",
                    color = Color.Black,
                    modifier = Modifier.padding(start = 8.dp),
                    fontSize = 16.sp,
                    fontFamily = FontFamily(Font(R.font.nata_bold))
                )
                Spacer(modifier = Modifier.weight(1f))


                Image(
                    modifier = Modifier
                        .padding(end = 16.dp)
                        .height(24.dp)
                        .width(24.dp)
                        .padding(4.dp)
                        .rotate(180f)
                        .clickable {

                        },

                    painter = painterResource(id = R.drawable.ic_back_green),
                    contentDescription = ""
                )
            }
            Box(
                modifier = Modifier
                    .padding(top = 16.dp, bottom = 16.dp)
                    .height(1.dp)
                    .background(Color.Gray)
                    .fillMaxWidth()
            )

            ThemeItem {

            }


        }


    }
}


@Preview
@Composable
fun SettingsPreview() {
    SettingsContent()
}