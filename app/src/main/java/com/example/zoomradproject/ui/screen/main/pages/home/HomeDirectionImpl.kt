package com.example.zoomradproject.ui.screen.main.pages.home

import cafe.adriel.voyager.core.screen.Screen
import com.example.zoomradproject.ui.screen.add_card.AddCardScreen
import com.example.zoomradproject.ui.screen.auth.log_in.LogInScreen
import com.example.zoomradproject.ui.screen.cards.CardsScreen
import com.example.zoomradproject.ui.screen.language.LanguageScreen
import com.example.zoomradproject.ui.screen.main.pages.help.HelpPage
import com.example.zoomradproject.ui.screen.monitoring.MonitoringScreen
import com.example.zoomradproject.ui.screen.profile.ProfileScreen
import com.example.zoomradproject.ui.screen.settings.main.SettingsScreen
import com.example.zoomradproject.utils.navigation.AppNavigator
import javax.inject.Inject


interface HomeDirection {
    suspend fun openAddCart()
    suspend fun openDrawer(int: Int)
    suspend fun openCardsScreen()
    suspend fun logOut()
}

class HomeDirectionImpl @Inject constructor(
    private val navigator: AppNavigator
) : HomeDirection {
    override suspend fun openAddCart() {
        navigator.navigateTo(AddCardScreen())
    }

    override suspend fun openCardsScreen() {
        navigator.navigateTo(CardsScreen(0))
    }

    override suspend fun logOut() {
        navigator.replaceAll(LanguageScreen())
    }

    override suspend fun openDrawer(int: Int) {
        when (int) {
            0 -> {
                navigator.navigateTo(ProfileScreen())
            }

            1 -> {
                // Settings
                navigator.navigateTo(SettingsScreen())
            }

            2 -> {
                // operatsiya tarixi
            }

            3 -> {
                navigator.navigateTo(MonitoringScreen())
            }

            4 -> {
                // Mening arizalarim
            }

            5 -> {
                // Yordam
                navigator.navigateTo(HelpPage as Screen)
            }

            6 -> {
                // programming share
            }

            else -> {
                // exit
            }
        }
    }
}