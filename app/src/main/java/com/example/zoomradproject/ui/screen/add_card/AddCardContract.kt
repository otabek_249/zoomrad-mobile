package com.example.zoomradproject.ui.screen.add_card

import com.example.zoomradproject.data.remote.request.AddCardData
import org.orbitmvi.orbit.ContainerHost

interface AddCardContract {

    interface AddCardViewModel :ContainerHost<UIState,SideEffect>{
        fun onEventDispatcher(intent:Intent)
    }




    data class  UIState(
        val nameCard:String =""
    )

    sealed interface SideEffect{

        data class Toast(val msg:String):SideEffect

    }

    sealed interface Intent{
        data class AddButton(val data: AddCardData) :Intent
        data class GetByOwner(val string: String):Intent
        data object BackText :Intent
    }
}