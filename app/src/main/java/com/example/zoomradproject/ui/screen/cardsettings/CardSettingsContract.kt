package com.example.zoomradproject.ui.screen.cardsettings

import com.example.zoomradproject.data.remote.request.UpDateCard
import org.orbitmvi.orbit.ContainerHost

interface CardSettingsContract {


    interface CardSettingViewModel:ContainerHost<UIState,SideEffect>{
        fun onEventDispatcher(intent: Intent)
    }


    data class  UIState(
        val anything:String? =null
    )

    sealed interface SideEffect{
        data class  OpenDialog(val index:String):SideEffect
    }

    sealed interface Intent{
        data class UpDateCardButton(val card:UpDateCard):Intent

        data class DeleteCardButton(val index:String):Intent
        data class Delete(val index:String):Intent

        data object  Back:Intent
    }
}