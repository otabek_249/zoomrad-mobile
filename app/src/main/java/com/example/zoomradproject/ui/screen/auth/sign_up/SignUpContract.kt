package com.example.zoomradproject.ui.screen.auth.sign_up

import com.example.zoomradproject.data.remote.request.SignUpData
import com.example.zoomradproject.ui.screen.auth.log_in.LogInContract
import org.orbitmvi.orbit.ContainerHost

interface SignUpContract {


    interface SignUpModel : ContainerHost<UIState, SideEffect> {
        fun onEventDispatcher(intent: Intent)
    }

    sealed interface UIState {
        data class Display(val text: String = "") : UIState
    }

    sealed interface SideEffect {
        data class ShowToast(val msg: String) : SideEffect
    }

    sealed interface Intent {
        data class ClickButtonSave(val signUpData: SignUpData) : Intent
        data object ClickTextOpenLogIn : Intent

        data class PhoneNumber(val phone:String): Intent
    }
}