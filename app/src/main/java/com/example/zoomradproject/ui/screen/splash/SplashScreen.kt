package com.example.zoomradproject.ui.screen.splash

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.hilt.getViewModel
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.animateLottieCompositionAsState
import com.airbnb.lottie.compose.rememberLottieComposition
import com.example.zoomradproject.R
import com.example.zoomradproject.ui.theme.MainBgColor
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.delay

class SplashScreen:Screen {
    @Composable
    override fun Content() {
        val viewModel = getViewModel<SplashViewModel>()
        SplashContent (viewModel::onEventDispatcher)
    }
}

@Composable
fun SplashContent(onEventDispatcher: (SplashContract.Intent)->Unit){

    val composition by rememberLottieComposition(LottieCompositionSpec.Asset("splash_white.json"))
    val progress by animateLottieCompositionAsState(
        composition = composition,
        iterations = 1
    )
    Surface(modifier = Modifier.fillMaxSize()) {
        Box(modifier = Modifier.fillMaxSize()) {
            LottieAnimation(
                composition = composition,
                progress = progress,
                modifier = Modifier
                    .align(Alignment.Center)
                    .fillMaxSize(),
            )
            LaunchedEffect(progress) {
                if (progress == 1f) {
                   onEventDispatcher.invoke(SplashContract.Intent.NextScreen)
                }
            }

        }
    }
}



@Composable
@Preview
fun SplashPreview(){
    SplashContent(){}
}