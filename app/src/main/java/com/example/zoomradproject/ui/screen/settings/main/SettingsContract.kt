package com.example.zoomradproject.ui.screen.settings.main

import org.orbitmvi.orbit.ContainerHost

interface SettingsContract {


     interface SettingsViewModel :ContainerHost<UIState,SideEffect>{
         fun onEventDispatcher(intent: Intent)
     }

     sealed interface UIState{
         data object Screen:UIState
     }


     sealed interface SideEffect{

     }

     sealed interface Intent{
         data object BackButton :Intent
     }


}