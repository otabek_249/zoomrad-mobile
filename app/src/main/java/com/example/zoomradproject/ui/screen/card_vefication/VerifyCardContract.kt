package com.example.zoomradproject.ui.screen.card_vefication

import org.orbitmvi.orbit.ContainerHost

interface VerifyCardContract {


    interface VerifyCardModel : ContainerHost<UIState, SideEffect> {
        fun onEventDispatcher(intent: Intent)

    }

    data class UIState(
        val phone:String =""
    )

    sealed interface SideEffect {
        data class ToastSideEffect(val msg: String) : SideEffect

    }

    sealed interface Intent {
        data class ButtonClickSend(val code :String,val token:String) : Intent
        data object First:Intent
    }
}