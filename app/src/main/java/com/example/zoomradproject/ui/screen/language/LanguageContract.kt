package com.example.zoomradproject.ui.screen.language

import org.orbitmvi.orbit.ContainerHost

sealed interface LanguageContract {

    interface LanguageModel : ContainerHost<UIState, SideEffect> {
        fun onEventDispatcher(intent: Intent)

    }

    sealed interface UIState {
        data class Display(val text: String = "") : UIState

    }

    sealed interface SideEffect {
        data class ShowToast(val msg: String) : SideEffect
    }

    sealed interface Intent {
        data class ShowToast(val text: String) : Intent
        data object ClickUzButton : Intent
        data object ClickEngButton : Intent
        data object ClickRusButton : Intent
    }

}