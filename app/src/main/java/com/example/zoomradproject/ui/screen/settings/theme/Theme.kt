package com.example.zoomradproject.ui.screen.settings.theme

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.zoomradproject.R


@Composable
fun ThemeItem(click: (Unit) -> Unit) {
    var isExpanded by rememberSaveable {
        mutableStateOf(false)
    }

    Column(
        modifier = Modifier
            .padding()
            .fillMaxWidth()

    ) {

        Row(
            modifier = Modifier
                .fillMaxWidth()
        ) {
            Image(
                modifier = Modifier
                    .padding(end = 8.dp)
                    .height(24.dp)
                    .width(24.dp),
                painter = painterResource(id = R.drawable.ic_moon),
                contentDescription = "")
            Text(
                text = stringResource(R.string.theme),
                color = Color.Black,
                modifier = Modifier.padding(start = 8.dp),
                fontSize = 16.sp,
                fontFamily = FontFamily(Font(R.font.nata_semibold))
            )
            Spacer(modifier = Modifier.weight(1f))

            if(isExpanded){
                Image(
                    modifier = Modifier
                        .padding(end = 16.dp)
                        .height(24.dp)
                        .width(24.dp)
                        .padding(4.dp)
                        .rotate(180f)
                        .clickable {
                            isExpanded = !isExpanded
                        },

                    painter = painterResource(id = R.drawable.ic_bottom_line),
                    contentDescription = ""
                )
            }
            else {
                Image(
                    modifier = Modifier
                        .padding(end = 16.dp)
                        .height(24.dp)
                        .width(24.dp)
                        .padding(4.dp)
                        .clickable {
                            isExpanded = !isExpanded
                        },

                    painter = painterResource(id = R.drawable.ic_bottom_line),
                    contentDescription = ""
                )
            }
        }



        AnimatedVisibility(visible = isExpanded) {
            Column(
                modifier = Modifier
                    .padding(start = 48.dp)
            ) {
                Spacer(modifier = Modifier.height(4.dp))
                Text(
                    modifier = Modifier
                        .padding(start = 24.dp)
                        .clickable {
                            click.invoke(Unit)
                        },
                    fontFamily = FontFamily(Font(R.font.nata_semibold)),
                    text = "Tizimli",
                    fontSize = 15.sp
                )
                Spacer(modifier = Modifier.height(4.dp))
                Text(
                    modifier = Modifier
                        .padding(start = 24.dp)
                        .clickable {
                            click.invoke(Unit)
                        },
                    fontFamily = FontFamily(Font(R.font.nata_semibold)),
                    text = "Yorqin",
                    fontSize = 15.sp
                )
                Spacer(modifier = Modifier.height(4.dp))
                Text(
                    modifier = Modifier
                        .padding(start = 24.dp)
                        .clickable {
                            click.invoke(Unit)
                        },
                    fontFamily = FontFamily(Font(R.font.nata_semibold)),
                    text = "Kecha",
                    fontSize = 15.sp
                )
                Spacer(modifier = Modifier.height(4.dp))
                Text(
                    modifier = Modifier
                        .padding(start = 24.dp)
                        .clickable {
                            click.invoke(Unit)
                        },
                    fontFamily = FontFamily(Font(R.font.nata_semibold)),
                    text = "Qorong'u",
                    fontSize = 15.sp
                )

            }
        }

    }
}


@Preview
@Composable
fun ThemePreview() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.White)
    ) {
        ThemeItem(){

        }
    }

}