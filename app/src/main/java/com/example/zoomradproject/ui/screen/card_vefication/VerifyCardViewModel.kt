package com.example.zoomradproject.ui.screen.card_vefication

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.zoomradproject.data.pref.MyPref
import com.example.zoomradproject.data.remote.request.VerificationData
import com.example.zoomradproject.repository.auth.impl.AuthRepositoryImpl
import com.example.zoomradproject.repository.main.AppRepository
import com.example.zoomradproject.ui.screen.auth.log_in.LogInContract
import com.example.zoomradproject.ui.screen.auth.verify.VerifyContract
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.syntax.simple.postSideEffect
import org.orbitmvi.orbit.syntax.simple.reduce
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject


@HiltViewModel
class VerifyCardViewModel @Inject constructor(
    private val navigation: VerifyCardDirection,
    private val repo:AppRepository
) : ViewModel(), VerifyCardContract.VerifyCardModel {
    private val myPref = MyPref.getInstance()







    override val container =
        container<VerifyCardContract.UIState, VerifyCardContract.SideEffect>(VerifyCardContract.UIState(""))

    override fun onEventDispatcher(intent: VerifyCardContract.Intent) = intent {

        when (intent) {
           is VerifyCardContract.Intent.ButtonClickSend -> {
               verification(intent.code,intent.token)
            }
            VerifyCardContract.Intent.First->{
                intent{reduce { VerifyCardContract.UIState(myPref.getPhone()) }}
            }
        }
    }

   private fun verification(code: String,token:String){
        repo.transferCardByPanVerify(VerificationData(code = code, token = token))
            .onEach { it ->
                it.onSuccess {
                    navigation.back()
                }
                it.onFailure {

                    if(it.message?.length==0)return@onFailure
                    intent {

                        postSideEffect(VerifyCardContract.SideEffect.ToastSideEffect(it.message.toString())) }
                }
            }
            .launchIn(viewModelScope)
    }


}