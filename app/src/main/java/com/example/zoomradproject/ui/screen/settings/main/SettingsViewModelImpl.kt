package com.example.zoomradproject.ui.screen.settings.main

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import org.orbitmvi.orbit.Container
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject


@HiltViewModel
class SettingsViewModelImpl  @Inject
constructor(
    private val directionImpl: SettingsDirection
) :SettingsContract.SettingsViewModel,ViewModel() {
    override fun onEventDispatcher(intent: SettingsContract.Intent) =intent{

    }


    override val container = container<SettingsContract.UIState,SettingsContract.SideEffect>(SettingsContract.UIState.Screen)
}