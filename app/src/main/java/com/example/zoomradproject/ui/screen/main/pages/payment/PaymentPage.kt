package com.example.zoomradproject.ui.screen.main.pages.payment

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.staggeredgrid.LazyVerticalStaggeredGrid
import androidx.compose.foundation.lazy.staggeredgrid.StaggeredGridCells
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import cafe.adriel.voyager.hilt.getViewModel
import cafe.adriel.voyager.navigator.tab.Tab
import cafe.adriel.voyager.navigator.tab.TabOptions
import com.example.zoomradproject.R
import com.example.zoomradproject.data.model.Item2Data
import com.example.zoomradproject.ui.screen.main.pages.home.CustomLogOutDialog
import com.example.zoomradproject.ui.screen.main.pages.more.MoreContract
import com.example.zoomradproject.ui.theme.MainBgColor
import com.example.zoomradproject.ui.theme.TextColor
import com.example.zoomradproject.utils.ItemPayment
import com.example.zoomradproject.utils.MyDrawer
import kotlinx.coroutines.launch
import org.orbitmvi.orbit.compose.collectAsState
import org.orbitmvi.orbit.compose.collectSideEffect

object PaymentPage : Tab {
    private fun readResolve(): Any = PaymentPage
    override val options: TabOptions
        @Composable
        get() {

            val title = "Payment"
            val icon = painterResource(id = R.drawable.ic_payments)

            return remember {
                TabOptions(
                    index = 1u,
                    title = title,
                    icon = icon
                )
            }
        }

    @Composable
    override fun Content() {
        val viewModel:PaymentContract.PaymentViewModel = getViewModel<PaymentViewModelImpl>()
        val uiState = viewModel.collectAsState().value

        var boolean by remember {
            mutableStateOf(false)
        }
        if(boolean) {
            CustomLogOutDialog(
                onConfirm = {
                    viewModel.onEventDispatcher(PaymentContract.Intent.DialogButtonYes)
                    boolean=false
                }) {
                boolean = false
            }
        }
        viewModel.collectSideEffect {
            when(it){
                PaymentContract.SideEffect.OpenDialog->{
                    boolean = true
                }
            }

        }
        PaymentContent(uiState = uiState, onEventDispatcher = viewModel::onEventDispatcher)
    }
}

val list = arrayListOf<Item2Data>(
    Item2Data(R.drawable.ic_credit_cards, "Kartalarim"),
    Item2Data(R.drawable.ic_shopping, "Onlayn do'kon Sello!"),
    Item2Data(R.drawable.ic_clock_green, "Monitoring"),
    Item2Data(R.drawable.ic_face_scanner, "Identifikatsiya"),
    Item2Data(R.drawable.ic_create_autopay, "Kartalarim"),
    Item2Data(R.drawable.ic_home_green, "Mening uyim"),
    Item2Data(R.drawable.ic_my_avto, "Mening avtomobilim"),
    Item2Data(R.drawable.ic_my_gov_green, "Onlayn davlat xizmatlari"),
    Item2Data(R.drawable.ic_card, "Kartaga buyurtma"),
    Item2Data(R.drawable.ic_credit_cards, "Kartalarim"),
    Item2Data(R.drawable.ic_shopping, "Onlayn do'kon Sello!"),
    Item2Data(R.drawable.ic_clock_green, "Monitoring"),
    Item2Data(R.drawable.ic_face_scanner, "Identifikatsiya"),
    Item2Data(R.drawable.ic_create_autopay, "Kartalarim"),
    Item2Data(R.drawable.ic_home_green, "Mening uyim"),
    Item2Data(R.drawable.ic_my_avto, "Mening avtomobilim"),
    Item2Data(R.drawable.ic_my_gov_green, "Onlayn davlat xizmatlari"),
    Item2Data(R.drawable.ic_card, "Kartaga buyurtma"),
    Item2Data(R.drawable.ic_credit_cards, "Kartalarim"),
    Item2Data(R.drawable.ic_shopping, "Onlayn do'kon Sello!"),
    Item2Data(R.drawable.ic_clock_green, "Monitoring"),
    Item2Data(R.drawable.ic_face_scanner, "Identifikatsiya"),
    Item2Data(R.drawable.ic_create_autopay, "Kartalarim"),
    Item2Data(R.drawable.ic_home_green, "Mening uyim"),
    Item2Data(R.drawable.ic_my_avto, "Mening avtomobilim"),
    Item2Data(R.drawable.ic_my_gov_green, "Onlayn davlat xizmatlari"),
    Item2Data(R.drawable.ic_card, "Kartaga buyurtma"),
    Item2Data(R.drawable.ic_credit_cards, "Kartalarim"),
    Item2Data(R.drawable.ic_shopping, "Onlayn do'kon Sello!"),
    Item2Data(R.drawable.ic_clock_green, "Monitoring"),
    Item2Data(R.drawable.ic_face_scanner, "Identifikatsiya"),
    Item2Data(R.drawable.ic_create_autopay, "Kartalarim"),
    Item2Data(R.drawable.ic_home_green, "Mening uyim"),
    Item2Data(R.drawable.ic_my_avto, "Mening avtomobilim"),
    Item2Data(R.drawable.ic_my_gov_green, "Onlayn davlat xizmatlari"),
    Item2Data(R.drawable.ic_card, "Kartaga buyurtma"),

    )

@Composable
fun PaymentContent(
    uiState: PaymentContract.UIState,
    onEventDispatcher: (PaymentContract.Intent) -> Unit
) {

    var selectedItemIndex by rememberSaveable {
        mutableIntStateOf(0)
    }

    val drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)
    val scope = rememberCoroutineScope()
    ModalNavigationDrawer(
        drawerState = drawerState, drawerContent = {
            MyDrawer { index ->
                selectedItemIndex = index
                if(index==7){
                    onEventDispatcher(PaymentContract.Intent.OpenDialog)
                }
                else {
                    onEventDispatcher.invoke(PaymentContract.Intent.OpenDrawer(index))
                }

            }
        }, gesturesEnabled = true
    ) {
        Scaffold(
            modifier = Modifier
                .fillMaxSize(),
            contentColor = MainBgColor
        ) { padding ->
            Column(
                modifier = Modifier
                    .padding(padding)
                    .fillMaxSize()
            ) {
                Row(
                    modifier = Modifier
                        .padding(start = 16.dp, end = 16.dp, top = 24.dp),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Image(
                        modifier = Modifier
                            .clickable {
                                scope.launch {
                                    drawerState.apply {
                                        if (isClosed) open() else close()
                                    }
                                }
                            }
                            .height(24.dp)
                            .width(24.dp)
                            .padding(3.dp),
                        painter = painterResource(id = R.drawable.ic_menu),
                        contentDescription = "",
                    )
                    Spacer(modifier = Modifier.weight(1f))
                    Text(
                        text = stringResource(R.string.more_text),
                        fontSize = 18.sp,
                        color = Color(0xFFB7B7B7),
                        fontFamily = FontFamily(Font(R.font.nata_semibold)),
                        fontWeight = FontWeight(300)
                    )
                    Spacer(modifier = Modifier.weight(1f))


                    Image(
                        modifier = Modifier
                            .width(24.dp)
                            .height(24.dp)
                            .padding(2.dp),
                        painter = painterResource(id = R.drawable.ic_notification),
                        contentDescription = ""
                    )
                }
                Text(
                    modifier = Modifier
                        .padding(top = 8.dp, start = 16.dp),
                    fontSize = 14.sp,
                    fontFamily = FontFamily(Font(R.font.nata_semibold)),
                    color = TextColor,
                    text = "Barcha servislar"
                )

                LazyVerticalStaggeredGrid(
                    columns = StaggeredGridCells.Fixed(2),
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(start = 4.dp, end = 4.dp, bottom = 56.dp),
                    contentPadding = PaddingValues(16.dp),
                    horizontalArrangement = Arrangement.spacedBy(16.dp),
                    verticalItemSpacing = 18.dp
                ) {
                    list.forEach { data ->
                        item {
                            ItemPayment(text = data.msg, image = data.image)
                        }
                    }
                }


            }

        }
    }
}


@Composable
@Preview
fun PaymentPreview() {
    PaymentContent(PaymentContract.UIState(null)){}

}

