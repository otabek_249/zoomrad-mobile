package com.example.zoomradproject.ui.screen.auth.password2

import android.annotation.SuppressLint
import android.content.Context
import android.widget.Toast
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.hilt.getViewModel
import com.example.zoomradproject.R
import com.example.zoomradproject.data.pref.MyPref
import com.example.zoomradproject.ui.screen.auth.password.PasswordViewModel
import com.example.zoomradproject.ui.theme.MainBgColor
import com.example.zoomradproject.ui.theme.TextColor
import com.example.zoomradproject.utils.CustomCircleOne
import com.example.zoomradproject.utils.CustomCircleTwo
import com.example.zoomradproject.utils.NumberText
import com.example.zoomradproject.utils.myLog
import org.orbitmvi.orbit.compose.collectAsState

class Password2Screen : Screen {
    @Composable
    override fun Content() {
        val model: Password2Contract.PasswordViewModel = getViewModel<Password2ViewModel>()
        val uiState = model.collectAsState().value
        PasswordContent(uiState, model::onEventDispatcher)
    }
}

@SuppressLint("SuspiciousIndentation")
@Composable
fun PasswordContent(
    uiState: Password2Contract.UIState, onEventDispatcher: (Password2Contract.Intent) -> Unit = {}
) {



    val context = LocalContext.current
    var password by remember { mutableStateOf("") }
    val myPref = MyPref.getInstance()


    val listBoolean =
        remember { mutableStateListOf<Boolean>().apply { repeat(4) { this.add(false) } } }

    var index by remember {
        mutableIntStateOf(0)
    }


    var boolean by remember {
        mutableStateOf(true)
    }



    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(MainBgColor)
    ) {
        if (boolean) {
            check(context) {

            }
            boolean = false

        }
        if(password.length == 4  && password==myPref.getPassword()){

            onEventDispatcher.invoke(Password2Contract.Intent.OpenMainScreen)

        }



        Box(
            modifier = Modifier
                .weight(0.3f)
                .fillMaxWidth(),
            contentAlignment = Alignment.BottomCenter
        ) {
            Image(
                painter = painterResource(id = R.drawable.ic_zoomrad),
                contentDescription = "",
                modifier = Modifier
                    .height(100.dp)
                    .width(100.dp)
            )

        }

        Column(
            modifier = Modifier
                .weight(1f)
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Spacer(modifier = Modifier.height(48.dp))

            Text(
                text = "Pin kodni kiriting",
                fontSize = 18.sp,
                color = TextColor,
                fontFamily = FontFamily(Font(R.font.nata_bold)),
                fontWeight = FontWeight(600)
            )


            Spacer(modifier = Modifier.height(24.dp))
            Row(
                modifier = Modifier
                    .height(32.dp)
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {

                for (i in 0 until 4) {
                    if (i < index) {
                        CustomCircleTwo()
                    } else {
                        CustomCircleOne()
                    }
                }
            }
            Spacer(modifier = Modifier.weight(1f))
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(4f)
            ) {
                Row(
                    modifier = Modifier
                        .padding(horizontal = 48.dp)
                        .fillMaxWidth()
                        .weight(1f),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    NumberText(number = "1") {
                        if (index >= 4) return@NumberText
                        index += 1


                        password += "1"
                        Toast.makeText(context, "1", Toast.LENGTH_SHORT).show()
                        boolean = false

                    }
                    NumberText(number = "2") {
                        if (index > 4) return@NumberText
                        index += 1


                        password += "2"
                    }
                    NumberText(number = "3") {
                        if (index > 4) return@NumberText
                        index += 1


                        password += "3"
                    }

                }
                Row(
                    modifier = Modifier
                        .padding(horizontal = 48.dp)
                        .fillMaxWidth()
                        .weight(1f),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    NumberText(number = "4") {
                        if (index > 4) return@NumberText
                        index += 1


                        password += "4"
                    }
                    NumberText(number = "5") {
                        if (index > 4) return@NumberText
                        index += 1


                        password += "5"
                    }
                    NumberText(number = "6") {
                        if (index > 4) return@NumberText
                        index += 1


                        password += "6"
                    }

                }
                Row(
                    modifier = Modifier
                        .padding(horizontal = 48.dp)
                        .fillMaxWidth()
                        .weight(1f),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    NumberText(number = "7") {
                        if (index > 4) return@NumberText
                        index += 1


                        password += "7"
                    }
                    NumberText(number = "8") {
                        if (index > 4) return@NumberText
                        index += 1


                        password += "8"
                    }
                    NumberText(number = "9") {
                        if (index > 4) return@NumberText
                        index += 1


                        password += "9"
                    }
                }

                Row(
                    modifier = Modifier
                        .padding(horizontal = 48.dp)
                        .fillMaxWidth()
                        .weight(1f),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Spacer(
                        modifier = Modifier
                            .height(48.dp)
                            .width(48.dp)
                    )

                    NumberText(number = "0") {
                        if (index > 4) return@NumberText
                        index += 1
                        listBoolean[index] = true

                        password += "0"
                    }
                    Image(painter = painterResource(id = R.drawable.ic_number_delete),
                        contentDescription = "",
                        modifier = Modifier
                            .clickable {
                                if (password.isEmpty()) return@clickable
                                index--
                                password = password.substring(0, password.length - 1)
                            }
                            .height(48.dp)
                            .width(48.dp)
                            .padding(8.dp))

                }
                Spacer(modifier = Modifier.height(16.dp))

            }
        }

    }


}

fun displayMessage(s: String) {
    println(s)
}


@Composable
@Preview

fun PasswordPreview() {
    PasswordContent(Password2Contract.UIState.Screen)
}


fun check(context: Context, click: (Unit) -> Unit) {
    lateinit var biometricPrompt: BiometricPrompt


    val biometricManager = BiometricManager.from(context)
    when (biometricManager.canAuthenticate()) {
        BiometricManager.BIOMETRIC_SUCCESS -> displayMessage("Biometric authentication is available")

        BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> displayMessage("This device doesn't support biometric authentication")

        BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> displayMessage("Biometric authentication is currently unavailable")

        BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> displayMessage("No biometric credentials are enrolled")
        BiometricManager.BIOMETRIC_ERROR_SECURITY_UPDATE_REQUIRED -> {

        }

        BiometricManager.BIOMETRIC_ERROR_UNSUPPORTED -> {
            TODO()
        }

        BiometricManager.BIOMETRIC_STATUS_UNKNOWN -> {
            TODO()
        }
    }

    val executor = ContextCompat.getMainExecutor(context)
    biometricPrompt = BiometricPrompt(
        context as FragmentActivity,
        executor,
        object : BiometricPrompt.AuthenticationCallback() {
            override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                super.onAuthenticationError(errorCode, errString)
                displayMessage("Authentication error: $errString")
            }

            override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                super.onAuthenticationSucceeded(result)
                displayMessage("Authentication succeeded!")
                click.invoke(Unit)
            }

            override fun onAuthenticationFailed() {
                super.onAuthenticationFailed()
                displayMessage("Authentication failed")
            }
        })

    val promptInfo: BiometricPrompt.PromptInfo =
        BiometricPrompt.PromptInfo.Builder().setTitle("Biometric Authentication")
            .setSubtitle("Log in using your biometric credential").setNegativeButtonText("Cancel")
            .build()
    biometricPrompt.authenticate(promptInfo)
}