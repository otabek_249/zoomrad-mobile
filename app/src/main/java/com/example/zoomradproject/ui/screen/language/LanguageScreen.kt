package com.example.zoomradproject.ui.screen.language

import android.content.Context
import android.content.res.Resources
import android.util.DisplayMetrics
import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.recreate
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.hilt.getViewModel
import cafe.adriel.voyager.navigator.CurrentScreen
import com.example.zoomradproject.R
import com.example.zoomradproject.ui.theme.MainBgColor
import com.example.zoomradproject.utils.ButtonLanguage
import org.orbitmvi.orbit.compose.collectAsState
import org.orbitmvi.orbit.compose.collectSideEffect
import java.util.Locale

class LanguageScreen : Screen {
    @Composable
    override fun Content() {


        val model: LanguageContract.LanguageModel = getViewModel<LanguageViewModel>()
        val uiState = model.collectAsState().value
        val context = LocalContext.current
        model.collectSideEffect {
            when (it) {
                is LanguageContract.SideEffect.ShowToast -> {
                    Toast.makeText(context, it.msg, Toast.LENGTH_SHORT).show()
                }
            }
        }

        /*SplashScreenContent(uiState = uiSate, model::onEventDispatcher)*/
        LanguageContent(uiState, model::onEventDispatcher)
    }
}


@Composable
fun LanguageContent(
    uiState: LanguageContract.UIState,
    onEventDispatcher: (LanguageContract.Intent) -> Unit = {}
) {
    val context = LocalContext.current
    var language by remember {
        mutableStateOf("uz")
    }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MainBgColor),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .weight(0.3f),
            contentAlignment = Alignment.BottomCenter
        ) {
            Image(
                painter = painterResource(id = R.drawable.ic_zoomrad),
                contentDescription = "",
                modifier = Modifier
                    .height(100.dp)
                    .width(100.dp)
            )

        }
        Column(
            modifier = Modifier
                .weight(1f)
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Spacer(modifier = Modifier.height(48.dp))
            ButtonLanguage(language = R.string.uzbek, horizontalPadding = 24) {
                setLocate2(context,"uz")
                onEventDispatcher.invoke(LanguageContract.Intent.ClickUzButton)
                onEventDispatcher.invoke(LanguageContract.Intent.ShowToast("O'zbekcha tanlandi"))
            }
            Spacer(modifier = Modifier.height(16.dp))
            ButtonLanguage(language = R.string.russian, horizontalPadding = 24)
            {
                setLocate2(context,"ru")
                onEventDispatcher.invoke(LanguageContract.Intent.ClickRusButton)
                onEventDispatcher.invoke(LanguageContract.Intent.ShowToast("Русский выбран"))

            }
            Spacer(modifier = Modifier.height(16.dp))
            ButtonLanguage(language = R.string.english, horizontalPadding = 24) {
                setLocate2(context,"eng")
                onEventDispatcher.invoke(LanguageContract.Intent.ClickEngButton)
                onEventDispatcher.invoke(LanguageContract.Intent.ShowToast("English selected"))
            }


        }


    }

}




@Composable
@Preview(device = Devices.PIXEL_3_XL)
fun LanguagePreview() {
    LanguageContent(LanguageContract.UIState.Display(""))
}

fun setLocate2(context: Context, language: String) {
    val resources: Resources = context.resources
    val configuration = resources.configuration
    configuration.setLocale(Locale(language))
    resources.updateConfiguration(configuration, resources.displayMetrics)

}


fun setLocate(context: Context, language: String) {
    val resources: Resources = context.resources
    val configuration = resources.configuration
    configuration.setLocale(Locale(language))
    resources.updateConfiguration(configuration, resources.displayMetrics)
    recreate(context as FragmentActivity)
}