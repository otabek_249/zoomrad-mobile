package com.example.zoomradproject.ui.activity

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.lifecycleScope
import cafe.adriel.voyager.navigator.CurrentScreen
import cafe.adriel.voyager.navigator.Navigator
import cafe.adriel.voyager.navigator.bottomSheet.BottomSheetNavigator
import com.example.zoomradproject.R
import com.example.zoomradproject.data.pref.MyPref
import com.example.zoomradproject.ui.screen.auth.log_in.LogInScreen
import com.example.zoomradproject.ui.screen.auth.password.PasswordScreen
import com.example.zoomradproject.ui.screen.auth.password2.Password2Screen
import com.example.zoomradproject.ui.screen.auth.sign_up.SignUpScreen
import com.example.zoomradproject.ui.screen.auth.verify.VerifyScreen
import com.example.zoomradproject.ui.screen.language.LanguageScreen
import com.example.zoomradproject.ui.screen.splash.SplashScreen
import com.example.zoomradproject.ui.theme.GreenColor
import com.example.zoomradproject.ui.theme.MainBgColor
import com.example.zoomradproject.ui.theme.ZoomradProjectTheme
import com.example.zoomradproject.utils.NetworkStatusValidator
import com.example.zoomradproject.utils.navigation.NavigationHandler
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@OptIn(ExperimentalMaterialApi::class)
@AndroidEntryPoint
class MainActivity : FragmentActivity() {

    @Inject
    lateinit var networkStatusValidator: NetworkStatusValidator

    @Inject
    lateinit var navigationHandler: NavigationHandler

    @SuppressLint("CoroutineCreationDuringComposition")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var isNetworkAvailable by mutableStateOf(false)
        var shouldShowDialog by mutableStateOf(false)

        networkStatusValidator.init(
            availableNetworkBlock = {
                isNetworkAvailable = true
            },
            lostConnection = {
                isNetworkAvailable = false
            }
        )

        setContent {
            ZoomradProjectTheme {
                BottomSheetNavigator(
                    sheetShape = RoundedCornerShape(topStart = 16.dp, topEnd = 16.dp)
                ) {
                    Navigator(screen = SplashScreen()) { navigator ->
                        val currentScreen = navigator.lastItem

                        shouldShowDialog = when (currentScreen) {
                            is SplashScreen, is Password2Screen, is PasswordScreen,is LanguageScreen-> false
                            else -> true
                        }

                        navigationHandler.navigationStack
                            .onEach { it.invoke(navigator) }
                            .launchIn(lifecycleScope)

                        CurrentScreen()

                        if (!isNetworkAvailable && shouldShowDialog) {
                            CustomErrorDialog(
                                onConfirm = { /* Handle Confirm Action */ },
                                onDismiss = {},
                                isNetworkAvailable)
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun NoInternetDialog() {
    AlertDialog(
        onDismissRequest = { },
        confirmButton = {
            TextButton(onClick = { /* Handle Confirm Action */ }) {
                Text("OK")
            }
        },
        title = {
            Image(
                modifier = Modifier
                    .height(36.dp)
                    .width(36.dp),
                painter = painterResource(id = R.drawable.ic_zoomrad),
                contentDescription = ""
            )
            Text(text = "No Internet Connection")
        },
        text = {
            Text("Please check your internet connection and try again.")
        }
    )
}

@Composable
fun CustomDeleteDialog(
    onConfirm: () -> Unit,
    onDismiss: () -> Unit,

) {
    Dialog(onDismissRequest = { onDismiss() }) {
        Surface(
            shape = RoundedCornerShape(8.dp),
            color = MainBgColor,
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .padding(16.dp)
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp)
            ) {
                Text(
                    text = "Haqiqatan ham bu kartani oʻchirib tashlamoqchimisiz?",
                    fontSize = 16.sp,
                    fontWeight = FontWeight.Bold
                )
                Spacer(modifier = Modifier.height(16.dp))
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.End
                ) {
                    OutlinedButton(onClick = { onDismiss() }) {
                        Text(text = "Yuq")
                    }
                    Spacer(modifier = Modifier.width(8.dp))
                    Button(
                        onClick = { onConfirm() },
                        colors = ButtonDefaults.buttonColors( Color(0xFF00C853))
                    ) {
                        Text(text = "Ha", color = Color.White)
                    }
                }
            }
        }
    }
}

@Composable
fun CustomErrorDialog(
    onConfirm: () -> Unit,
    onDismiss: () -> Unit,
    boolean: Boolean
) {
    if (boolean){
        onDismiss()
    }
    AlertDialog(
        onDismissRequest = { onDismiss() },

        backgroundColor = MainBgColor,
        shape = RoundedCornerShape(16.dp),
        buttons = {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Image(
                    painter = painterResource(id = R.drawable.image_internet_lost),
                    contentDescription = null,
                    modifier = Modifier
                        .size(200.dp)
                        .padding(16.dp)
                )
                Text(
                    text = "Ko'zda tutilmagan xatolik. Qaytadan urinib ko'ring",
                    fontSize = 16.sp,
                    textAlign = TextAlign.Center,
                    fontWeight = FontWeight.Normal,
                    color = Color.Black,
                    modifier = Modifier.padding(vertical = 16.dp)
                )
                Button(
                    onClick = { /*onConfirm()*/ onDismiss()},
                    colors = ButtonDefaults.buttonColors(GreenColor),
                    shape = RoundedCornerShape(12.dp),
                    modifier = Modifier

                        .fillMaxWidth()
                        .padding(horizontal = 16.dp)
                ) {
                    Text(text = "Takrorlash", color = Color.White)
                }
            }
        }
    )
}
