package com.example.zoomradproject.ui.screen.main.pages.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.zoomradproject.data.pref.MyPref
import com.example.zoomradproject.repository.main.AppRepository
import com.example.zoomradproject.utils.myLog
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.syntax.simple.postSideEffect
import org.orbitmvi.orbit.syntax.simple.reduce
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject


@HiltViewModel
class HomeViewModelImpl  @Inject constructor(

    private val navigation :HomeDirection,
    private val repo :AppRepository

):ViewModel(),HomeContract.HomeViewModel{

    private val myPref = MyPref.getInstance()
    init {
        getCardsList()
        "is work getCardList init".myLog()
    }

    override val container = container<HomeContract.UIState,HomeContract.SideEffect>(HomeContract.UIState())

    override fun onEventDispatcher(intent: HomeContract.Intent)  = intent{
        when(intent){
            HomeContract.Intent.OpenAddCart->{
                navigation.openAddCart()
            }
            is HomeContract.Intent.OpenDrawer->{
                if(intent.index==7){
                    postSideEffect(HomeContract.SideEffect.OpenDialog)
                }
                navigation.openDrawer(intent.index)
            }
            HomeContract.Intent.OpenCardsScreen->{
                navigation.openCardsScreen()
            }
            HomeContract.Intent.Refresh->{
                getCardsList()
            }
            HomeContract.Intent.OpenDialog->{
                postSideEffect(HomeContract.SideEffect.OpenDialog)
            }
            HomeContract.Intent.DialogButtonYes->{
                myPref.setAccessToke("otabek")
                myPref.setRefreshToken("otabek")
                myPref.setCardIndex(0)
                myPref.setPassword("")
                navigation.logOut()
            }
        }

    }

    private fun getCardsList(){
        repo.getCards()
            .onEach {
                it.onSuccess {list->
                    //container.stateFlow.value.cardsResponse = list
                    intent { reduce { HomeContract.UIState(list) } }
                    "getCardsList size viewModel ${list.size}".myLog()

                }
                it.onFailure {

                }
            }
            .launchIn(viewModelScope)
    }

}