package com.example.zoomradproject.ui.screen.cardsettings

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.zoomradproject.data.remote.request.UpDateCard
import com.example.zoomradproject.repository.main.AppRepository
import com.example.zoomradproject.ui.screen.settings.main.SettingsDirection
import com.example.zoomradproject.ui.screen.settings.main.SettingsDirectionImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import org.orbitmvi.orbit.Container
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.syntax.simple.postSideEffect
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject


@HiltViewModel
class CardSettingsViewModelImpl @Inject constructor(
    private val repo:AppRepository,
    private val direction: SettingsDirection
):CardSettingsContract.CardSettingViewModel,ViewModel() {


    override val container = container<CardSettingsContract.UIState,CardSettingsContract.SideEffect>(CardSettingsContract.UIState())

    override fun onEventDispatcher(intent: CardSettingsContract.Intent)  = intent{
        when(intent){
            is CardSettingsContract.Intent.DeleteCardButton->{
                deleteCard(intent.index)
            }
            is CardSettingsContract.Intent.UpDateCardButton->{
                updateCard(intent.card)
            }
            CardSettingsContract.Intent.Back->{
                direction.back()
            }
            is CardSettingsContract.Intent.Delete->{
                postSideEffect(CardSettingsContract.SideEffect.OpenDialog(intent.index))
            }
        }

    }

    private fun deleteCard(id:String){
        repo.deleteCard(id)
            .onEach {
                it.onSuccess {
                    direction.back()
                }
                it.onFailure {

                }
            }
            .launchIn(viewModelScope)
    }

    private fun updateCard(card:UpDateCard){
        repo.update(card)
            .onEach {
                it.onSuccess {
                    direction.back()
                }
                it.onFailure {  }
            }
            .launchIn(viewModelScope)
    }

}