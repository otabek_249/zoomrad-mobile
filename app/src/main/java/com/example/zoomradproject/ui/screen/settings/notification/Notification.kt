package com.example.zoomradproject.ui.screen.settings.notification

import android.annotation.SuppressLint
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Slider
import androidx.compose.material3.SliderDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.zoomradproject.R
import com.example.zoomradproject.ui.theme.GreenColor


@OptIn(ExperimentalMaterial3Api::class)
@SuppressLint("UnrememberedMutableInteractionSource")
@Composable
fun NotificationItem(click: (Unit) -> Unit) {
    val interactionSource = MutableInteractionSource()
    var isExpanded by rememberSaveable {
        mutableStateOf(false)
    }
    var sliderValue by remember {
        mutableFloatStateOf(0f)
    }
    var sliderValue2 by remember {
        mutableFloatStateOf(0f)
    }
    Column(
        modifier = Modifier

            .fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {


        Row(
            modifier = Modifier
                .fillMaxWidth()
        ) {
            Image(
                modifier = Modifier
                    .padding(end = 8.dp)
                    .height(24.dp)
                    .width(24.dp),
                painter = painterResource(id = R.drawable.ic_notification),
                contentDescription = ""
            )
            Text(
                text = stringResource(R.string.safety),
                color = Color.Black,
                modifier = Modifier.padding(start = 8.dp),
                fontSize = 16.sp,
                fontFamily = FontFamily(Font(R.font.nata_bold))
            )
            Spacer(modifier = Modifier.weight(1f))

            if (isExpanded) {
                Image(
                    modifier = Modifier
                        .padding(end = 16.dp)
                        .height(22.dp)
                        .width(22.dp)
                        .padding(4.dp)
                        .rotate(180f)
                        .clickable {
                            isExpanded = !isExpanded
                        },

                    painter = painterResource(id = R.drawable.ic_bottom_line),
                    contentDescription = ""
                )
            } else {
                Image(
                    modifier = Modifier
                        .padding(end = 16.dp)
                        .height(22.dp)
                        .width(22.dp)
                        .padding(4.dp)
                        .clickable {
                            isExpanded = !isExpanded
                        },

                    painter = painterResource(id = R.drawable.ic_bottom_line),
                    contentDescription = ""
                )
            }

        }

        AnimatedVisibility(visible = isExpanded) {
            Column(
                modifier = Modifier
                    .padding(start = 48.dp)
            ) {
                Spacer(modifier = Modifier.height(4.dp))
                Row (
                    verticalAlignment = Alignment.CenterVertically
                ){
                    Text(
                        text = "O'tkazmalar haqida",
                        color = Color.Black,
                        modifier = Modifier.padding(start = 8.dp),
                        fontSize = 15.sp,
                        fontFamily = FontFamily(Font(R.font.nata_semibold))
                    )
                    Spacer(modifier = Modifier.weight(1f))
                    Slider(
                        modifier = Modifier
                            .height(40.dp)
                            .width(60.dp),
                        value =sliderValue, onValueChange ={
                            sliderValue = it
                        },
                        thumb = {


                            SliderDefaults.Thumb(
                                colors = SliderDefaults.colors(GreenColor),
                                interactionSource = interactionSource,
                                thumbSize = DpSize(25.dp, 25.dp)
                            )
                        }
                    )

                }
                Spacer(modifier = Modifier.height(4.dp))
                Row (
                    verticalAlignment = Alignment.CenterVertically
                ){
                    Text(
                        text = "Avtoto'lov status haqida",
                        color = Color.Black,
                        modifier = Modifier.padding(start = 8.dp),
                        fontSize = 15.sp,
                        fontFamily = FontFamily(Font(R.font.nata_semibold))
                    )
                    Spacer(modifier = Modifier.weight(1f))
                    Slider(
                        modifier = Modifier
                            .height(40.dp)
                            .width(60.dp),
                        value =sliderValue2, onValueChange ={
                            sliderValue2 = it
                        },
                        thumb = {


                            SliderDefaults.Thumb(
                                colors = SliderDefaults.colors(GreenColor),
                                interactionSource = interactionSource,
                                thumbSize = DpSize(25.dp, 25.dp)
                            )
                        }
                    )


                }
                Spacer(modifier = Modifier.height(4.dp))


            }
        }

    }
}


@Preview
@Composable
fun NotificationPreview() {
    NotificationItem() {

    }
}