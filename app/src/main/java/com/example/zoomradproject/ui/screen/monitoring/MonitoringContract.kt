package com.example.zoomradproject.ui.screen.monitoring

import com.example.zoomradproject.data.remote.response.MonitoringResponse
import org.orbitmvi.orbit.ContainerHost

interface MonitoringContract {


    interface MonitoringViewModel:ContainerHost<UIState,SideEffect>{
        fun onEventDispatcher(intent:Intent)
    }

    data class UIState(
        val data:MonitoringResponse ?= null,
        val income:Long=0,
        val outcome :Long =0
    )


    sealed interface SideEffect{

    }

    sealed interface Intent{
        data object Back:Intent
    }
}