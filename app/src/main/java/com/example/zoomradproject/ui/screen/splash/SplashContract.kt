package com.example.zoomradproject.ui.screen.splash

import org.orbitmvi.orbit.ContainerHost

interface SplashContract {

    interface SplashViewModel :ContainerHost<UIState, SideEffect>{
        fun onEventDispatcher(intent: Intent)
    }

    sealed interface UIState{
        data object Screen: UIState
    }
    sealed interface  SideEffect{

    }
    sealed interface Intent{
        data object NextScreen: Intent
    }
}