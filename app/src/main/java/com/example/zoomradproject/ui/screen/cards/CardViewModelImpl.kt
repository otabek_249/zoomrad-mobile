package com.example.zoomradproject.ui.screen.cards

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.zoomradproject.data.pref.MyPref
import com.example.zoomradproject.data.remote.response.CardsResponse
import com.example.zoomradproject.repository.main.AppRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.syntax.simple.postSideEffect
import org.orbitmvi.orbit.syntax.simple.reduce
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject


@HiltViewModel
class CardViewModelImpl @Inject constructor(
    private val directionImpl: CardsDirection,
    private val repo: AppRepository
) : CardsContract.CardsViewModel, ViewModel() {
    private val myPref = MyPref.getInstance()


    override val container =
        container<CardsContract.UIState, CardsContract.SideEffect>(CardsContract.UIState())

    override fun onEventDispatcher(intent: CardsContract.Intent) = intent {
        when (intent) {
            CardsContract.Intent.Back -> {
                directionImpl.back()
            }
            CardsContract.Intent.OpenCarsScreen->{
                directionImpl.openAddScreen()
            }
            is CardsContract.Intent.ClickCards->{
                postSideEffect(CardsContract.SideEffect.ClickCards(intent.cardsResponse))
            }
            is CardsContract.Intent.SetIndex->{
                myPref.setCardIndex(intent.index)
            }
            CardsContract.Intent.OpenMonitoringScreen->{
                directionImpl.openMonitoring()
            }
            is CardsContract.Intent.OpenCardsSettingsScreen->{
                directionImpl.openSettingsCard(intent.cardsResponse)
            }
            is CardsContract.Intent.DeleteCard->{
                deleteCard(intent.id)
            }

        }

    }
    init {
        getAllCards()
    }

    private fun deleteCard(id:String){
        repo.deleteCard(id)
            .onEach {
                it.onSuccess {
                    getAllCards()
                }
                it.onFailure {

                }
            }
            .launchIn(viewModelScope)
    }

   private fun getAllCards(){
        repo.getCards()
            .onEach {
                it.onSuccess { list->
                    intent { reduce { CardsContract.UIState(list) } }
                }
                it.onFailure {

                }
            }
            .launchIn(viewModelScope)
    }


}