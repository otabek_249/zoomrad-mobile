package com.example.zoomradproject.ui.screen.cards

import com.example.zoomradproject.data.remote.response.CardsResponse
import com.example.zoomradproject.ui.screen.add_card.AddCardScreen
import com.example.zoomradproject.ui.screen.cardsettings.CardSettingsScreen
import com.example.zoomradproject.ui.screen.monitoring.MonitoringScreen
import com.example.zoomradproject.ui.screen.settings.main.SettingsScreen
import com.example.zoomradproject.utils.navigation.AppNavigator
import javax.inject.Inject


interface CardsDirection{
    suspend fun back()
    suspend fun openAddScreen()
    suspend fun openMonitoring()
    suspend fun openSettingsCard(card:CardsResponse)
}
class CardsDirectionImpl @Inject constructor(
    private val navigator: AppNavigator
) :CardsDirection {

    override suspend fun openSettingsCard(card: CardsResponse) {
        navigator.replace(CardSettingsScreen(card))
    }
    override suspend fun openMonitoring() {
        navigator.replace(MonitoringScreen())
    }
    override suspend fun back() {
        navigator.back()
    }

    override suspend fun openAddScreen() {
        navigator.navigateTo(AddCardScreen())
    }
}