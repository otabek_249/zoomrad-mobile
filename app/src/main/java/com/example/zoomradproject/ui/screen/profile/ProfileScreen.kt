package com.example.zoomradproject.ui.screen.profile

import android.annotation.SuppressLint
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Scaffold
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.hilt.getViewModel
import com.example.zoomradproject.R
import com.example.zoomradproject.data.pref.MyPref
import com.example.zoomradproject.ui.theme.EditTextBgColor
import com.example.zoomradproject.ui.theme.GreenColor
import com.example.zoomradproject.ui.theme.MainBgColor
import org.orbitmvi.orbit.compose.collectAsState

class ProfileScreen : Screen {
    @Composable
    override fun Content() {
        val viewModel :ProfileContract.ProfileViewModel = getViewModel<ProfileViewModelImpl>()

        val uiState = viewModel.collectAsState().value

        ProfileContent(uiState =uiState, onEventDispatcher = viewModel::onEventDispatcher )
    }
}


@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun ProfileContent(uiState: ProfileContract.UIState ,onEventDispatcher:(ProfileContract.Intent)->Unit) {
    val myPref = MyPref.getInstance()
    var name by remember {
        mutableStateOf("")
    }
    name= myPref.getFirstName()

    var lastName by remember {
        mutableStateOf("")
    }
    lastName= myPref.getFLastName()
    Scaffold(
        topBar = {
            Row(
                modifier = Modifier
                    .background(MainBgColor)
                    .padding(top = 16.dp)
                    .height(56.dp)
                    .fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Center

            ) {
                Image(
                    modifier = Modifier
                        .padding(start = 16.dp)
                        .height(24.dp)
                        .width(24.dp)
                        .padding(2.dp)
                        .clickable {
                                   onEventDispatcher.invoke(ProfileContract.Intent.Back)
                        },
                    painter = painterResource(id = R.drawable.ic_back_green),
                    contentDescription = ""
                )
                Text(
                    modifier = Modifier
                        .weight(1f),

                    textAlign = TextAlign.Center,
                    fontFamily = FontFamily(Font(R.font.nata_bold)),
                    fontSize = 18.sp,
                    text = stringResource(id = R.string.profile)
                )
                Spacer(modifier = Modifier.width(40.dp))

            }
        },
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(MainBgColor),
            horizontalAlignment = Alignment.CenterHorizontally,


            ) {
            Box(
                contentAlignment = Alignment.BottomCenter
            ) {
                Image(
                    modifier = Modifier
                        .padding(bottom = 6.dp)
                        .width(156.dp)
                        .height(156.dp)
                        .clip(shape = CircleShape),

                    contentScale = ContentScale.Crop,

                    painter = painterResource(id = R.drawable.img_user),
                    contentDescription = ""
                )
                Box(
                    modifier = Modifier
                        .background(color = GreenColor, shape = CircleShape)
                        .size(48.dp),
                    contentAlignment = Alignment.Center
                ) {
                    Image(
                        modifier = Modifier
                            .padding(2.dp)
                            .size(32.dp),
                        painter = painterResource(id = R.drawable.ic_camera),
                        contentDescription = ""
                    )
                }

            }

            Text(
                modifier = Modifier
                    .padding(top = 16.dp),
                fontSize = 24.sp,
                fontFamily = FontFamily(Font(R.font.nata_semibold)),
                text = "$name $lastName".uppercase()
            )

            Button(

                modifier = Modifier
                    .padding(start = 24.dp, end = 24.dp)
                    .height(48.dp)
                    .fillMaxWidth(),
                shape = RoundedCornerShape(12.dp),
                colors = ButtonDefaults.buttonColors(GreenColor),
                onClick = { /*TODO*/ })
            {
                Row(
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(
                        fontFamily = FontFamily(Font(R.font.nata_semibold)),
                        fontSize = 16.sp,
                        text = "Tasdiqlangan mijoz"
                    )
                    Spacer(modifier = Modifier.width(8.dp))
                    Image(
                        modifier = Modifier
                            .size(24.dp),
                        painter = painterResource(id = R.drawable.ic_question),
                        contentDescription = ""
                    )
                }

            }

            OutlinedTextField(
                modifier = Modifier
                    .padding(start = 24.dp, end = 24.dp, top = 18.dp)
                    .height(52.dp)
                    .shadow(4.dp, RoundedCornerShape(12.dp))
                    .fillMaxWidth(),
                shape = RoundedCornerShape(12.dp),


                value = name.uppercase(),
                textStyle = TextStyle(
                    fontFamily = FontFamily(Font(R.font.nata_bold)),
                    fontSize = 18.sp
                ),
                onValueChange = {
                },


                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = EditTextBgColor,
                    cursorColor = Color.Black,
                    focusedIndicatorColor = GreenColor,
                    unfocusedIndicatorColor = EditTextBgColor,
                    placeholderColor = Color(0xFFB2B2B2)
                ),
                maxLines = 1,

                )
            OutlinedTextField(
                modifier = Modifier
                    .padding(start = 24.dp, end = 24.dp, top = 18.dp)
                    .height(52.dp)
                    .shadow(4.dp, RoundedCornerShape(12.dp))
                    .fillMaxWidth(),
                shape = RoundedCornerShape(12.dp),


                value = lastName.uppercase(),
                textStyle = TextStyle(
                    fontFamily = FontFamily(Font(R.font.nata_bold)),
                    fontSize = 18.sp
                ),
                onValueChange = {
                },


                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = EditTextBgColor,
                    cursorColor = Color.Black,
                    focusedIndicatorColor = GreenColor,
                    unfocusedIndicatorColor = EditTextBgColor,
                    placeholderColor = Color(0xFFB2B2B2)
                ),
                maxLines = 1,

                )

            Row(
                modifier = Modifier
                    .padding(start = 24.dp, end = 24.dp)
                    .fillMaxWidth()
                    .height(32.dp),
                verticalAlignment = Alignment.Bottom
            ) {
                Text(
                    color = Color.Gray,
                    fontFamily = FontFamily(Font(R.font.nata_semibold)),
                    fontSize = 16.sp,
                    text = "Tug'ilgan sana:"
                )
                Spacer(modifier = Modifier.width(16.dp))
                Column(
                    modifier = Modifier
                        .width(IntrinsicSize.Min)


                ) {
                    Text(
                        color = Color.Black,
                        fontFamily = FontFamily(Font(R.font.nata_semibold)),
                        fontSize = 16.sp,
                        text = "8",
                        modifier=  Modifier

                            .padding(start = 4.dp, end = 4.dp)
                    )
                    Box(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(2.dp)
                            .background(Color.Gray)
                    )
                }



                Spacer(modifier = Modifier.width(16.dp))

                Column(
                    modifier = Modifier
                        .width(IntrinsicSize.Min),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        color = Color.Black,
                        fontFamily = FontFamily(Font(R.font.nata_bold)),
                        fontSize = 16.sp,
                        text = "avgust",
                        modifier = Modifier
                            .padding(start = 8.dp, end = 8.dp)
                    )
                    Box(
                        modifier = Modifier
                            .background(Color.Gray)
                            .fillMaxWidth()
                            .height(2.dp)
                    )
                }
                Spacer(modifier = Modifier.width(16.dp))
                Column(
                    modifier = Modifier
                        .width(IntrinsicSize.Min),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        color = Color.Black,
                        fontFamily = FontFamily(Font(R.font.nata_bold)),
                        fontSize = 16.sp,
                        text = "2005",
                        modifier = Modifier
                            .padding(horizontal = 8.dp)
                    )
                    Box(
                        modifier = Modifier
                            .background(Color.Gray)
                            .fillMaxWidth()
                            .height(2.dp)
                    )
                }
            }
            Spacer(modifier = Modifier.weight(1f))
            Button(

                modifier = Modifier
                    .padding(start = 24.dp, end = 24.dp)
                    .height(48.dp)
                    .fillMaxWidth(),
                shape = RoundedCornerShape(12.dp),
                colors = ButtonDefaults.buttonColors(GreenColor),
                onClick = { /*TODO*/ })
            {
                Text(
                    fontSize = 16.sp,
                    text = "Bo'ldi")

            }
            Spacer(modifier = Modifier.height(36.dp))


        }

    }
}


@Composable
@Preview
fun ProfilePreview() {
    ProfileContent(ProfileContract.UIState(null)){

    }
}
