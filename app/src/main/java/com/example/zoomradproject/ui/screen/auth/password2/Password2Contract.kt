package com.example.zoomradproject.ui.screen.auth.password2

import org.orbitmvi.orbit.ContainerHost

interface Password2Contract {


    interface PasswordViewModel : ContainerHost<UIState, SideEffect> {
        fun onEventDispatcher(intent: Intent)
    }

    sealed interface UIState {
        data object Screen : UIState
    }

    sealed interface SideEffect {

    }

    sealed interface Intent {
        data object OpenMainScreen : Intent
    }
}