package com.example.zoomradproject.ui.screen.auth.verify

import org.orbitmvi.orbit.ContainerHost

interface VerifyContract {


    interface VerifyModel : ContainerHost<UIState, SideEffect> {
        fun onEventDispatcher(intent: Intent)

    }

    data class UIState (
        val phone:String =""
        )

    sealed interface SideEffect {
        data class ToastSideEffect(val msg: String) : SideEffect

    }

    sealed interface Intent {
        data class ButtonClickSend(val code :String) : Intent
        data object First:Intent
    }
}