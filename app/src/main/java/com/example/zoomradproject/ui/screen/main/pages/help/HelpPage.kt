package com.example.zoomradproject.ui.screen.main.pages.help

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.startActivity
import cafe.adriel.voyager.hilt.getViewModel
import cafe.adriel.voyager.navigator.tab.Tab
import cafe.adriel.voyager.navigator.tab.TabOptions
import com.example.zoomradproject.R
import com.example.zoomradproject.ui.screen.main.pages.home.CustomLogOutDialog
import com.example.zoomradproject.ui.screen.main.pages.more.MoreContract
import com.example.zoomradproject.ui.theme.MainBgColor
import com.example.zoomradproject.utils.HelpItem
import com.example.zoomradproject.utils.MyDrawer
import kotlinx.coroutines.launch
import org.orbitmvi.orbit.compose.collectAsState
import org.orbitmvi.orbit.compose.collectSideEffect


object HelpPage : Tab {
    private fun readResolve(): Any = HelpPage
    override val options: TabOptions
        @Composable
        get() {
            val title = "Help"
            val icon = painterResource(id = R.drawable.ic_talk)

            return remember {
                TabOptions(
                    index = 3u,
                    title = title,
                    icon = icon
                )
            }
        }

    @Composable
    override fun Content() {
        val model: HelpContract.HelpViewModel =
            getViewModel<HelpViewModel>()
        val uiState = model.collectAsState().value
        var boolean by remember {
            mutableStateOf(false)
        }
        if(boolean) {
            CustomLogOutDialog(
                onConfirm = {
                    model.onEventDispatcher(HelpContract.Intent.DialogButtonYes)
                    boolean=false
                }) {
                boolean = false
            }
        }
        model.collectSideEffect {
            when(it){
                HelpContract.SideEffect.OpenDialog->{
                    boolean = true
                }
            }

        }
        HelpContent(uiState, model::onEventDispatcher)
    }
}


@Composable
fun HelpContent(
    uiState: HelpContract.UIState,
    onEventDispatcher: (HelpContract.Intent) -> Unit
) {
    var selectedItemIndex by rememberSaveable {
        mutableIntStateOf(0)
    }
    val context = LocalContext.current

    val drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)
    val scope = rememberCoroutineScope()
    ModalNavigationDrawer(
        drawerState = drawerState, drawerContent = {
            MyDrawer { index ->
                selectedItemIndex = index

                if(index==7){
                    onEventDispatcher(HelpContract.Intent.OpenDialog)
                }
                else {
                    onEventDispatcher.invoke(HelpContract.Intent.OpenDrawer(index))
                }

            }
        }, gesturesEnabled = true
    ) {

        Scaffold(
            modifier = Modifier
                .fillMaxSize(),
            contentColor = MainBgColor
        ) { padding ->
            Column(
                modifier = Modifier
                    .fillMaxSize()
            ) {
                Row(
                    modifier = Modifier
                        .padding(start = 16.dp, end = 16.dp, top = 24.dp),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Image(
                        modifier = Modifier
                            .clickable {
                                scope.launch {
                                    drawerState.apply {
                                        if (isClosed) open() else close()
                                    }
                                }
                            }
                            .height(24.dp)
                            .width(24.dp)
                            .padding(3.dp),
                        painter = painterResource(id = R.drawable.ic_menu),
                        contentDescription = "",
                    )
                    Spacer(modifier = Modifier.weight(1f))
                    Text(
                        text = stringResource(R.string.help_text),
                        fontSize = 16.sp,
                        color = Color(0xFFB7B7B7),
                        fontFamily = FontFamily(Font(R.font.nata_bold)),
                        fontWeight = FontWeight(300)
                    )
                    Spacer(modifier = Modifier.weight(1f))


                    Spacer(
                        modifier = Modifier
                            .width(24.dp)
                    )
                }
                Row(
                    modifier = Modifier
                        .padding(start = 24.dp, end = 24.dp, top = 24.dp)
                        .fillMaxWidth()
                ) {
                    HelpItem(R.string.call_bank, R.drawable.ic_call_green, 16) {
                        try {
                            val callIntent: Intent = Intent(Intent.ACTION_CALL)
                            callIntent.setData(Uri.parse("tel:" + "+998930720405"))
                            ContextCompat.startActivity(context, callIntent, null)
                        } catch (activityException: ActivityNotFoundException) {
                            Log.e("Calling a Phone Number", "Call failed", activityException)
                        }
                    }
                    HelpItem(R.string.help_text, R.drawable.ic_target, 14) {
                        onEventDispatcher.invoke(HelpContract.Intent.ClickMap)
                    }

                }
                Spacer(modifier = Modifier.height(48.dp))

            }

        }
    }
}


@Composable
@Preview
fun HelpPagePreview() {
    HelpContent(uiState = HelpContract.UIState.Screen,
        {})
}