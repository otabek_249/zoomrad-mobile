package com.example.zoomradproject.ui.screen.map.branch

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.hilt.getViewModel
import cafe.adriel.voyager.navigator.bottomSheet.LocalBottomSheetNavigator
import com.example.zoomradproject.R
import com.example.zoomradproject.data.model.MarkerData
import com.example.zoomradproject.ui.dialog.MapDialog
import com.example.zoomradproject.ui.theme.DefaultButtonColor
import com.example.zoomradproject.ui.theme.EditTextBgColor
import com.example.zoomradproject.ui.theme.GreenColor
import com.example.zoomradproject.ui.theme.ItemBgColor
import com.example.zoomradproject.ui.theme.MainBgColor
import com.example.zoomradproject.utils.ItemBranchCategory
import com.example.zoomradproject.utils.ItemBranchInfo
import com.example.zoomradproject.utils.MapComponent
import com.example.zoomradproject.utils.myLog
import org.orbitmvi.orbit.compose.collectAsState
import org.orbitmvi.orbit.compose.collectSideEffect
import java.util.Locale


class BranchScreen : Screen {
    @Composable
    override fun Content() {
        val model :BranchContract.BranchViewModel  = getViewModel<BranchViewModel>()
        val uiState = model.collectAsState().value
        val bottomSheetNavigator = LocalBottomSheetNavigator.current
        val context = LocalContext.current
        val side = model.collectSideEffect {sideEffect->
            when(sideEffect) {
              is  BranchContract.SideEffect.BottomSheetDialog -> {

                  bottomSheetNavigator.show(MapDialog(sideEffect.markerData){
                      val uri: String = String.format(Locale.ENGLISH, "geo:%f,%f", sideEffect.markerData.lat, sideEffect.markerData.lng)
                      val intent: Intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                      context.startActivity(intent)
                  })

                }
            }
        }

        BranchContent(uiState = uiState, onEventDispatcher = model ::onEventDispatcher)
    }
}




@SuppressLint(
    "UnusedMaterialScaffoldPaddingParameter",
    "MutableCollectionMutableState",
    "UnrememberedMutableState"
)
@Composable
fun BranchContent(uiState: BranchContract.UIState,onEventDispatcher: (BranchContract.Intent) -> Unit = {}) {
    var  index by remember {
        mutableIntStateOf(0)
    }

    val branchList = arrayListOf<MarkerData>(
        MarkerData(name = "Amliyot boshqarmasi", location = "Toshkent shahri, Mirobod tumani,Amir Temur shoh ko'chasi" ,lat= 41.31136895954227, lng =69.27787744866806),
        MarkerData(lat =41.29565600570065, lng=69.28219468905212, location = "Toshkent shahri, Shayxontohur tumani,Navoiy  ko'chasi ,27-uy", name = "Mirobod filiali"),
        MarkerData(lat=41.32348757453901, lng =69.2404902841117, location = "Shayxontohur tumani,A.Navoiy  ko'chasi ,40-uy", name = "Shayxontoxur"),
        MarkerData(lat=41.32348757453901, lng =69.2404902841117, location = "Toshkent shari,Mirzo Ulug'bek tumani,Muminov  ko'chasi ", name = "IT-Park"),
        MarkerData(lat=37.23784519551908, lng=67.29301889891771, location = "Surxondaryo viloyati,Termiz shahri,Alisher Navoiy ko'chasi ", name = "Surdandaryo filiali"),
        MarkerData(lat=38.83849247499948, lng =65.80218783945786, location = "Qashqadaryo viloyati,Qarshi shahri,Mustaqillik shohko'chasi ", name = "Qashqadaryo filiali"),
    )



    val listCategory =
        listOf("Filiallar", "K.H.K Markazlari", "Bankomatlar", "Ayriboshlash Shahobchalari")

    var boolean by remember { mutableStateOf(false) }
    val zoom by remember { mutableFloatStateOf(15f) }
    val context = LocalContext.current

    var data by remember {
        mutableStateOf(uiState.map[0])
    }

    Scaffold(
        modifier = Modifier.fillMaxSize(),
        backgroundColor = MainBgColor
    ) {
        Column(
            modifier = Modifier.fillMaxSize()
        ) {
            Row(
                modifier = Modifier
                    .padding(start = 16.dp, end = 16.dp, top = 24.dp),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    modifier = Modifier
                        .clickable {
                            onEventDispatcher.invoke(BranchContract.Intent.BackClick)
                        }
                        .height(24.dp)
                        .width(24.dp)
                        .padding(3.dp),
                    painter = painterResource(id = R.drawable.ic_back_green),
                    contentDescription = ""
                )
                Spacer(modifier = Modifier.weight(1f))
                Text(
                    text = stringResource(R.string.brach_text),
                    fontSize = 16.sp,
                    color = Color(0xFFB7B7B7),
                    fontFamily = FontFamily(Font(R.font.nata_bold)),
                    fontWeight = FontWeight.Normal
                )
                Spacer(modifier = Modifier.weight(1f))

                Image(
                    modifier = Modifier
                        .clickable {
                            boolean = !boolean
                        }
                        .padding(bottom = 4.dp)
                        .height(24.dp)
                        .width(24.dp),
                    painter = painterResource(id = if (boolean) R.drawable.ic_menu else R.drawable.ic_map),
                    contentDescription = ""
                )
            }
            Box(
                modifier = Modifier
                    .padding(top = 16.dp, bottom = 4.dp, start = 24.dp, end = 24.dp)
                    .fillMaxWidth()
                    .height(40.dp)
                    .background(color = EditTextBgColor, shape = RoundedCornerShape(12.dp)),
                contentAlignment = Alignment.Center
            ) {
                Text(
                    modifier = Modifier.padding(start = 16.dp),
                    color = Color(0xFF1A1A1A),
                    textAlign = TextAlign.Center,
                    text = "Barchasi",
                    fontSize = 14.sp,
                    fontFamily = FontFamily(Font(R.font.nata_bold))
                )
            }
            LazyRow(
                modifier = Modifier,
                contentPadding = PaddingValues(start = 18.dp)
            ) {
                items(listCategory.size) { i ->

                    if(i==index){
                        val bgColor =  GreenColor
                        val textColor =  Color.White
                        ItemBranchCategory(
                            textColor = textColor,
                            background = bgColor,
                            text = listCategory[i]
                        ) {
                            index = i
                            data =uiState.map[index]
                        }

                    }
                    else{
                        val bgColor =  ItemBgColor
                        val textColor =  Color.Black
                        ItemBranchCategory(
                            textColor = textColor,
                            background = bgColor,
                            text = listCategory[i]
                        ) {
                            index = i
                            data =uiState.map[index]
                        }
                    }


                }
            }
            if (!boolean) {

                LazyColumn(
                    modifier = Modifier,
                    contentPadding = PaddingValues(start = 8.dp)
                ) {

                    for (i in 0 until (data?.size ?: 0)) {
                        item {
                            data?.get(i)?.let { item->
                                ItemBranchInfo(
                                    location = item.location,
                                    timeWork = item.time,
                                    name = item.name
                                ){
                                    boolean = !boolean
                                    onEventDispatcher.invoke(BranchContract.Intent.ClickMarker(item))
                                }
                            }
                        }
                    }
                }
            } else {
                MapComponent(
                        context = context,
                        modifier = Modifier
                            .padding(top = 8.dp)
                            .fillMaxWidth()
                            .weight(1f), zoom = zoom, ls = branchList
                    ) {
                    onEventDispatcher.invoke(BranchContract.Intent.ClickMarker(it))
                    }
            }
        }
    }
}

@Preview
@Composable
fun BranchPreview() {
    BranchContent(BranchContract.UIState(emptyMap()))
}