package com.example.zoomradproject.ui.screen.monitoring

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.zoomradproject.data.model.Transaction
import com.example.zoomradproject.repository.main.AppRepository
import com.example.zoomradproject.utils.myLog
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.syntax.simple.reduce
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject

@HiltViewModel
class MonitoringViewModelImpl @Inject constructor(
    private val repo: AppRepository,
    private val paging: TestPaginationSource
) : ViewModel(), MonitoringContract.MonitoringViewModel {


    private val _transaction: MutableStateFlow<PagingData<Transaction>> =
        MutableStateFlow(PagingData.empty())
    var transaction = _transaction.asStateFlow()
        private set

    override val container = container<MonitoringContract.UIState, MonitoringContract.SideEffect>(MonitoringContract.UIState())

    init {
        getMonitoring()
        getHistory()
    }

    override fun onEventDispatcher(intent: MonitoringContract.Intent) = intent {
        when (intent) {
            MonitoringContract.Intent.Back -> {
                // Handle back navigation
            }
        }
    }



    private fun getMonitoring() {
        repo.getHistory()
            .onEach {
                it.onSuccess { data ->
                    "${data.child.size} getMonitoring viewModel".myLog()
                    intent { reduce { MonitoringContract.UIState(data) } }
                }.onFailure {
                    // Handle failure if needed
                }
            }.launchIn(viewModelScope)
    }

    private fun getHistory() {
        viewModelScope.launch {
            Pager(
                config = PagingConfig(
                    pageSize = 10,
                    enablePlaceholders = true
                )
            ) {
                paging
            }.flow.cachedIn(viewModelScope).collect {
                _transaction.value = it
            }
        }
    }
}
