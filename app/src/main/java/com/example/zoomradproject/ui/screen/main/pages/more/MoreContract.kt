package com.example.zoomradproject.ui.screen.main.pages.more

import com.example.zoomradproject.ui.screen.main.pages.home.HomeContract
import org.orbitmvi.orbit.ContainerHost

interface MoreContract {


    interface MoreViewModel : ContainerHost<UIState,SideEffect> {
        fun onEventDispatcher(intent:Intent)

    }

    data class UIState(
        val string: String? = null
    )

    sealed interface  SideEffect{
        data object  OpenDialog: SideEffect
    }

    sealed interface Intent{
        data class OpenDrawer(val index :Int) :Intent
        data object DialogButtonYes: Intent
        data object OpenDialog: Intent
    }

}