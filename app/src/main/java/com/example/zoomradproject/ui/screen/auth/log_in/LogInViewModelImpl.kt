package com.example.zoomradproject.ui.screen.auth.log_in

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.zoomradproject.data.pref.MyPref
import com.example.zoomradproject.data.remote.request.LogInRequest
import com.example.zoomradproject.repository.auth.AuthRepository
import com.example.zoomradproject.utils.myLog
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.syntax.simple.postSideEffect
import org.orbitmvi.orbit.syntax.simple.reduce
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject


@HiltViewModel
class  LogInViewModelImpl @Inject constructor(
    private val authRepository: AuthRepository,
    private val navigation: LogInDirection
) : LogInContract.LogInViewModel, ViewModel() {
    private val listener : ((String) -> Unit?)? = null
    private val myPref = MyPref.getInstance()

    override val container = container<LogInContract.UIState, LogInContract.SideEffect>(
        LogInContract.UIState("")
    )


    override fun onEventDispatcher(intent: LogInContract.Intent) = intent {
        when (intent) {
            LogInContract.Intent.ClickTextOpenSignUpScreen -> {
                navigation.openSignUpScreen()

            }

            is LogInContract.Intent.ClickButton -> {
                logIn(intent.logInRequest)
            }
            is LogInContract.Intent.PhoneNumber->{
                myPref.setPhone(intent.phone)
            }
        }


    }

   private fun logIn(logIn:LogInRequest){
        authRepository.logIn(logIn)
            .onEach {
                it.onSuccess {
                    navigation.openVerifyScreen()
                }
                it.onFailure {it->

                    if(it.message?.length==0)return@onFailure
                    intent {
                        reduce { LogInContract.UIState(state = true) }
                        postSideEffect(LogInContract.SideEffect.TextToast(it.message.toString())) }
                }
            }
            .launchIn(viewModelScope)
    }
}