package com.example.zoomradproject.ui.screen.auth.sign_up

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.zoomradproject.data.pref.MyPref
import com.example.zoomradproject.data.remote.request.SignUpData
import com.example.zoomradproject.repository.auth.impl.AuthRepositoryImpl
import com.example.zoomradproject.utils.myLog
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.syntax.simple.postSideEffect
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject



@HiltViewModel
class SignUpViewModel @Inject constructor(
    private val navigator: SignUpDirection,
    private val repositoryImpl: AuthRepositoryImpl
) : ViewModel(), SignUpContract.SignUpModel {


    override val container = container<SignUpContract.UIState, SignUpContract.SideEffect>(
        SignUpContract.UIState.Display("")
    )
    private val myPref = MyPref.getInstance()


    override fun onEventDispatcher(intent: SignUpContract.Intent) = intent {
        when (intent) {


            is SignUpContract.Intent.ClickButtonSave -> {
                singUp(intent.signUpData)
            }

            SignUpContract.Intent.ClickTextOpenLogIn -> {
                navigator.openLogInScreen()
            }
            is SignUpContract.Intent.PhoneNumber->{
                myPref.setPhone(intent.phone)
            }
        }
    }

    private  fun singUp(signUpData: SignUpData) {
        repositoryImpl.signUp(signUpData)
            .onEach {
                it.onSuccess {
                    navigator.openVerifyScreen()
                }
                it.onFailure {
                    intent { postSideEffect(SignUpContract.SideEffect.ShowToast(it.message.toString())) }
                }
            }
            .launchIn(viewModelScope)

    }
}