package com.example.zoomradproject.ui.screen.cardsettings

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.zoomradproject.data.remote.request.UpDateCard
import com.example.zoomradproject.repository.main.AppRepository
import com.example.zoomradproject.ui.screen.cards.CardsScreen
import com.example.zoomradproject.utils.navigation.AppNavigator
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.orbitmvi.orbit.Container
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject


interface CardSettingsDirection{
    suspend fun back()
}

class CardSettingsDirectionImpl @Inject  constructor(
    val navigator: AppNavigator
)  :CardSettingsDirection{
    override suspend fun back() {
        navigator.navigateTo(CardsScreen(0))
    }


}