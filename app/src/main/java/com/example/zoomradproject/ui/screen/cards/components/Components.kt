package com.example.zoomradproject.ui.screen.cards.components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import cafe.adriel.voyager.core.screen.Screen
import com.example.zoomradproject.R
import com.example.zoomradproject.data.remote.response.CardsResponse
import com.example.zoomradproject.ui.theme.MainBgColor



class CardBottomSheet(var cardsResponse: CardsResponse,var click:(Int)->Unit):Screen{
    @Composable
    override fun Content() {
        CardsBottomSheetContent(click =click)
    }

}

@Composable
fun CardsBottomSheetContent(click: (Int) -> Unit) {
    Column(
        modifier = Modifier
            .background(MainBgColor)
            .fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        Spacer(modifier = Modifier.height(24.dp))
        Text(
            modifier = Modifier
                .clickable {
                           click.invoke(1)
                },
            text = stringResource(R.string.monitoring),
            fontSize = 18.sp,
            fontFamily = FontFamily(Font(R.font.nata_semibold)),
            color = Color.Black
        )
        Spacer(modifier = Modifier.height(16.dp))
        Box(
            modifier = Modifier
                .padding(horizontal = 16.dp)
                .fillMaxWidth()
                .height(2.dp)
                .background(Color.Gray),
        )
        Spacer(modifier = Modifier.height(16.dp))
        Text(
            modifier = Modifier
                .clickable {
                           click.invoke(2)
                },
            text = stringResource(R.string.card_setting_text),
            fontSize = 18.sp,
            fontFamily = FontFamily(Font(R.font.nata_semibold)),
            color = Color.Black
        )
        Spacer(modifier = Modifier.height(16.dp))
        Box(
            modifier = Modifier
                .padding(horizontal = 16.dp)
                .fillMaxWidth()
                .height(2.dp)
                .background(Color.Gray),
        )

        Spacer(modifier = Modifier.height(16.dp))
        Text(
            modifier = Modifier
                .clickable {
                           click.invoke(3)
                },
            text = stringResource(R.string.rekvizitlar),
            fontSize = 18.sp,
            fontFamily = FontFamily(Font(R.font.nata_semibold)),
            color = Color.Black
        )
        Spacer(modifier = Modifier.height(16.dp))
        Box(
            modifier = Modifier
                .padding(horizontal = 16.dp)
                .fillMaxWidth()
                .height(2.dp)
                .background(Color.Gray),
        )
        Spacer(modifier = Modifier.height(16.dp))
        Text(
            modifier = Modifier
                .clickable {
                           click.invoke(4)
                },
            text = stringResource(R.string.main_card_text),
            fontSize = 18.sp,
            fontFamily = FontFamily(Font(R.font.nata_semibold)),
            color = Color.Black
        )
        Spacer(modifier = Modifier.height(16.dp))
        Box(
            modifier = Modifier
                .padding(horizontal = 16.dp)
                .fillMaxWidth()
                .height(2.dp)
                .background(Color.Gray),
        )
        Spacer(modifier = Modifier.height(16.dp))
        Text(
            modifier = Modifier
                .clickable {
                           click.invoke(5)
                },
            text = stringResource(R.string.delete),
            fontSize = 18.sp,
            fontFamily = FontFamily(Font(R.font.nata_semibold)),
            color = Color.Black
        )
        Spacer(modifier = Modifier.height(24.dp))


    }
}
