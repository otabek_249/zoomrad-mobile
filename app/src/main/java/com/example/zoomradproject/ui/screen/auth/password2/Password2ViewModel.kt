package com.example.zoomradproject.ui.screen.auth.password2

import androidx.lifecycle.ViewModel
import com.example.zoomradproject.ui.screen.auth.password.PasswordDirection
import dagger.hilt.android.lifecycle.HiltViewModel
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject


@HiltViewModel
class Password2ViewModel @Inject constructor(
    private val navigation: PasswordDirection
) : Password2Contract.PasswordViewModel, ViewModel() {


    override val container = container<Password2Contract.UIState, Password2Contract.SideEffect>(
        Password2Contract.UIState.Screen
    )

    override fun onEventDispatcher(intent: Password2Contract.Intent) = intent {
        when (intent) {
            Password2Contract.Intent.OpenMainScreen -> {
                navigation.openMainScreen()
            }

        }
    }
}