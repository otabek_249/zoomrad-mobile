package com.example.zoomradproject.ui.screen.splash

import com.example.zoomradproject.ui.screen.auth.log_in.LogInScreen
import com.example.zoomradproject.ui.screen.auth.password.PasswordScreen
import com.example.zoomradproject.ui.screen.auth.password2.Password2Screen
import com.example.zoomradproject.ui.screen.language.LanguageScreen
import com.example.zoomradproject.utils.navigation.AppNavigator
import javax.inject.Inject

interface SplashDirection {
    suspend fun logInScreen()
    suspend fun passwordScreen()

}

class SplashDirectionImpl @Inject constructor(
    private var appNavigator: AppNavigator
) : SplashDirection {
    override suspend fun logInScreen() {
        appNavigator.replace(LanguageScreen())
    }

    override suspend fun passwordScreen() {
        appNavigator.replace(Password2Screen())
    }


}