package com.example.zoomradproject.ui.screen.monitoring

import com.example.zoomradproject.utils.navigation.AppNavigator
import javax.inject.Inject


interface MonitoringDirection{
    suspend fun back()
}
class MonitoringDirectionImpl @Inject constructor(
    private val navigator: AppNavigator
):MonitoringDirection{
    override suspend fun back() {
        navigator.back()
    }
}