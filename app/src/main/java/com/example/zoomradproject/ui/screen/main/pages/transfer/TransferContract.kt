package com.example.zoomradproject.ui.screen.main.pages.transfer

import com.example.zoomradproject.data.remote.request.TransferDataRequest
import com.example.zoomradproject.data.remote.response.CardsResponse
import com.example.zoomradproject.ui.screen.main.pages.payment.PaymentContract
import org.orbitmvi.orbit.ContainerHost

interface TransferContract {

    interface TransferViewModel:ContainerHost<UIState,SideEffect>{
        fun onEventDispatcher(intent:Intent)
    }

    data class UIState(
        val nameCardOwner:String="",
        val list:List<CardsResponse> = emptyList(),
        val index :Int =0
    )
    sealed interface SideEffect{
        data object  OpenDialog: SideEffect
    }

    sealed interface Intent{
        data class TransferCard(val data:TransferDataRequest):Intent
        data class GetOwnerByPan(val cardName:String) :Intent
        data class OpenDrawer(val index :Int): Intent
        data object Selected:Intent
        data object DialogButtonYes:Intent
        data object OpenDialog: Intent
    }
}