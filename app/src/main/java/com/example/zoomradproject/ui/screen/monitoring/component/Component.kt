package com.example.zoomradproject.ui.screen.monitoring.component

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.zoomradproject.R
import com.example.zoomradproject.data.model.Transaction
import com.example.zoomradproject.ui.theme.EditTextBgColor
import com.example.zoomradproject.ui.theme.GreenColor
import com.example.zoomradproject.ui.theme.ZoomradProjectTheme


@Composable
fun IncomeSmallItem() {
    Box(
        modifier = Modifier
            .height(56.dp)
            .width(56.dp)
            .background(Color.White, shape = RoundedCornerShape(10.dp))
    ) {
        Image(

            modifier = Modifier
                .rotate(180f)
                .padding(12.dp)
                .fillMaxSize(),
            painter = painterResource(id = R.drawable.arrow),
            contentDescription = "",
            colorFilter = ColorFilter.tint(Color.Gray)
        )

    }
}

@Composable
fun OutcomeSmallItem() {
    Box(
        modifier = Modifier
            .height(56.dp)
            .width(56.dp)
            .background(Color.White, shape = RoundedCornerShape(10.dp))
    ) {
        Image(

            modifier = Modifier
                .padding(12.dp)
                .fillMaxSize(),
            painter = painterResource(id = R.drawable.arrow),
            contentDescription = "",
            colorFilter = ColorFilter.tint(Color.Gray)
        )

    }
}


@Composable
fun ItemMonitoring( transaction: Transaction) {
    Column(
        modifier = Modifier
            .padding(16.dp)
            .fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,

    ) {
        Row {
            if(transaction.type=="income"){
                IncomeSmallItem()
            }
            else{
                OutcomeSmallItem()
            }

            Column(
                modifier = Modifier
                    .padding(start = 8.dp)
                    .weight(3f),
            ) {
                Text(
                    modifier = Modifier,
                    fontSize = 13.sp,
                    text = transaction.type,
                    color = Color.Black)
                Text(
                    modifier = Modifier,
                    fontSize = 16.sp,
                    text = transaction.to,
                    color = Color.Black)
                Text(
                    modifier = Modifier,
                    fontSize = 13.sp,
                    text = transaction.time.toString(),
                    color = Color.Black)

            }
            Column (
                modifier = Modifier,
                horizontalAlignment = Alignment.End


            ){
                if(transaction.type=="income"){
                    Text(
                        fontSize = 18.sp,
                        color = GreenColor,
                        fontFamily = FontFamily(Font(R.font.nata_bold)),
                        text = "+"+transaction.amount.toString())
                }
                else{
                    Text(
                        fontSize = 18.sp,
                        color = Color.Red,
                        fontFamily = FontFamily(Font(R.font.nata_bold)),
                        text = "-"+transaction.amount.toString())

                }


                Text(
                    modifier = Modifier,
                    fontSize = 14.sp,
                    text = transaction.from,
                    color = Color.Black)

            }
        }
        Box(
            modifier = Modifier
                .height(2.dp)
                .background(Color.Gray)
        )

    }
}

@Preview
@Composable
fun SmallItemPreview() {
    ZoomradProjectTheme {
        Column(
            modifier = Modifier
                .background(Color.Transparent)
                .fillMaxSize()
        ) {

        }
       ItemMonitoring(Transaction(
           type = "income",
           from = "Otabek",
           to = "Ogabek",
           amount = 10000.0,
           time = 1000L
       ))
    }
}

fun millisToHours(millis: Long): Double {
    val hours = millis.toDouble() / (1000 * 60 * 60)
    return hours
}
