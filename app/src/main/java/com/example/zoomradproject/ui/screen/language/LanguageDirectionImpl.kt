package com.example.zoomradproject.ui.screen.language

import com.example.zoomradproject.ui.screen.auth.log_in.LogInScreen
import com.example.zoomradproject.utils.navigation.AppNavigator
import javax.inject.Inject


interface LanguageDirection {
    suspend fun openSignUpScreen()
}

class LanguageDirectionImpl @Inject constructor(
    private val navigation: AppNavigator
) : LanguageDirection {
    override suspend fun openSignUpScreen() {
        navigation.navigateTo(LogInScreen())
    }
}