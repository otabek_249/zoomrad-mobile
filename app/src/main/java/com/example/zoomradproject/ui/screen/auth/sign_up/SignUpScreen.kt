package com.example.zoomradproject.ui.screen.auth.sign_up

import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.hilt.getViewModel
import com.example.zoomradproject.R
import com.example.zoomradproject.data.remote.request.SignUpData
import com.example.zoomradproject.ui.screen.auth.log_in.LogInContract
import com.example.zoomradproject.ui.screen.auth.log_in.formatPhoneNumber
import com.example.zoomradproject.ui.screen.auth.log_in.formatPhoneNumber2
import com.example.zoomradproject.ui.theme.DefaultButtonColor
import com.example.zoomradproject.ui.theme.EditTextBgColor
import com.example.zoomradproject.ui.theme.GreenColor
import com.example.zoomradproject.ui.theme.MainBgColor
import org.orbitmvi.orbit.compose.collectAsState
import org.orbitmvi.orbit.compose.collectSideEffect

class SignUpScreen : Screen {


    @Composable
    override fun Content() {
        val model: SignUpContract.SignUpModel = getViewModel<SignUpViewModel>()
        val uiState = model.collectAsState().value
        val context = LocalContext.current
        model.collectSideEffect {
            when (it) {
                is SignUpContract.SideEffect.ShowToast -> {
                    Toast.makeText(context,it.msg,Toast.LENGTH_SHORT).show()
                }
            }
        }

        SignUpContent(uiState, model::onEventDispatcher)
    }
}


@Composable
fun SignUpContent(
    uiState: SignUpContract.UIState, onEventDispatcher: (SignUpContract.Intent) -> Unit = {}
) {
    val phoneNumber = remember { mutableStateOf("") }
    val password = remember { mutableStateOf("") }
    val data = remember { mutableStateOf("1234567") }
    val gender = remember { mutableStateOf("1") }
    val firstName = remember { mutableStateOf("") }
    val lastName = remember { mutableStateOf("") }
    var boolean by remember {
        mutableStateOf(false)
    }

    boolean=
        (password.value.length > 5 && phoneNumber.value.startsWith("+998") && phoneNumber.value.length > 12 && firstName.value.length >= 3 && lastName.value.length > 3)


    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MainBgColor),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        Box(
            modifier = Modifier
                .weight(0.3f)
                .fillMaxWidth(),
            contentAlignment = Alignment.BottomCenter
        ) {
            Image(
                painter = painterResource(id = R.drawable.ic_zoomrad),
                contentDescription = "",
                modifier = Modifier
                    .height(100.dp)
                    .width(100.dp)
            )

        }
        Column(
            modifier = Modifier
                .weight(1f)
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Spacer(modifier = Modifier.height(32.dp))
            OutlinedTextField(
                modifier = Modifier
                    .padding(start = 24.dp, end = 24.dp)

                    .height(52.dp)
                    .fillMaxWidth(),
                shape = RoundedCornerShape(12.dp),
                singleLine = true,


                value = firstName.value,
                onValueChange = {
                    if(it.length<=16){
                        firstName.value = it
                    }


                },
                textStyle = TextStyle(
                    fontFamily = FontFamily(Font(R.font.nata_regular)),
                    fontSize = 18.sp,
                    textAlign = TextAlign.Start,
                ),
                placeholder = {
                    Text(
                        text = stringResource(R.string.enter_firstname)
                    )
                },
                leadingIcon = {
                              Image(
                                  modifier = Modifier
                                      .height(24.dp)
                                      .width(24.dp),
                                  painter = painterResource(id = R.drawable.ic_user),
                                  contentDescription ="" )
                },


                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = EditTextBgColor,
                    focusedIndicatorColor = GreenColor,
                    unfocusedIndicatorColor = EditTextBgColor,
                    placeholderColor = Color(0xFFB2B2B2)
                ),
                maxLines = 1,

                )
            Spacer(modifier = Modifier.height(12.dp))
            OutlinedTextField(
                modifier = Modifier
                    .padding(start = 24.dp, end = 24.dp)

                    .height(52.dp)
                    .fillMaxWidth(),
                shape = RoundedCornerShape(12.dp),
                textStyle = TextStyle(
                    fontFamily = FontFamily(Font(R.font.nata_regular)),
                    fontSize = 18.sp,
                    textAlign = TextAlign.Start,
                ),
                singleLine = true,


                value = lastName.value,

                onValueChange = {
                    if(it.length<=20){
                        lastName.value = it
                    }

                },
                placeholder = {
                    Text(
                        text = stringResource(R.string.enter_lastname)
                    )
                },
                leadingIcon = {
                    Image(
                        modifier = Modifier
                            .height(24.dp)
                            .width(24.dp),
                        painter = painterResource(id = R.drawable.ic_user),
                        contentDescription ="" )
                },


                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = EditTextBgColor,
                    focusedIndicatorColor = GreenColor,
                    unfocusedIndicatorColor = EditTextBgColor,
                    placeholderColor = Color(0xFFB2B2B2)
                ),
                maxLines = 1,

                )


            Spacer(modifier = Modifier.height(12.dp))

            OutlinedTextField(
                modifier = Modifier
                    .padding(start = 24.dp, end = 24.dp)

                    .height(52.dp)
                    .fillMaxWidth(),
                shape = RoundedCornerShape(12.dp),
                textStyle = TextStyle(
                    fontFamily = FontFamily(Font(R.font.nata_regular)),
                    fontSize = 18.sp,
                    textAlign = TextAlign.Start,
                ),
                singleLine = true,

                value = phoneNumber.value,
                onValueChange = {
                    if(it.length<=13){
                        phoneNumber.value = it
                    }


                },
                placeholder = {
                    Text(
                        text = stringResource(id = R.string.enter_phonenumber)
                    )
                },
                leadingIcon = {
                    Image(
                        modifier = Modifier
                            .height(24.dp)
                            .width(24.dp)
                            .padding(1.dp),
                        painter = painterResource(id = R.drawable.ic_telephone),
                        contentDescription ="" )
                },


                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = EditTextBgColor,
                    focusedIndicatorColor = GreenColor,
                    unfocusedIndicatorColor = EditTextBgColor,
                    placeholderColor = Color(0xFFB2B2B2)
                ),
                maxLines = 1,

                )
            Spacer(modifier = Modifier.height(12.dp))
            OutlinedTextField(
                modifier = Modifier
                    .padding(start = 24.dp, end = 24.dp)

                    .height(52.dp)
                    .fillMaxWidth(),
                shape = RoundedCornerShape(12.dp),
                textStyle = TextStyle(
                    fontFamily = FontFamily(Font(R.font.nata_regular)),
                    fontSize = 18.sp,
                    textAlign = TextAlign.Start,
                ),
                singleLine = true,


                value = password.value,
                onValueChange = {
                    if(it.length<=14){
                        password.value = it
                    }


                },
                placeholder = {
                    Text(
                        modifier = Modifier
                            .align(Alignment.CenterHorizontally),
                        text = stringResource(id = R.string.enter_password)
                    )
                },
                leadingIcon = {
                    Image(
                        modifier = Modifier
                            .height(24.dp)
                            .width(24.dp),
                        painter = painterResource(id = R.drawable.ic_padlock),
                        contentDescription ="" ,
                        colorFilter = ColorFilter.tint(Color.Black))
                },


                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = EditTextBgColor,
                    focusedIndicatorColor = GreenColor,
                    unfocusedIndicatorColor = EditTextBgColor,
                    placeholderColor = Color(0xFFB2B2B2)
                ),
                maxLines = 1,

                )






            Button(

                shape = RoundedCornerShape(12.dp),
                enabled = boolean,
                modifier = Modifier
                    .padding(start = 24.dp, end = 24.dp, top = 16.dp)
                    .height(48.dp)
                    .fillMaxWidth(),
                onClick = {
                    onEventDispatcher.invoke(SignUpContract.Intent.PhoneNumber(phoneNumber.value))
                    onEventDispatcher.invoke(
                        SignUpContract.Intent.ClickButtonSave(
                            SignUpData(
                                phone = phoneNumber.value,
                                password = password.value,
                                firstName = firstName.value,
                                lastName = lastName.value,
                                bornDate = data.value,
                                gender = gender.value
                            )
                        )
                    )
                },
                colors = ButtonDefaults.buttonColors(

                    containerColor = GreenColor,
                    disabledContainerColor = DefaultButtonColor


                )
            ) {
                Text("Save")

            }

            Row {
                Text(modifier = Modifier
                    .padding(top = 16.dp, end = 4.dp),
                    text = stringResource(R.string.already_have_an_account),
                    fontWeight = FontWeight(700),
                    fontFamily = FontFamily(Font(R.font.nata_semibold)),
                    fontSize = 14.sp,
                    color = Color.Black)

                Text(modifier = Modifier
                    .clickable {
                        onEventDispatcher.invoke(SignUpContract.Intent.ClickTextOpenLogIn)
                    }
                    .padding(top = 14.dp),
                    text = stringResource(R.string.log_in),
                    fontFamily = FontFamily(Font(R.font.nata_semibold)),
                    fontWeight = FontWeight(700),
                    fontSize = 16.sp,
                    color = GreenColor)

            }



        }


    }


}

@Composable
@Preview
fun SingUpPreview() {
    SignUpContent(SignUpContract.UIState.Display(""))
}




