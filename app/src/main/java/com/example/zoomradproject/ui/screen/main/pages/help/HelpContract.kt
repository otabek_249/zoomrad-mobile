package com.example.zoomradproject.ui.screen.main.pages.help

import android.content.Context
import com.example.zoomradproject.ui.screen.main.pages.home.HomeContract
import com.example.zoomradproject.ui.screen.main.pages.more.MoreContract
import org.orbitmvi.orbit.ContainerHost

interface HelpContract {


    interface HelpViewModel :
        ContainerHost<UIState, SideEffect> {
        fun onEventDispatcher(intent: Intent)
    }


    sealed interface UIState {
        data object Screen : UIState
    }

    sealed interface SideEffect {
        data object  OpenDialog: SideEffect
    }

    sealed interface Intent {
        data class ClickCall(val context: Context) :
           Intent

        data object ClickMap : Intent

        data object OpenDialog:Intent
        data object DialogButtonYes: Intent

        data class OpenDrawer(val index: Int): Intent
    }
}