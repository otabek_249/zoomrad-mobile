package com.example.zoomradproject.ui.screen.monitoring

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.zoomradproject.data.model.Transaction
import com.example.zoomradproject.data.remote.auth_api.MobileApi
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class TestPaginationSource  @Inject constructor( val api: MobileApi) : PagingSource<Int, Transaction>() {

    override fun getRefreshKey(state: PagingState<Int, Transaction>): Int? {
        return state.anchorPosition?.let { anchor ->
            state.closestPageToPosition(anchor)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchor)?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Transaction> {
        val page = params.key ?: 1
        return try {
            val response = api.getHistory(10, page)
            if (response.isSuccessful && response.body() != null) {
                LoadResult.Page(
                    data = response.body()!!.child,
                    nextKey = if (response.body()!!.totalPages > page) page.plus(1) else null,
                    prevKey = if (page > 1) page.minus(1) else null
                )
            } else {
                LoadResult.Error(HttpException(response))
            }
        } catch (e: IOException) {
            LoadResult.Error(e)
        } catch (e: HttpException) {
            LoadResult.Error(e)
        }
    }
}