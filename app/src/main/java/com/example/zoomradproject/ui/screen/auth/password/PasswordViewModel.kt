package com.example.zoomradproject.ui.screen.auth.password

import androidx.lifecycle.ViewModel
import com.example.zoomradproject.ui.screen.auth.password2.Password2Contract
import dagger.hilt.android.lifecycle.HiltViewModel
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject


@HiltViewModel
class PasswordViewModel @Inject constructor(
    private val navigation: PasswordDirection
) : PasswordContract.PasswordViewModel, ViewModel() {


    override val container = container<PasswordContract.UIState, PasswordContract.SideEffect>(
        PasswordContract.UIState.Screen
    )

    override fun onEventDispatcher(intent: PasswordContract.Intent) = intent {
        when (intent) {
            PasswordContract.Intent.OpenMainScreen -> {
                navigation.openMainScreen()
            }

        }
    }
}