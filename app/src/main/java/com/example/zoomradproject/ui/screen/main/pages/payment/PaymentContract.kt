package com.example.zoomradproject.ui.screen.main.pages.payment

import com.example.zoomradproject.ui.screen.main.pages.more.MoreContract
import org.orbitmvi.orbit.ContainerHost

interface PaymentContract {


    interface PaymentViewModel :ContainerHost<UIState,SideEffect>{
        fun onEventDispatcher(intent:Intent)
    }

    data class UIState(
        val any:String ? = null
    )

    sealed interface SideEffect{
        data object  OpenDialog: SideEffect
    }

    sealed interface Intent{
        data class OpenDrawer(val index :Int):Intent

        data object DialogButtonYes: Intent
        data object OpenDialog: Intent
    }

}