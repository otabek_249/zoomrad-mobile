package com.example.zoomradproject.ui.screen.main.pages.transfer

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.Scaffold
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.OffsetMapping
import androidx.compose.ui.text.input.TransformedText
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import cafe.adriel.voyager.hilt.getViewModel
import cafe.adriel.voyager.navigator.tab.Tab
import cafe.adriel.voyager.navigator.tab.TabOptions
import com.example.zoomradproject.R
import com.example.zoomradproject.data.pref.MyPref
import com.example.zoomradproject.data.remote.request.TransferDataRequest
import com.example.zoomradproject.data.remote.response.CardsResponse
import com.example.zoomradproject.ui.screen.add_card.formatToCard
import com.example.zoomradproject.ui.screen.cards.addCommasToNumber
import com.example.zoomradproject.ui.screen.main.pages.home.CustomLogOutDialog
import com.example.zoomradproject.ui.screen.main.pages.payment.PaymentContract
import com.example.zoomradproject.ui.theme.EditTextBgColor
import com.example.zoomradproject.ui.theme.GreenColor
import com.example.zoomradproject.ui.theme.MainBgColor
import com.example.zoomradproject.ui.theme.TextColor
import com.example.zoomradproject.utils.CardNumberVisualTransformation
import com.example.zoomradproject.utils.MyDrawer
import kotlinx.coroutines.launch
import org.orbitmvi.orbit.compose.collectAsState
import org.orbitmvi.orbit.compose.collectSideEffect

object TransferPage : Tab {
    private fun readResolve(): Any = TransferPage


    override val options: TabOptions
        @Composable
        get() {
            val title = "Transfer"
            val icon = painterResource(id = R.drawable.ic_transfer)

            return remember {
                TabOptions(
                    index = 2u,
                    title = title,
                    icon = icon
                )
            }

        }

    @Composable
    override fun Content() {
        val viewModel: TransferContract.TransferViewModel = getViewModel<TransferViewModel>()
        val uiState = viewModel.collectAsState()

        var boolean by remember {
            mutableStateOf(false)
        }
        if(boolean) {
            CustomLogOutDialog(
                onConfirm = {
                    viewModel.onEventDispatcher(TransferContract.Intent.DialogButtonYes)
                    boolean=false
                }) {
                boolean = false
            }
        }
        viewModel.collectSideEffect {
            when(it){
                TransferContract.SideEffect.OpenDialog->{
                    boolean = true
                }
            }

        }

        TransferContent(uiState = uiState, onEventDispatcher = viewModel::onEventDispatcher)
    }
}

@Composable
fun TransferContent(
    uiState: State<TransferContract.UIState>,
    onEventDispatcher: (TransferContract.Intent) -> Unit
) {
    var cardNumber by remember {
        mutableStateOf("")
    }
    var value by remember {
        mutableStateOf("")
    }

    val cardLifetime = remember {
        mutableStateOf("")
    }
    val cardName = remember {
        mutableStateOf("")
    }
    var selectedItemIndex by rememberSaveable {
        mutableIntStateOf(0)
    }
    var index by remember {
        mutableStateOf(0)
    }
    index = MyPref.getInstance().getCardIndex()
    var selectCardData by remember {

        mutableStateOf(
            CardsResponse(
                id = 0,
                name = "",
                amount = "",
                pan = "",
                expiredMonth = "",
                expiredYear = "",
                themeType = 1,
                isVisible = true,
                owner = ""
            )
        )
    }
    if (uiState.value.list.isNotEmpty()) {
        selectCardData=uiState.value.list[index]
    }
    if(cardNumber.length==16){
        onEventDispatcher.invoke(TransferContract.Intent.GetOwnerByPan(cardNumber))
    }

    val drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)
    val scope = rememberCoroutineScope()
    index = uiState.value.index
    ModalNavigationDrawer(
        drawerState = drawerState, drawerContent = {
            MyDrawer { index ->
                selectedItemIndex = index

                if(index==7){
                    onEventDispatcher(TransferContract.Intent.OpenDialog)
                }
                else {
                    onEventDispatcher.invoke(TransferContract.Intent.OpenDrawer(index))
                }

            }
        }, gesturesEnabled = true
    ) {

        Scaffold(
            modifier = Modifier.fillMaxSize(),
            contentColor = MainBgColor

        )
        { padding ->
            Column(
                Modifier
                    .fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally,


                )
            {
                Row(
                    modifier = Modifier
                        .padding(start = 16.dp, end = 16.dp, top = 24.dp),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Image(
                        modifier = Modifier
                            .clickable {
                                scope.launch {
                                    drawerState.apply {
                                        if (isClosed) open() else close()
                                    }
                                }
                            }
                            .height(24.dp)
                            .width(24.dp)
                            .padding(3.dp),
                        painter = painterResource(id = R.drawable.ic_menu),
                        contentDescription = "",
                    )
                    Spacer(modifier = Modifier.weight(1f))
                    Text(

                        text = stringResource(R.string.transfer_card_from_other_card),
                        fontSize = 16.sp,
                        color = TextColor,
                        fontFamily = FontFamily(Font(R.font.nata_bold))
                    )
                    Spacer(modifier = Modifier.weight(1f))


                    Image(
                        modifier = Modifier
                            .height(24.dp)
                            .width(24.dp)
                            .padding(3.dp),
                        painter = painterResource(id = R.drawable.ic_notification),
                        contentDescription = ""
                    )
                }

                Row(
                    modifier = Modifier

                        .padding(start = 24.dp, top = 8.dp, end = 24.dp)
                        .fillMaxWidth()
                        .height(48.dp)
                        .shadow(elevation = 2.dp, RoundedCornerShape(12.dp))
                        .background(color = EditTextBgColor, RoundedCornerShape(12.dp)),

                    ) {
                    OutlinedTextField(
                        modifier = Modifier
                            .padding(start = 4.dp, end = 24.dp)
                            .weight(1f)
                            .height(48.dp),
                        shape = RoundedCornerShape(12.dp),


                        value = cardNumber,
                        onValueChange = {

                            cardNumber = it
                        },


                        colors = TextFieldDefaults.textFieldColors(
                            backgroundColor = EditTextBgColor,
                            focusedIndicatorColor = EditTextBgColor,
                            cursorColor = Color.Black,
                            unfocusedIndicatorColor = EditTextBgColor,
                            placeholderColor = Color(0xFFB2B2B2)
                        ),
                        maxLines = 1,
                        keyboardOptions = KeyboardOptions(
                            keyboardType = KeyboardType.Number,
                            imeAction = ImeAction.Done
                        ),
                        visualTransformation = CardNumberVisualTransformation(),
                        keyboardActions = KeyboardActions.Default,


                        )
                    Image(
                        modifier = Modifier
                            .clickable {

                            }
                            .height(48.dp)
                            .width(48.dp)
                            .padding(10.dp),
                        painter = painterResource(id = R.drawable.ic_card_scanner),
                        contentDescription = ""
                    )
                    Spacer(modifier = Modifier.width(8.dp))
                }
                Box(
                    modifier = Modifier
                        .padding(start = 24.dp, top = 6.dp)
                        .fillMaxWidth(),

                    ) {
                    Text(
                        text = if(cardNumber.length==16){uiState.value.nameCardOwner.toUpperCase()} else{""},
                        fontSize = 14.sp,
                        fontFamily = FontFamily(Font(R.font.nata_bold)),
                        color = GreenColor
                    )

                }


                Text(
                    modifier = Modifier
                        .padding(top = 16.dp),
                    text = stringResource(R.string.transfer_text),
                    fontFamily = FontFamily(Font(R.font.nata_bold)),
                    color = Color.Black,
                    fontSize = 18.sp
                )

                Box(
                    modifier = Modifier
                        .fillMaxWidth(),
                    contentAlignment = Alignment.Center
                ) {
                    OutlinedTextField(
                        visualTransformation = MoneyFormatTransformation,
                        modifier = Modifier
                            .padding(top = 8.dp, bottom = 8.dp)
                            .width(280.dp)
                            .height(56.dp),
                        value = value,
                        onValueChange = {
                            if (value.length == 18) {
                                return@OutlinedTextField
                            } else {
                                value = it
                            }


                        },

                        textStyle = TextStyle(
                            color = GreenColor,
                            textAlign = TextAlign.Center,
                            fontSize = 28.sp,
                            fontFamily = FontFamily(Font(R.font.nata_semibold))
                        ),
                        colors = TextFieldDefaults.textFieldColors(
                            backgroundColor = Color(0xFFFEFAFD),
                            focusedIndicatorColor = Color(0xFFFEFAFD),
                            cursorColor = Color.Black,
                            unfocusedIndicatorColor = Color(0xFFFEFAFD),

                            ),
                        placeholder = {
                            Text(
                                modifier = Modifier
                                    .height(56.dp)
                                    .fillMaxWidth(),
                                textAlign = TextAlign.Center,
                                text = "0 so'm",
                                fontSize = 28.sp,
                                fontFamily = FontFamily(Font(R.font.nata_bold))
                            )
                        }
                    )

                }

                Row(
                    modifier = Modifier
                        .padding(horizontal = 24.dp)

                ) {
                    Row(
                        modifier = Modifier
                            .weight(1f),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Box(
                            modifier = Modifier
                                .width(60.dp)
                                .height(60.dp)
                                .background(Color.White, shape = RoundedCornerShape(12.dp)),
                            contentAlignment = Alignment.Center
                        ) {
                            Image(
                                modifier = Modifier
                                    .height(50.dp)
                                    .width(50.dp)
                                    .padding(10.dp),
                                painter = painterResource(id = R.drawable.ic_create_autopay),
                                contentDescription = ""
                            )

                        }
                        Text(
                            modifier = Modifier
                                .padding(start = 6.dp),
                            fontFamily = FontFamily(Font(R.font.nata_semibold)),
                            fontSize = 12.sp,
                            text = "Mening kartamga o'takish"
                        )

                    }
                    Spacer(modifier = Modifier.width(48.dp))
                    Row(
                        modifier = Modifier
                            .weight(1f),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Box(
                            modifier = Modifier
                                .width(60.dp)
                                .height(60.dp)
                                .background(Color.White, shape = RoundedCornerShape(12.dp)),
                            contentAlignment = Alignment.Center
                        ) {
                            Image(
                                modifier = Modifier
                                    .height(50.dp)
                                    .width(50.dp)
                                    .padding(10.dp),
                                painter = painterResource(id = R.drawable.ic_create_autopay),
                                contentDescription = ""
                            )

                        }
                        Text(
                            modifier = Modifier
                                .padding(start = 6.dp),
                            fontFamily = FontFamily(Font(R.font.nata_semibold)),
                            fontSize = 12.sp,
                            text = "Mening kartamga o'takish"
                        )

                    }

                }
                Spacer(modifier = Modifier.weight(1f))
                Card(
                    modifier = Modifier
                        .clickable {
                            onEventDispatcher.invoke(TransferContract.Intent.Selected)
                        }
                        .padding(start = 24.dp, end = 24.dp, top = 24.dp)
                        .height(148.dp)
                        .fillMaxWidth(),
                    colors = CardDefaults.cardColors(GreenColor),
                    shape = RoundedCornerShape(12.dp)
                ) {
                    Column(
                        modifier = Modifier
                            .fillMaxSize()
                    ) {
                        Row(
                            modifier = Modifier,
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Image(
                                modifier = Modifier
                                    .padding(start = 18.dp, top = 16.dp)
                                    .height(24.dp)
                                    .width(24.dp),
                                painter = painterResource(id = R.drawable.ic_humo),
                                contentDescription = ""
                            )
                            Text(
                                modifier = Modifier
                                    .padding(start = 18.dp, top = 16.dp),
                                text = selectCardData.name,
                                fontFamily = FontFamily(Font(R.font.nata_semibold)),
                                fontSize = 16.sp,
                                color = Color.White
                            )

                        }



                        Spacer(modifier = Modifier.weight(1f))
                        Text(
                            modifier = Modifier
                                .padding(start = 18.dp, bottom = 8.dp),
                            text = "0008 **** **** " + selectCardData.pan,
                            fontFamily = FontFamily(Font(R.font.nata_semibold)),
                            fontSize = 14.sp,
                            color = Color.White
                        )

                        Text(
                            modifier = Modifier
                                .padding(start = 18.dp, bottom = 8.dp),
                            text = selectCardData.amount.formatToCard() + "so'm",
                            fontFamily = FontFamily(Font(R.font.nata_semibold)),
                            fontSize = 18.sp,
                            color = Color.White
                        )




                        Spacer(modifier = Modifier.height(16.dp))

                    }


                }
                Button(

                    shape = RoundedCornerShape(12.dp),
                    enabled = cardNumber.length>15,
                    modifier = Modifier
                        .padding(start = 24.dp, end = 24.dp, top = 16.dp)
                        .height(48.dp)
                        .fillMaxWidth(),
                    onClick = {
                        onEventDispatcher.invoke(
                            TransferContract.Intent.TransferCard(
                                TransferDataRequest(
                                    type = "third-card",
                                    senderId = selectCardData.id.toString(),
                                    receiverPan = cardNumber,
                                    amount = value.trim().toLong()
                                )
                            )
                        )
                    },
                    colors = ButtonDefaults.buttonColors(

                        containerColor = GreenColor,
                        disabledContainerColor = Color.Gray


                    )
                ) {
                    Text(
                        "Davom ettirish",
                        fontSize = 16.sp,
                        color = Color.White
                    )

                }
                Text(
                    modifier = Modifier
                        .padding(start = 24.dp, end = 24.dp, bottom = 64.dp),
                    textAlign = TextAlign.Center,
                    fontSize = 16.sp,
                    text = stringResource(R.string.continue_button)
                )


            }


        }
    }
}

@Composable
@Preview
fun TransferPreview() {
    TransferContent(remember {
        mutableStateOf(TransferContract.UIState())
    }) {}

}


object MoneyFormatTransformation : VisualTransformation {
    override fun filter(text: AnnotatedString): TransformedText {
        val originalText = text.text
        val out = StringBuilder()
        var count = 0

        // Insert spaces from the right for every three digits
        for (i in originalText.indices.reversed()) {
            if (originalText[i].isDigit()) {
                out.insert(0, originalText[i])
                count++
                if (count % 3 == 0 && i != 0) {
                    out.insert(0, ' ')
                }
            } else {
                out.insert(0, originalText[i])
            }
        }

        val transformedText = out.toString()

        val offsetMapping = object : OffsetMapping {
            override fun originalToTransformed(offset: Int): Int {
                var extraSpaces = 0
                var digits = 0
                for (i in 0 until offset) {
                    if (originalText[i].isDigit()) {
                        digits++
                        if (digits % 3 == 0 && i != originalText.length - 1) {
                            extraSpaces++
                        }
                    }
                }
                return offset + extraSpaces
            }

            override fun transformedToOriginal(offset: Int): Int {
                var extraSpaces = 0
                var transOffset = offset
                while (transOffset > 0 && extraSpaces < offset) {
                    transOffset--
                    if (transformedText[transOffset] == ' ') {
                        extraSpaces++
                    }
                }
                return offset - extraSpaces
            }
        }

        return TransformedText(AnnotatedString(transformedText), offsetMapping)
    }
}
