package com.example.zoomradproject.ui.screen.cards

import android.annotation.SuppressLint
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Scaffold
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.hilt.getViewModel
import cafe.adriel.voyager.navigator.bottomSheet.LocalBottomSheetNavigator
import com.example.zoomradproject.R
import com.example.zoomradproject.data.pref.MyPref
import com.example.zoomradproject.data.remote.response.CardsResponse
import com.example.zoomradproject.ui.activity.CustomDeleteDialog
import com.example.zoomradproject.ui.screen.add_card.formatToCard
import com.example.zoomradproject.ui.screen.cards.components.CardBottomSheet
import com.example.zoomradproject.ui.theme.EditTextBgColor
import com.example.zoomradproject.ui.theme.GreenColor
import com.example.zoomradproject.ui.theme.MainBgColor
import com.example.zoomradproject.utils.navigation.AppNavigator
import org.orbitmvi.orbit.compose.collectAsState
import org.orbitmvi.orbit.compose.collectSideEffect
import javax.inject.Inject

class CardsScreen(private val index: Int) : Screen {

    @Composable
    override fun Content() {

        var state by remember {
            mutableStateOf(false)
        }


        val viewModel: CardsContract.CardsViewModel = getViewModel<CardViewModelImpl>()
        val bottomSheetNavigator = LocalBottomSheetNavigator.current
        val uiState = viewModel.collectAsState().value
        var id by remember {
            mutableIntStateOf(0)
        }


        if (state) {
            CustomDeleteDialog(
                onConfirm = {
                    viewModel.onEventDispatcher(
                        CardsContract.Intent.DeleteCard(id.toString())

                    )
                    state = false
                },
                onDismiss = {
                    state = false
                })
        }
        viewModel.collectSideEffect {data->
            when (data) {
                is CardsContract.SideEffect.ClickCards -> {
                    bottomSheetNavigator.show(CardBottomSheet(data.cardsResponse) {
                        when (it) {
                            1 -> viewModel.onEventDispatcher(CardsContract.Intent.OpenMonitoringScreen)
                            2 -> {
                                viewModel.onEventDispatcher(
                                    CardsContract.Intent.OpenCardsSettingsScreen(data.cardsResponse))

                            }

                            3 -> {

                            }

                            4 -> {}
                            else -> {
                                id = data.cardsResponse.id
                                state = true
                            }
                        }
                    })

                }
            }
        }
        CardsContent(
            uiState = uiState,
            onEventDispatcher = viewModel::onEventDispatcher,
            index = index
        )

    }
}


@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun CardsContent(
    uiState: CardsContract.UIState,
    onEventDispatcher: (CardsContract.Intent) -> Unit,
    index: Int
) {
    val list = uiState.listCards
    Scaffold(

        modifier = Modifier
            .fillMaxSize(),
        contentColor = EditTextBgColor,
    ) {

        Column(
            modifier = Modifier
                .background(MainBgColor)
                .fillMaxSize()
        ) {

            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(56.dp)
            ) {
                Row(
                    modifier = Modifier.padding(start = 16.dp, end = 16.dp, top = 24.dp),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Image(
                        modifier = Modifier
                            .clickable {
                                onEventDispatcher.invoke(CardsContract.Intent.Back)
                            }
                            .height(24.dp)
                            .width(24.dp)
                            .padding(3.dp),
                        painter = painterResource(id = R.drawable.ic_back_green),
                        contentDescription = "",

                        )
                    Spacer(modifier = Modifier.weight(1f))
                    Text(
                        text = stringResource(R.string.my_cards),
                        fontSize = 18.sp,
                        color = Color.Gray,
                        fontFamily = FontFamily(Font(R.font.nata_semibold))
                    )
                    Spacer(modifier = Modifier.weight(1f))

                    Spacer(
                        modifier = Modifier
                            .height(24.dp)
                            .width(24.dp)
                            .padding(3.dp),
                    )
                }
            }

            LazyColumn(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f)
            ) {

                list.forEach { data ->
                    item {
                        CardsItem(cardsResponse = data, index=data.themeType) {
//                            onEventDispatcher.invoke(CardsContract.Intent.ClickCards(it))
                            if (index == 0) {
                                onEventDispatcher(CardsContract.Intent.ClickCards(it))
                            } else {
                                onEventDispatcher.invoke(
                                    CardsContract.Intent.SetIndex(
                                        list.indexOf(
                                            it
                                        )
                                    )
                                )
                                onEventDispatcher.invoke(CardsContract.Intent.Back)

                            }

                        }
                    }
                }
            }
            Row(
                modifier = Modifier
                    .padding(bottom = 6.dp)
                    .fillMaxWidth()
                    .height(56.dp)
            ) {
                Row(
                    modifier = Modifier
                        .clickable { }
                        .padding(horizontal = 24.dp)
                        .weight(1f),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Box(
                        modifier = Modifier
                            .shadow(4.dp, shape = RoundedCornerShape(12.dp))
                            .width(60.dp)
                            .height(60.dp)
                            .background(Color.White, shape = RoundedCornerShape(12.dp)),
                        contentAlignment = Alignment.Center
                    ) {
                        Image(
                            modifier = Modifier
                                .height(50.dp)
                                .width(50.dp)
                                .padding(10.dp),
                            painter = painterResource(id = R.drawable.ic_create_autopay),
                            contentDescription = ""
                        )

                    }
                    androidx.compose.material.Text(
                        modifier = Modifier
                            .padding(start = 6.dp),
                        fontFamily = FontFamily(Font(R.font.nata_semibold)),
                        fontSize = 12.sp,
                        color = Color.Black,
                        text = "Virtual karta ochish"
                    )

                }
                Spacer(modifier = Modifier.width(48.dp))
                Row(
                    modifier = Modifier
                        .clickable {
                            onEventDispatcher.invoke(CardsContract.Intent.OpenCarsScreen)
                        }
                        .weight(1f),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Box(
                        modifier = Modifier
                            .shadow(4.dp, shape = RoundedCornerShape(12.dp))
                            .width(60.dp)
                            .height(60.dp)
                            .background(Color.White, shape = RoundedCornerShape(12.dp)),
                        contentAlignment = Alignment.Center
                    ) {
                        Image(
                            modifier = Modifier
                                .height(50.dp)
                                .width(50.dp)
                                .padding(10.dp),
                            painter = painterResource(id = R.drawable.ic_create_autopay),
                            contentDescription = ""
                        )

                    }
                    Text(
                        modifier = Modifier
                            .padding(start = 6.dp),
                        fontFamily = FontFamily(Font(R.font.nata_semibold)),
                        fontSize = 12.sp,
                        text = "Karta qo'shish"
                    )

                }

            }


        }


    }
}


private val list = arrayListOf(
    CardsResponse(
        0,
        "UzCard",
        amount = "10000",
        owner = "Otabek Chorshanbiyev",
        pan = "9303",
        expiredYear = "2005",
        expiredMonth = "12",
        themeType = 1,
        isVisible = false
    ),
    CardsResponse(
        0,
        "UzCard",
        amount = "10000",
        owner = "Otabek Chorshanbiyev",
        pan = "9303",
        expiredYear = "2005",
        expiredMonth = "12",
        themeType = 1,
        isVisible = false
    ),


    )

val data = arrayListOf(
    R.drawable.img_card1,
    R.drawable.img_card2,
    R.drawable.img_card3,
    R.drawable.img_card4,
    R.drawable.img_card5,
    R.drawable.img_card6,
    R.drawable.img_card7,
)

@Preview
@Composable
fun CardsPreview() {
    CardsContent(CardsContract.UIState(list), index = 1, onEventDispatcher = {})

}


@Composable
fun CardsItem(cardsResponse: CardsResponse, index: Int, clickCard: (CardsResponse) -> Unit) {
    var index2 by remember {
        mutableStateOf(0)
    }
    if(index<7){
        index2 = index
    }

    Card(
        modifier = Modifier
            .padding(start = 32.dp, end = 32.dp, top = 24.dp)
            .height(180.dp)
            .fillMaxWidth()
            .clickable {
                clickCard.invoke(cardsResponse)
            },
        colors = CardDefaults.cardColors(GreenColor),
        shape = RoundedCornerShape(12.dp)
    ) {
        Box(
            modifier = Modifier
                .fillMaxSize()
        ) {
            Image(
                modifier = Modifier
                    .fillMaxSize(),
                contentScale = ContentScale.Crop,
                painter = painterResource(id = data[index2]), contentDescription = ""
            )
            Column(
                modifier = Modifier
                    .fillMaxSize()
            ) {
                Image(
                    modifier = Modifier
                        .padding(start = 18.dp, top = 24.dp)
                        .height(42.dp)
                        .width(42.dp),
                    painter = painterResource(id = R.drawable.ic_humo),
                    contentDescription = ""
                )

                Text(
                    modifier = Modifier
                        .padding(start = 18.dp, top = 16.dp),
                    text = cardsResponse.name,
                    fontFamily = FontFamily(Font(R.font.nata_semibold)),
                    fontSize = 16.sp,
                    color = Color.White
                )
                Spacer(modifier = Modifier.weight(1f))
                Row(
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(
                        cardsResponse.amount.addCommasToNumber(),
                        fontSize = 20.sp,
                        fontFamily = FontFamily(Font(R.font.nata_semibold)),
                        color = Color.White,
                        modifier = Modifier
                            .padding(start = 16.dp, bottom = 8.dp, top = 8.dp)
                    )
                    Text(
                        modifier = Modifier
                            .padding(start = 4.dp),
                        fontSize = 13.sp,
                        color = Color.White,
                        fontFamily = FontFamily(Font(R.font.nata_semibold)),
                        text = "so'm"
                    )
                }

                Row(
                    modifier = Modifier
                        .padding(start = 18.dp)
                ) {
                    Text(
                        modifier = Modifier,
                        text = "0008********" + cardsResponse.pan.formatToCard(),
                        fontFamily = FontFamily(Font(R.font.nata_semibold)),
                        fontSize = 13.sp,
                        color = Color.White
                    )
                    Text(
                        modifier = Modifier
                            .padding(start = 8.dp),
                        text = cardsResponse.expiredMonth + "/" + cardsResponse.expiredYear.substring(
                            2
                        ),
                        fontFamily = FontFamily(Font(R.font.nata_semibold)),
                        fontSize = 13.sp,
                        color = Color.White
                    )

                }

                Spacer(modifier = Modifier.height(16.dp))

            }
        }


    }
}

fun String.addCommasToNumber(): String {
    val parts = this.split(".")
    val integerPart = parts[0]
    val decimalPart = if (parts.size > 1) "." + parts[1] else ""
    val regex = "(\\d)(?=(\\d{3})+(?!\\d))"
    val formattedIntegerPart = integerPart.replace(Regex(regex), "$1 ")
    return formattedIntegerPart + decimalPart
}
