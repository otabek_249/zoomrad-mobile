package com.example.zoomradproject.ui.screen.cards

import com.example.zoomradproject.data.remote.response.CardsResponse
import org.orbitmvi.orbit.ContainerHost

interface CardsContract {

    interface CardsViewModel :ContainerHost<UIState,SideEffect>{
        fun onEventDispatcher(intent: Intent)

    }

   data class UIState(
       var listCards:List<CardsResponse> = emptyList()
   )

    sealed interface SideEffect{
        data class ClickCards(val cardsResponse: CardsResponse) :SideEffect
    }

    sealed interface Intent{
        data object Back:Intent
        data object OpenCarsScreen:Intent

        data class OpenCardsSettingsScreen(val cardsResponse: CardsResponse) :Intent

        data object OpenMonitoringScreen:Intent
        data class ClickCards(val cardsResponse: CardsResponse) :Intent
        data class SetIndex(val index:Int):Intent

        data class DeleteCard(val id:String):Intent
    }
}