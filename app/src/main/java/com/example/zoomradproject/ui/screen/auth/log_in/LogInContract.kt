package com.example.zoomradproject.ui.screen.auth.log_in

import com.example.zoomradproject.data.remote.request.LogInRequest
import org.orbitmvi.orbit.ContainerHost

interface LogInContract {

    interface LogInViewModel : ContainerHost<UIState, SideEffect> {
        fun onEventDispatcher(intent: Intent)
    }


    data class UIState (
       val msg: String ="",
        val state :Boolean =false
    )

    sealed interface SideEffect {
        data class TextToast(val msg: String) : SideEffect
    }

    sealed interface Intent {
        data object ClickTextOpenSignUpScreen : Intent
        data class ClickButton(val logInRequest: LogInRequest) : Intent
        data class PhoneNumber(val phone:String):Intent
    }
}