package com.example.zoomradproject.ui.screen.auth.verify

import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.hilt.getViewModel
import com.example.zoomradproject.R
import com.example.zoomradproject.ui.screen.auth.log_in.formatPhoneNumber
import com.example.zoomradproject.ui.theme.DefaultButtonColor
import com.example.zoomradproject.ui.theme.EditTextBgColor
import com.example.zoomradproject.ui.theme.GreenColor
import com.example.zoomradproject.ui.theme.MainBgColor
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.orbitmvi.orbit.compose.collectAsState
import org.orbitmvi.orbit.compose.collectSideEffect

class VerifyScreen : Screen {
    @Composable
    override fun Content() {

        val model: VerifyContract.VerifyModel = getViewModel<VerifyViewModel>()
        val uiState = model.collectAsState().value
        val context = LocalContext.current
        model.collectSideEffect {
            when(it){
                is VerifyContract.SideEffect.ToastSideEffect->{
                    Toast.makeText(context, it.msg,Toast.LENGTH_SHORT).show()
                }
            }
        }
        VerifyContent(uiState, model::onEventDispatcher)
    }
}


@Composable
fun VerifyContent(
    uiState: VerifyContract.UIState,
    onEventDispatcher: (VerifyContract.Intent) -> Unit = {}
) {
    val password = remember { mutableStateOf("") }
    val max = 6

    onEventDispatcher.invoke(VerifyContract.Intent.First)

    var time by remember { mutableStateOf(120) } // total time in seconds (2 minutes)
    val formattedTime = String.format("%02d:%02d", time / 60, time % 60)
    val coroutineScope = rememberCoroutineScope()

    LaunchedEffect(Unit) {
        coroutineScope.launch {
            while (time > 0) {
                delay(1000L)
                time--
            }
            // Trigger timeout action here
            // For example: show a toast, navigate to another screen, etc.
            println("Timeout for OTP")
        }
    }



    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MainBgColor),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        Box(
            modifier = Modifier
                .fillMaxWidth()
                .weight(0.3f),
            contentAlignment = Alignment.BottomCenter
        ) {

            Image(
                painter = painterResource(id = R.drawable.ic_zoomrad),
                contentDescription = "",
                modifier = Modifier
                    .height(100.dp)
                    .width(100.dp)
            )

        }
        Spacer(modifier = Modifier.height(32.dp))



        Column(
            modifier = Modifier
                .weight(1f)
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally,
        )
        {
            Text(
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
                    .padding(horizontal = 24.dp),
                text = stringResource(id = R.string.send_code_for_number),
                fontSize = 16.sp,
                textAlign = TextAlign.Center,
                overflow = TextOverflow.Ellipsis,
                fontWeight = FontWeight(400)
            )
            Spacer(modifier = Modifier.height(8.dp))
            Text(
                text = uiState.phone.formatPhoneNumber(),
                fontSize = 16.sp
            )
            Box(
                modifier = Modifier
                    .padding(24.dp)
                    .fillMaxWidth()
                    .height(48.dp)
                    .background(color = EditTextBgColor, RoundedCornerShape(12.dp)),
                contentAlignment = Alignment.Center

            ) {
                OutlinedTextField(
                    modifier = Modifier
                        .padding(start = 24.dp, end = 24.dp)
                        .width(100.dp)
                        .height(48.dp),
                    shape = RoundedCornerShape(12.dp),
                    singleLine = true,

                    value = password.value,
                    onValueChange = {
                        if (it.length <= max) {
                            password.value = it
                        }


                    },





                    colors = TextFieldDefaults.textFieldColors(
                        backgroundColor = EditTextBgColor,
                        focusedIndicatorColor = EditTextBgColor,
                        cursorColor = Color.Black,
                        unfocusedIndicatorColor = EditTextBgColor,
                        placeholderColor = Color(0xFFB2B2B2)
                    ),
                    maxLines = 1,

                    )
            }


            /*    Row(
                    modifier = Modifier
                        .padding(horizontal = 24.dp)
                        .fillMaxWidth(),
                    verticalAlignment = Alignment.CenterVertically

                ) {
                    Spacer(modifier = Modifier.weight(1f))

                    Text(
                        modifier = Modifier
                            .padding(end = 8.dp),
                        fontSize = 16.sp,
                        fontFamily = FontFamily(Font(R.font.nata_semibold)),
                        text = formattedTime)
                    Box(
                        modifier = Modifier
                            .clickable {

                            }
                            .shadow(5.dp, shape = RoundedCornerShape(10.dp))
                            .background(color = Color.White, shape = RoundedCornerShape(10.dp))
                            .height(60.dp)
                            .width(60.dp)
                    )
                    {
                        Image(

                            modifier = Modifier
                                .padding(15.dp)
                                .fillMaxSize(),

                            painter = painterResource(R.drawable.ic_smartphone),
                            contentDescription = ""
                        )

                    }
                    Text(
                        modifier = Modifier
                            .clickable {
                                onEventDispatcher.invoke(VerifyContract.Intent.First)
                            }
                            .width(100.dp)
                            .padding(start = 8.dp),
                        style = TextStyle(
                            letterSpacing = TextUnit(5f, type = TextUnitType(1))
                        ),
                        text = "Kodni qayta yuborish",
                        fontFamily = FontFamily(Font(R.font.nata_bold)),
                        fontSize = 14.sp
                    )


                }*/


            Button(
                enabled = password.value.length>5,
                shape = RoundedCornerShape(12.dp),
                modifier = Modifier
                    .padding(start = 24.dp, end = 24.dp, top = 8.dp)
                    .height(48.dp)
                    .fillMaxWidth(),
                onClick = {
                    onEventDispatcher.invoke(VerifyContract.Intent.ButtonClickSend(code = password.value))
                },
                colors = ButtonDefaults.buttonColors(

                    containerColor = GreenColor,
                    disabledContainerColor = DefaultButtonColor


                )
            ) {
                Text(stringResource(R.string.send))

            }

        }


    }

}


@Preview
@Composable
fun VerifyPreview() {
    VerifyContent(VerifyContract.UIState("+998 930720405"))

}

