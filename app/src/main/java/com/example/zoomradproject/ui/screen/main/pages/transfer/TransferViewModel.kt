package com.example.zoomradproject.ui.screen.main.pages.transfer

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.zoomradproject.data.pref.MyPref
import com.example.zoomradproject.data.remote.request.GetOwnerByPanRequest
import com.example.zoomradproject.data.remote.request.TransferDataRequest
import com.example.zoomradproject.repository.main.AppRepository
import com.example.zoomradproject.ui.screen.add_card.AddCardContract
import com.example.zoomradproject.ui.screen.main.pages.payment.PaymentContract
import com.example.zoomradproject.utils.myLog
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.orbitmvi.orbit.Container
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.syntax.simple.postSideEffect
import org.orbitmvi.orbit.syntax.simple.reduce
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject


@HiltViewModel
class TransferViewModel @Inject constructor(
    private val repo:AppRepository,
    private val direction: TransferDirection
) :ViewModel(),TransferContract.TransferViewModel{
    private val myPref = MyPref.getInstance()
    init {
        getAllCards()

    }



    override val container = container<TransferContract.UIState,TransferContract.SideEffect>(TransferContract.UIState())


    override fun onEventDispatcher(intent: TransferContract.Intent) = intent {
       when(intent){
           is TransferContract.Intent.TransferCard->{
               transferCard(intent.data)
           }
           is TransferContract.Intent.GetOwnerByPan->{
               getOwnerByPan(GetOwnerByPanRequest(intent.cardName))
           }
           TransferContract.Intent.Selected->{
               direction.openSelectCard()
           }
           is TransferContract.Intent.OpenDrawer->{
               direction.openDrawer(intent.index)
           }
           TransferContract.Intent.DialogButtonYes->{
               myPref.setAccessToke("otabek")
               myPref.setRefreshToken("otabek")
               myPref.setCardIndex(0)
               myPref.setPassword("")
               direction.logOut()
           }

           TransferContract.Intent.OpenDialog->{
               postSideEffect(TransferContract.SideEffect.OpenDialog)
           }
       }
    }


    private fun transferCard(data:TransferDataRequest){
        repo.transferCardByPan(data)
            .onEach {
                it.onSuccess {
                    direction.openVerify(it.token)
                }
                it.onFailure {

                }

            }
            .launchIn(viewModelScope)

    }

    private fun getAllCards(){
        repo.getCards()
            .onEach {
                it.onSuccess {
                    intent { reduce { TransferContract.UIState(list = it) } }
                }
                it.onFailure {

                }
            }
            .launchIn(viewModelScope)
    }

    private fun getOwnerByPan( pan: GetOwnerByPanRequest){

        repo.getOwnerByPan(pan)
            .onEach {
                it.onSuccess {
                   intent { reduce { TransferContract.UIState(nameCardOwner = it.name) } }
                }
                it.onFailure {
                    intent { reduce { TransferContract.UIState("can't find") } }
                }
            }

            .launchIn(viewModelScope)
    }






}