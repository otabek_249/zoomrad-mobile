package com.example.zoomradproject.ui.screen.monitoring

import android.net.http.HttpException
import android.os.Build
import androidx.annotation.RequiresExtension
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.zoomradproject.data.model.Transaction
import com.example.zoomradproject.data.remote.response.MonitoringResponse
import com.example.zoomradproject.repository.main.AppRepository
import java.io.IOException
import javax.inject.Inject

class MonitoringPagingSource @Inject constructor(
    private val repo: AppRepository
) : PagingSource<Int, Transaction>() {

    override fun getRefreshKey(state: PagingState<Int, Transaction>): Int? = state.anchorPosition

    @RequiresExtension(extension = Build.VERSION_CODES.S, version = 7)
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Transaction> {
        val page = params.key ?: 1
        return try {
            val response = repo.getHistories(page, params.loadSize)
            LoadResult.Page(
                data = response.child,
                prevKey = if (page == 1) null else page - 1,
                nextKey = if (response.child.isEmpty()) null else page + 1
            )
        } catch (e: IOException) {
            LoadResult.Error(e)
        } catch (e: HttpException) {
            LoadResult.Error(e)
        }
    }
}
