package com.example.zoomradproject.ui.screen.settings.main

import com.example.zoomradproject.utils.navigation.AppNavigator
import javax.inject.Inject


interface SettingsDirection {
    suspend fun back()
}

class SettingsDirectionImpl @Inject constructor
    (private val navigator: AppNavigator) : SettingsDirection {
    override suspend fun back() {
        navigator.back()
    }
}