package com.example.zoomradproject.ui.screen.auth.password2

import com.example.zoomradproject.ui.screen.auth.password.PasswordDirection
import com.example.zoomradproject.ui.screen.main.MainScreen
import com.example.zoomradproject.utils.navigation.AppNavigator
import javax.inject.Inject


interface Password2Direction {
    suspend fun openMainScreen()
}

class Password2DirectionImpl @Inject constructor(
    private val appNavigator: AppNavigator
) : PasswordDirection {
    override suspend fun openMainScreen() {
        appNavigator.replaceAll(MainScreen())
    }

}