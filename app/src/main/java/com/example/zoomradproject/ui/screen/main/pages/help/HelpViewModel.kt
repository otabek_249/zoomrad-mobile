package com.example.zoomradproject.ui.screen.main.pages.help

import androidx.lifecycle.ViewModel
import com.example.zoomradproject.data.pref.MyPref
import com.example.zoomradproject.ui.screen.main.pages.more.MoreContract
import dagger.hilt.android.lifecycle.HiltViewModel
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.syntax.simple.postSideEffect
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject


@HiltViewModel
class HelpViewModel @Inject constructor(
    private val navigation:HelpDirection
) : ViewModel(), HelpContract.HelpViewModel {
    val  myPref = MyPref.getInstance()
    override fun onEventDispatcher(intent: HelpContract.Intent) = intent {
        when (intent) {
           HelpContract.Intent.ClickMap -> {
                navigation.screenMap()
            }

            is HelpContract.Intent.ClickCall -> {

            }
            is   HelpContract.Intent.OpenDrawer->{
            navigation.openDrawer(intent.index)
        }
            HelpContract.Intent.DialogButtonYes->{
                myPref.setAccessToke("otabek")
                myPref.setRefreshToken("otabek")
                myPref.setCardIndex(0)
                myPref.setPassword("")
                navigation.logOut()
            }

            HelpContract.Intent.OpenDialog->{
                postSideEffect(HelpContract.SideEffect.OpenDialog)
            }

        }


    }

    override val container =
        container<HelpContract.UIState, HelpContract.SideEffect>(HelpContract.UIState.Screen)
}