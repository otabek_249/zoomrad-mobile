package com.example.zoomradproject.ui.screen.profile

import com.example.zoomradproject.utils.navigation.AppNavigator
import javax.inject.Inject

interface ProfileDirection{
    suspend fun back()
}
class ProfileDirectionImpl  @Inject constructor(
    private val appNavigator: AppNavigator
) :ProfileDirection {
    override suspend fun back() {
        appNavigator.back()
    }
}