package com.example.zoomradproject.ui.screen.map.branch

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.zoomradproject.repository.main.AppRepository
import com.example.zoomradproject.utils.myLog
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.syntax.simple.postSideEffect
import org.orbitmvi.orbit.syntax.simple.reduce
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject

@HiltViewModel
class BranchViewModel @Inject constructor(
    private val repository: AppRepository,
    private val navigation: BranchDirection
) : ViewModel(), BranchContract.BranchViewModel {

    override val container = container<BranchContract.UIState, BranchContract.SideEffect>(
        BranchContract.UIState()
    )

    init {

        viewModelScope.launch {

            getMapData()
        }

    }

    private fun getMapData() {
        repository.getMapsData()
            .onEach { result ->
                result.onSuccess { data ->
                    "$data map ".myLog()
                    intent {
                        reduce { BranchContract.UIState(data) }
                    }
                }.onFailure { exception ->
                    // Handle the failure case if needed
                    "Error fetching map data: ${exception.message}".myLog()
                }
            }
            .launchIn(viewModelScope)
    }

    override fun onEventDispatcher(intent: BranchContract.Intent) = intent {
        when (intent) {
            is BranchContract.Intent.ClickMarker -> {
                postSideEffect(BranchContract.SideEffect.BottomSheetDialog(intent.markerData))
            }

            BranchContract.Intent.BackClick -> {
                navigation.goBack()
            }
        }
    }
}
