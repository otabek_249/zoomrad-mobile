package com.example.zoomradproject.ui.screen.add_card

import com.example.zoomradproject.utils.navigation.AppNavigator
import javax.inject.Inject


interface AddCardDirection{
    suspend fun back()
}
class AddCardDirectionImpl @Inject constructor(
    private val appNavigator: AppNavigator
) :AddCardDirection{
    override suspend fun back() {
        appNavigator.back()
    }
}