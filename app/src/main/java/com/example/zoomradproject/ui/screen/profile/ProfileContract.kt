package com.example.zoomradproject.ui.screen.profile

import com.example.zoomradproject.data.model.UserData
import org.orbitmvi.orbit.ContainerHost

interface ProfileContract {

    interface ProfileViewModel :ContainerHost<UIState,SideEffect>{
        fun  onEventDispatcher(intent:Intent)
    }


    data class UIState(
        var userData: UserData? = null
    )


    sealed interface SideEffect{

    }
    sealed interface Intent{
        data class ClickButton(val userData: UserData) :Intent

        data object Back:Intent
    }
}