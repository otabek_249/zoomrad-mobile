package com.example.zoomradproject.ui.screen.add_card

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.PagerState
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.hilt.getViewModel
import com.example.zoomradproject.R
import com.example.zoomradproject.data.remote.request.AddCardData
import com.example.zoomradproject.data.remote.request.GetOwnerByPanRequest
import com.example.zoomradproject.ui.theme.EditTextBgColor
import com.example.zoomradproject.ui.theme.GreenColor
import com.example.zoomradproject.ui.theme.ItemBgColor
import com.example.zoomradproject.ui.theme.MainBgColor
import com.example.zoomradproject.ui.theme.TextColor
import com.example.zoomradproject.utils.CardNumberVisualTransformation
import com.example.zoomradproject.utils.myLog
import kotlinx.coroutines.sync.Mutex
import org.orbitmvi.orbit.compose.collectAsState
import java.util.Locale

class AddCardScreen : Screen {
    @Composable
    override fun Content() {
        val model: AddCardContract.AddCardViewModel = getViewModel<AddCardViewModelImpl>()
        val uiState = model.collectAsState()
        AddCardContent(uiState, model::onEventDispatcher)
    }
}
val data= arrayListOf(
    R.drawable.img_card1,
    R.drawable.img_card2,
    R.drawable.img_card3,
    R.drawable.img_card4,
    R.drawable.img_card5,
    R.drawable.img_card6,
    R.drawable.img_card7,
)

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun AddCardContent(
    uiState: State<AddCardContract.UIState>,
    onEventDispatcher: (AddCardContract.Intent) -> Unit
) {
    val pagerState = rememberPagerState(pageCount = {
        7
    })
    val cardNumber = remember {
        mutableStateOf("")
    }
    var cardLifetime by remember {
        mutableStateOf("")
    }
    val cardName = remember {
        mutableStateOf("")
    }
    var boolean by remember {
        mutableStateOf(false)
    }
    if(cardNumber.value.length==16){
        onEventDispatcher.invoke(AddCardContract.Intent.GetByOwner(cardNumber.value))
    }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
            .background(MainBgColor)
    ) {
        Row(
            modifier = Modifier
                .padding(top =24.dp)
        ) {
            Image(
                modifier = Modifier
                    .clickable {
                        onEventDispatcher.invoke(AddCardContract.Intent.BackText)
                    }
                    .padding(start = 16.dp)
                    .height(24.dp)
                    .width(24.dp),
                painter = painterResource(id = R.drawable.ic_back_green),
                contentDescription = "OpenDrawer"
            )
            Spacer(modifier = Modifier.weight(1f))

            Text(
                fontSize = 20.sp,
                color = TextColor,
                fontFamily = FontFamily(Font(R.font.nata_bold)),
                text = stringResource(id = R.string.add_card)
            )
            Spacer(modifier = Modifier.weight(1f))
            Text(
                modifier = Modifier
                    .clickable {
                        onEventDispatcher.invoke(AddCardContract.Intent.BackText)
                    }
                    .padding(end = 4.dp),
                fontSize = 19.sp,
                fontFamily = FontFamily(Font(R.font.nata_bold)),
                color = TextColor,
                text = stringResource(id = R.string.close)
            )

        }

        HorizontalPager(state = pagerState) {

            Card(
                modifier = Modifier
                    .padding(start = 32.dp, end = 32.dp, top = 24.dp)
                    .height(190.dp)
                    .fillMaxWidth(),
                colors = CardDefaults.cardColors(GreenColor),
                shape = RoundedCornerShape(12.dp)
            ) {
                Box(modifier = Modifier
                    .fillMaxSize()){
                    Image(
                        modifier = Modifier
                            .fillMaxSize(),
                        contentScale = ContentScale.Crop,
                        painter = painterResource(id = data[pagerState.currentPage]), contentDescription ="")
                    Column(
                        modifier = Modifier
                            .fillMaxSize()
                    ) {
                        Image(
                            modifier = Modifier
                                .padding(start = 18.dp, top = 24.dp)
                                .height(42.dp)
                                .width(42.dp),
                            painter = painterResource(id = R.drawable.ic_humo),
                            contentDescription = ""
                        )

                        Text(
                            modifier = Modifier
                                .padding(start = 18.dp, top = 16.dp),
                            text = cardName.value,
                            fontFamily = FontFamily(Font(R.font.nata_semibold)),
                            fontSize = 16.sp,
                            color = Color.White
                        )
                        Spacer(modifier = Modifier.weight(1f))
                        Row(
                            modifier = Modifier
                                .padding(start = 18.dp)
                        ) {
                            Text(
                                modifier = Modifier,
                                text = cardNumber.value.formatToCard(),
                                fontFamily = FontFamily(Font(R.font.nata_semibold)),
                                fontSize = 14.sp,
                                color = Color.White
                            )
                            Text(
                                modifier = Modifier
                                    .padding(start = 8.dp),
                                text = cardLifetime.formatToData(),
                                fontFamily = FontFamily(Font(R.font.nata_semibold)),
                                fontSize = 14.sp,
                                color = Color.White
                            )

                        }

                        Spacer(modifier = Modifier.height(16.dp))

                    }


                }



            }

        }
        DotIndicator(
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .padding(top = 8.dp),
            totalDots = 7, selectedIndex =pagerState,
            dotSize = 16,
            activeColor = Color.Gray,
            padding = 6
        )


        Row(
            modifier = Modifier
                .padding(start = 24.dp, end = 24.dp, top = 24.dp)
                .height(24.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            Text(
                text = stringResource(R.string.card_number),
                fontSize = 16.sp,
                fontFamily = FontFamily(Font(R.font.nata_semibold))
            )
            Spacer(modifier = Modifier.weight(1f))
            Text(
                text = if(cardNumber.value.length==16){uiState.value.nameCard.toUpperCase()} else{""},
                textAlign = TextAlign.Center,

                color = GreenColor,
                fontSize = 13.sp,
                fontFamily = FontFamily(Font(R.font.nata_semibold))
            )

        }


        Row(
            modifier = Modifier
                .padding(start = 24.dp, top = 8.dp, end = 24.dp)
                .fillMaxWidth()
                .height(48.dp)
                .background(color = EditTextBgColor, RoundedCornerShape(12.dp)),

            ) {
            OutlinedTextField(
                modifier = Modifier
                    .padding(start = 4.dp, end = 24.dp)
                    .weight(1f)
                    .height(48.dp),
                shape = RoundedCornerShape(12.dp),


                value = cardNumber.value,
                onValueChange = {
                    if (it.length <= 16) {
                        cardNumber.value = it
                    }
                },


                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = EditTextBgColor,
                    focusedIndicatorColor = EditTextBgColor,
                    cursorColor = Color.Black,
                    unfocusedIndicatorColor = EditTextBgColor,
                    placeholderColor = Color(0xFFB2B2B2)
                ),
                maxLines = 1,
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Number,
                    imeAction = ImeAction.Done
                ),
                visualTransformation = CardNumberVisualTransformation(),
                keyboardActions = KeyboardActions.Default,


                )
            Image(
                modifier = Modifier
                    .clickable {

                    }
                    .height(48.dp)
                    .width(48.dp)
                    .padding(10.dp),
                painter = painterResource(id = R.drawable.ic_card_scanner),
                contentDescription = ""
            )
            Spacer(modifier = Modifier.width(8.dp))
        }

        Row(
            modifier = Modifier
                .padding(top = 16.dp)
                .height(80.dp)
                .fillMaxWidth()
                .padding(start = 24.dp, end = 24.dp)
        ) {
            Column(
                modifier = Modifier
                    .fillMaxHeight()

            ) {
                Text(
                    text = stringResource(R.string.validity_period),
                    fontSize = 14.sp,
                    fontFamily = FontFamily(Font(R.font.nata_semibold))
                )
                OutlinedTextField(
                    modifier = Modifier
                        .padding(top = 4.dp)
                        .height(52.dp)
                        .width(120.dp),
                    shape = RoundedCornerShape(14.dp),


                    value = cardLifetime,
                    onValueChange = {
                        cardLifetime = it
                    },

                    colors = TextFieldDefaults.textFieldColors(
                        backgroundColor = EditTextBgColor,
                        focusedIndicatorColor = EditTextBgColor,
                        unfocusedIndicatorColor = EditTextBgColor,
                        placeholderColor = Color(0xFFB2B2B2)
                    ),
                    maxLines = 1,

                    )
                Spacer(modifier = Modifier.height(12.dp))

            }
            Spacer(modifier = Modifier.weight(0.2f))
            Column(
                modifier = Modifier
                    .weight(1f)
                    .fillMaxSize()

            ) {
                Text(
                    text = stringResource(R.string.card_name),
                    fontSize = 14.sp,
                    fontFamily = FontFamily(Font(R.font.nata_semibold))
                )
                OutlinedTextField(
                    modifier = Modifier
                        .padding(top = 4.dp)
                        .fillMaxHeight()
                        .fillMaxWidth(),
                    shape = RoundedCornerShape(14.dp),


                    value = cardName.value,
                    onValueChange = {
                        cardName.value = it
                    },

                    colors = TextFieldDefaults.textFieldColors(
                        backgroundColor = EditTextBgColor,
                        focusedIndicatorColor = EditTextBgColor,
                        unfocusedIndicatorColor = EditTextBgColor,
                        placeholderColor = Color(0xFFB2B2B2)
                    ),
                    maxLines = 1,

                    )
                Spacer(modifier = Modifier.height(12.dp))

            }


        }

        Box(modifier = Modifier.weight(1f))



        Row {
            Text(
                modifier = Modifier
                    .padding(start = 24.dp, top = 24.dp),

                text = stringResource(R.string.main_card),
                fontSize = 18.sp,
                fontFamily = FontFamily(Font(R.font.nata_semibold))
            )

        }

        Column(
            modifier = Modifier
                .padding(bottom = 24.dp)
                .fillMaxWidth()
        ) {
            Row(
                modifier = Modifier
                    .padding(start = 24.dp, end = 24.dp, top = 8.dp)
                    .shadow(elevation = 4.dp, shape = RoundedCornerShape(12.dp))
                    .background(color = ItemBgColor, shape = RoundedCornerShape(12.dp))
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Box {
                    Image(
                        modifier = Modifier
                            .padding(start = 8.dp, top = 4.dp, bottom = 4.dp, end = 8.dp)
                            .height(72.dp)
                            .width(72.dp),
                        painter = painterResource(id = R.drawable.image_people),
                        contentDescription = ""
                    )

                }
                Column {
                    Text(
                        fontSize = 18.sp,
                        color = GreenColor,
                        fontFamily = FontFamily(Font(R.font.nata_bold)),
                        text = "Kartaga buyurtma",
                    )
                    Text(
                        modifier = Modifier
                            .padding(end = 8.dp),
                        fontSize = 12.sp,
                        color = Color.Black,
                        fontFamily = FontFamily(Font(R.font.nata_semibold)),
                        text = "Bir nechta click va aloqa bank kartasi sizning qo'lingizda",
                    )

                }
            }


            Button(
                enabled = (cardName.value.length > 4 && cardNumber.value.length >= 14 && cardLifetime.length == 4),
                shape = RoundedCornerShape(12.dp),
                modifier = Modifier
                    .padding(start = 24.dp, end = 24.dp, top = 16.dp)
                    .height(48.dp)
                    .fillMaxWidth(),
                onClick = {
                    onEventDispatcher.invoke(
                        AddCardContract.Intent.AddButton(
                            AddCardData(
                                pan = cardNumber.value,
                                expiredYear = "20" + cardLifetime.substring(2),
                                expiredMonths = cardLifetime.substring(1, 2),
                                name = cardName.value
                            )
                        )
                    )
                },
                colors = ButtonDefaults.buttonColors(

                    containerColor = GreenColor,
                    disabledContainerColor = Color.Gray,


                    )
            ) {
                Text(stringResource(id = R.string.add_card))

            }


        }
    }
}


@Composable
@Preview(device = Devices.PIXEL_7, heightDp = 1080)
fun AddCardPreview() {
    AddCardContent(remember { mutableStateOf(AddCardContract.UIState()) }, {})
}


fun String.formatToCard(): String {
    val pattern = Regex("(\\d{4})")

    // Find all matches using the regex pattern
    val matches = pattern.findAll(this)

    // Join matched groups with spaces
    return matches.joinToString(separator = " ") { it.value }
}

// Example usage:

fun String.formatToData(): String {
    val pattern = Regex("(\\d{1,2})(\\d{1,2})")


    val matches = pattern.findAll(this)


    return matches.joinToString(separator = "/") { "${it.groupValues[1]}/${it.groupValues[2]}" }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun DotIndicator(
    modifier: Modifier,
    totalDots: Int,
    selectedIndex: PagerState,
    activeColor: Color = Color.Gray,
    inactiveColor: Color = Color.LightGray,
    dotSize: Int = 8,
    padding:Int=4
) {
    Row(
        modifier = modifier,
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically
    ) {
        repeat(totalDots) { iteration ->
            Box(
                modifier = Modifier
                    .padding(padding.dp)
                    .width(dotSize.dp)
                    .height(4.dp)
                    .clip(RoundedCornerShape(300.dp))
                    .background(
                        color = if (iteration == selectedIndex.currentPage) activeColor else inactiveColor,
                    )
            )
        }
    }
}

