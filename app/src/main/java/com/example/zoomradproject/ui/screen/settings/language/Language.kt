package com.example.zoomradproject.ui.screen.settings.language

import android.annotation.SuppressLint
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.zoomradproject.R



@Composable
fun LanguageItem(click: (Int) -> Unit) {

    var isExpanded by rememberSaveable {
        mutableStateOf(false)
    }

    Column(
        modifier = Modifier

            .fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {


        Row(
            modifier = Modifier
                .fillMaxWidth()
        ) {
            Image(
                modifier = Modifier
                    .padding(end = 8.dp)
                    .height(24.dp)
                    .width(24.dp),
                painter = painterResource(id = R.drawable.ic_world),
                contentDescription = ""
            )
            Text(
                text = stringResource(R.string.language),
                color = Color.Black,
                modifier = Modifier.padding(start = 8.dp),
                fontSize = 16.sp,
                fontFamily = FontFamily(Font(R.font.nata_bold))
            )
            Spacer(modifier = Modifier.weight(1f))

            if (isExpanded) {
                Image(
                    modifier = Modifier
                        .padding(end = 16.dp)
                        .height(22.dp)
                        .width(22.dp)
                        .padding(4.dp)
                        .rotate(180f)
                        .clickable {
                            isExpanded = !isExpanded
                        },

                    painter = painterResource(id = R.drawable.ic_bottom_line),
                    contentDescription = ""
                )
            } else {
                Image(
                    modifier = Modifier
                        .padding(end = 16.dp)
                        .height(22.dp)
                        .width(22.dp)
                        .padding(4.dp)
                        .clickable {
                            isExpanded = !isExpanded
                        },

                    painter = painterResource(id = R.drawable.ic_bottom_line),
                    contentDescription = ""
                )
            }

        }

        AnimatedVisibility(visible = isExpanded) {
            Column{
                Spacer(modifier = Modifier.height(4.dp))
                Text(
                    modifier = Modifier

                        .clickable {
                            click.invoke(1)
                        },
                    fontFamily = FontFamily(Font(R.font.nata_semibold)),
                    text = stringResource(id = R.string.uzbek),
                    fontSize = 15.sp
                )
                Spacer(modifier = Modifier.height(4.dp))
                Text(
                    modifier = Modifier
                        .clickable {
                            click.invoke(2)
                        },
                    fontFamily = FontFamily(Font(R.font.nata_semibold)),
                    text = stringResource(id = R.string.russian),
                    fontSize = 15.sp
                )
                Spacer(modifier = Modifier.height(4.dp))
                Text(
                    modifier = Modifier
                        .clickable {
                            click.invoke(3)
                        },
                    fontFamily = FontFamily(Font(R.font.nata_semibold)),
                    text = stringResource(id = R.string.english),
                    fontSize = 15.sp
                )
                Spacer(modifier = Modifier.height(4.dp))


            }
        }

    }
}


@Preview
@Composable
fun LanguagePreview() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.White)
    ) {
        LanguageItem {

        }
    }
}