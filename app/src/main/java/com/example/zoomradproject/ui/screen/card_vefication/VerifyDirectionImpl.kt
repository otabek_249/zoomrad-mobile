package com.example.zoomradproject.ui.screen.card_vefication

import com.example.zoomradproject.ui.screen.auth.password.PasswordScreen

import com.example.zoomradproject.utils.navigation.AppNavigator
import javax.inject.Inject


interface VerifyCardDirection {
    suspend fun back()
}

class VerifyCardDirectionImpl @Inject constructor(
    private val navigator: AppNavigator
) : VerifyCardDirection {
    override suspend fun back() {
        navigator.back()
    }

}