package com.example.zoomradproject.ui.screen.auth.sign_up

import com.example.zoomradproject.ui.screen.auth.log_in.LogInScreen
import com.example.zoomradproject.ui.screen.auth.verify.VerifyScreen
import com.example.zoomradproject.utils.navigation.AppNavigator
import javax.inject.Inject


interface SignUpDirection {
    suspend fun openVerifyScreen()
    suspend fun openLogInScreen()
}

class SignUpDirectionImpl @Inject constructor(
    private val navigation: AppNavigator
) : SignUpDirection {


    override suspend fun openVerifyScreen() {
        navigation.navigateTo(VerifyScreen())
    }

    override suspend fun openLogInScreen() {
        navigation.navigateTo(LogInScreen())
    }
}