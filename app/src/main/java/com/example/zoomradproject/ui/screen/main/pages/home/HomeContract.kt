package com.example.zoomradproject.ui.screen.main.pages.home

import com.example.zoomradproject.data.remote.response.CardsResponse
import org.orbitmvi.orbit.ContainerHost

interface HomeContract {
    interface HomeViewModel :ContainerHost<UIState,SideEffect>{
        fun onEventDispatcher(intent: Intent)
    }


    data class UIState(
        var cardsResponse: List<CardsResponse> = emptyList()
    )

    sealed interface SideEffect {
        data object  OpenDialog:SideEffect
    }

    sealed interface Intent {
        data object OpenAddCart : Intent
        data object OpenCardsScreen:Intent

        data object OpenDialog:Intent
        data object DialogButtonYes:Intent

        data class OpenDrawer(val index: Int):Intent
        data object Refresh:Intent


    }
}