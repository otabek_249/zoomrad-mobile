package com.example.zoomradproject.ui.screen.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.zoomradproject.data.pref.MyPref
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject


@HiltViewModel
class SplashViewModel  @Inject constructor(
    private val navigation : SplashDirection

) : SplashContract.SplashViewModel,ViewModel() {
    private var myPref =MyPref.getInstance()


    override val container = container<SplashContract.UIState, SplashContract.SideEffect>(
        SplashContract.UIState.Screen
    )



    override fun onEventDispatcher(intent: SplashContract.Intent) {
        when(intent){
            SplashContract.Intent.NextScreen ->{
                direction()
            }
        }
    }
    fun direction(){
        if(myPref.getAccessToken()=="otabek"){
            viewModelScope.launch {
                navigation.logInScreen()
            }

        }
        else{
            viewModelScope.launch {
                navigation.passwordScreen()
            }
        }
    }


}