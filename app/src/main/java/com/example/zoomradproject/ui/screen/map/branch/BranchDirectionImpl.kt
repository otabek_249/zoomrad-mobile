package com.example.zoomradproject.ui.screen.map.branch

import com.example.zoomradproject.utils.navigation.AppNavigator
import javax.inject.Inject

interface BranchDirection{
    suspend fun goBack()
}
class BranchDirectionImpl @Inject constructor(
    private val appNavigator: AppNavigator
) :BranchDirection {
    override suspend fun goBack() {
        appNavigator.back()
    }
}