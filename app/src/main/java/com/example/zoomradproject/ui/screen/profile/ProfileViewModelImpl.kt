package com.example.zoomradproject.ui.screen.profile

import androidx.lifecycle.ViewModel
import com.example.zoomradproject.data.model.UserData
import dagger.hilt.android.lifecycle.HiltViewModel
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject


@HiltViewModel
class ProfileViewModelImpl
@Inject constructor(
    private val direction: ProfileDirection
) : ProfileContract.ProfileViewModel, ViewModel() {
    override fun onEventDispatcher(intent: ProfileContract.Intent) = intent {
        when (intent) {
            is ProfileContract.Intent.ClickButton -> {

            }
            ProfileContract.Intent.Back->{
                direction.back()
            }
        }
    }

    override val container = container<ProfileContract.UIState, ProfileContract.SideEffect>(ProfileContract.UIState())

    fun a () {
        container.stateFlow.value.userData = null
    }
}