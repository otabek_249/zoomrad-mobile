package com.example.zoomradproject.ui.screen.auth.verify

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.zoomradproject.data.pref.MyPref
import com.example.zoomradproject.data.remote.request.VerificationData
import com.example.zoomradproject.repository.auth.impl.AuthRepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.syntax.simple.postSideEffect
import org.orbitmvi.orbit.syntax.simple.reduce
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject


@HiltViewModel
class VerifyViewModel @Inject constructor(
    private val navigation: VerifyDirection,
    private val repositoryImpl: AuthRepositoryImpl
) : ViewModel(), VerifyContract.VerifyModel {
    private val myPref = MyPref.getInstance()





    override val container =
        container<VerifyContract.UIState, VerifyContract.SideEffect>(VerifyContract.UIState())

    override fun onEventDispatcher(intent: VerifyContract.Intent) = intent {

        when (intent) {
           is VerifyContract.Intent.ButtonClickSend -> {
               verification(intent.code)
            }
            VerifyContract.Intent.First->{
                intent { reduce {VerifyContract.UIState(myPref.getPhone()) } }
            }
        }
    }

   private fun verification(code: String){
        repositoryImpl.verification(verificationData = VerificationData(myPref.getTokenFirst(),code))
            .onEach { it ->
                it.onSuccess {
                    navigation.openPasswordScreen()
                }
                it.onFailure {
                    repositoryImpl.verificationSignUp(verificationData = VerificationData(myPref.getTokenFirst(),code))
                        .onEach {

                            it.onSuccess {
                                navigation.openPasswordScreen()
                            }
                            it.onFailure {
                                intent { postSideEffect(VerifyContract.SideEffect.ToastSideEffect(it.message.toString())) }
                            }
                        }
                        .launchIn(viewModelScope)
                }
            }
            .launchIn(viewModelScope)
    }


}