package com.example.zoomradproject.ui.dialog

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.core.screen.Screen
import com.example.zoomradproject.data.model.MarkerData
import com.example.zoomradproject.utils.BottomSheetContent


data class MapDialog(val markerData: MarkerData, val click:(Unit)->Unit): Screen {
    @Composable
    override fun Content() {
        BottomSheetContent(markerData = markerData) {
            click.invoke(Unit)
        }

    }
}