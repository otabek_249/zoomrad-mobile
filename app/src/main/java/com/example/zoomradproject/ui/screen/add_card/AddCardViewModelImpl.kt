package com.example.zoomradproject.ui.screen.add_card

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.zoomradproject.data.remote.request.AddCardData
import com.example.zoomradproject.data.remote.request.GetOwnerByPanRequest
import com.example.zoomradproject.repository.main.AppRepository
import com.example.zoomradproject.utils.myLog
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.syntax.simple.reduce
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject


@HiltViewModel
class AddCardViewModelImpl @Inject constructor(
    private val appRepository: AppRepository,
    private val navigation: AddCardDirection
) : ViewModel(), AddCardContract.AddCardViewModel {
    var count =0

    override val container =
        container<AddCardContract.UIState, AddCardContract.SideEffect>(AddCardContract.UIState())


    override fun onEventDispatcher(intent: AddCardContract.Intent) = intent {
        when (intent) {
            AddCardContract.Intent.BackText -> {
                navigation.back()
            }

            is AddCardContract.Intent.AddButton -> {
                addCard(intent.data)
            }
            is AddCardContract.Intent.GetByOwner->{
                getOwnerByPan(GetOwnerByPanRequest(intent.string))
            }
        }

    }

    private fun addCard(addCardData: AddCardData) {
        "Add Card viewModel is work".myLog()
        appRepository.addCardData(addCardData)
            .onEach {
                it.onSuccess {msg->

                    viewModelScope.launch {
                        navigation.back()
                    }
                }
                it.onFailure{ msg->


                }
            }
            .launchIn(viewModelScope)

    }
    private fun getOwnerByPan( pan:GetOwnerByPanRequest){
        count++
        "$count".myLog()
        "${pan.pan}".myLog()
        appRepository.getOwnerByPan(pan)
            .onEach {
                it.onSuccess {
                    intent { reduce {AddCardContract.UIState(it.name)} }
                    "${it.name}".myLog()
                }
                it.onFailure {   intent { reduce {AddCardContract.UIState("can't find")} } }
            }

            .launchIn(viewModelScope)
    }


}