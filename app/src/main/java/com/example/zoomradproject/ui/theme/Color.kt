package com.example.zoomradproject.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)
val BottomColor = Color(0xFFD9D9D9)
val MainBgColor = Color(0xFFF3F3F3)
val ItemBgColor = Color(0xFFFDFAFA)
val GreenColor = Color(0xFF239C71)
val DefaultButtonColor = Color(0xFF7BAA99)
val ButtonBgColor = Color(0xFFFEFEFE)
val EditTextBgColor = Color(0xFFFEFEFE)
val TextColor =Color(0xFF3E3E3E)