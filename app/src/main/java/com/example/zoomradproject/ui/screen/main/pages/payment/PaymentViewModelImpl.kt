package com.example.zoomradproject.ui.screen.main.pages.payment

import androidx.lifecycle.ViewModel
import com.example.zoomradproject.data.pref.MyPref
import com.example.zoomradproject.repository.main.AppRepository
import com.example.zoomradproject.ui.screen.main.pages.more.MoreContract
import dagger.hilt.android.lifecycle.HiltViewModel
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.syntax.simple.postSideEffect
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject


@HiltViewModel
class PaymentViewModelImpl @Inject constructor(
    private val repositoryImpl: AppRepository,
    private val direction: PaymentDirection

) :ViewModel(),PaymentContract.PaymentViewModel {
    val  myPref = MyPref.getInstance()
    override fun onEventDispatcher(intent: PaymentContract.Intent) = intent {
        when(intent){
            is  PaymentContract.Intent.OpenDrawer ->{
                direction.openDrawer(intent.index)
            }
            PaymentContract.Intent.DialogButtonYes->{
                myPref.setAccessToke("otabek")
                myPref.setRefreshToken("otabek")
                myPref.setCardIndex(0)
                myPref.setPassword("")
                direction.logOut()
            }

            PaymentContract.Intent.OpenDialog->{
                postSideEffect(PaymentContract.SideEffect.OpenDialog)
            }
        }
    }

    override val container = container<PaymentContract.UIState,PaymentContract.SideEffect>(PaymentContract.UIState())
}