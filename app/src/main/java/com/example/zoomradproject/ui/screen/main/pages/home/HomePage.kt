package com.example.zoomradproject.ui.screen.main.pages.home

import android.annotation.SuppressLint
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Card
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import cafe.adriel.voyager.hilt.getViewModel
import cafe.adriel.voyager.navigator.tab.Tab
import cafe.adriel.voyager.navigator.tab.TabOptions
import com.example.zoomradproject.R
import com.example.zoomradproject.data.model.Item2Data
import com.example.zoomradproject.data.pref.MyPref
import com.example.zoomradproject.ui.screen.add_card.formatToCard
import com.example.zoomradproject.ui.screen.cards.addCommasToNumber
import com.example.zoomradproject.ui.theme.GreenColor
import com.example.zoomradproject.ui.theme.ItemBgColor
import com.example.zoomradproject.ui.theme.MainBgColor
import com.example.zoomradproject.ui.theme.TextColor
import com.example.zoomradproject.utils.CurrencyItem
import com.example.zoomradproject.utils.Item1
import com.example.zoomradproject.utils.Item2
import com.example.zoomradproject.utils.MyDrawer
import kotlinx.coroutines.launch
import org.orbitmvi.orbit.compose.collectAsState
import org.orbitmvi.orbit.compose.collectSideEffect

object HomePage : Tab {
    private fun readResolve(): Any = HomePage
    override val options: TabOptions
        @Composable get() {
            val title = "Home"
            val icon = painterResource(id = R.drawable.ic_home)

            return remember {
                TabOptions(
                    index = 0u, title = title, icon = icon
                )
            }
        }

    @Composable
    override fun Content() {
        val model: HomeContract.HomeViewModel = getViewModel<HomeViewModelImpl>()
        val uiState = model.collectAsState()
        // dialog
        var boolean by remember {
            mutableStateOf(false)
        }


        if (boolean) {
            CustomLogOutDialog(
                onConfirm = {
                    model.onEventDispatcher(HomeContract.Intent.DialogButtonYes)
                    boolean = false
                }) {
                boolean = false
            }
        }

        model.collectSideEffect {
            when (it) {
                HomeContract.SideEffect.OpenDialog -> {
                    boolean = true
                }
            }

        }
        HomeContent(uiState, model::onEventDispatcher)


    }
}

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun HomeContent(
    uiState: State<HomeContract.UIState>,
    onEventDispatcher: (HomeContract.Intent) -> Unit
) {
    var index by rememberSaveable {
        mutableIntStateOf(0)
    }
    index = MyPref.getInstance().getCardIndex()
    var selectedItemIndex by rememberSaveable {
        mutableIntStateOf(0)
    }
    var balanceState by remember {
        mutableStateOf(false)
    }

    val drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)
    val scope = rememberCoroutineScope()



    ModalNavigationDrawer(
        drawerState = drawerState, drawerContent = {
            MyDrawer { index ->

                selectedItemIndex = index
                if (index == 7) {
                    onEventDispatcher(HomeContract.Intent.OpenDialog)
                } else {
                    onEventDispatcher.invoke(HomeContract.Intent.OpenDrawer(index = index))
                }
            }
        }, gesturesEnabled = true
    ) {
        Scaffold(
            modifier = Modifier.fillMaxSize(),
            contentColor = MainBgColor,
        ) { padding ->

            Column(
                modifier = Modifier

                    .fillMaxSize()
            ) {
                Row(
                    modifier = Modifier.padding(start = 16.dp, end = 16.dp, top = 24.dp),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Image(
                        modifier = Modifier
                            .clickable {
                                scope.launch {
                                    drawerState.apply {
                                        if (isClosed) open() else close()
                                    }
                                }

                            }
                            .height(24.dp)
                            .width(24.dp)
                            .padding(3.dp),
                        painter = painterResource(id = R.drawable.ic_menu),
                        contentDescription = "",
                    )
                    Spacer(modifier = Modifier.weight(1f))


                    Image(
                        modifier = Modifier
                            .height(24.dp)
                            .width(24.dp)
                            .padding(3.dp),
                        painter = painterResource(id = R.drawable.ic_notification),
                        contentDescription = ""
                    )
                }

                Box(
                    modifier = Modifier
                        .padding(top = 12.dp)
                        .height(200.dp)
                        .fillMaxWidth()
                ) {
                    Box(
                        modifier = Modifier
                            .padding(horizontal = 24.dp)
                            .fillMaxWidth()
                            .height(180.dp)
                            .background(GreenColor, shape = RoundedCornerShape(12.dp))
                    )
                    Box(
                        modifier = Modifier
                            .padding(horizontal = 16.dp)
                            .height(170.dp)
                            .background(
                                color = Color.Transparent, shape = RoundedCornerShape(16.dp)
                            )
                            .fillMaxWidth()
                    ) {
                        Image(
                            contentScale = ContentScale.Crop,
                            painter = painterResource(id = R.drawable.image_card_bacground),
                            modifier = Modifier
                                .fillMaxSize()
                                .clip(RoundedCornerShape(16.dp)),
                            contentDescription = "",
                        )
                        Column(
                            modifier = Modifier
                                .padding(top = 24.dp, start = 24.dp, end = 24.dp)
                                .fillMaxSize()
                        ) {
                            Text(
                                text = stringResource(R.string.all_balans),
                                fontSize = 14.sp,
                                fontFamily = FontFamily(Font(R.font.nata_regular))
                            )
                            Row(
                                modifier = Modifier
                                    .weight(1f)
                                    .padding(top = 24.dp)
                                    .fillMaxWidth()
                                    .height(24.dp)
                            ) {
                                if (balanceState) {
                                    Image(
                                        modifier = Modifier
                                            .clickable {
                                                balanceState = false
                                            }
                                            .height(24.dp)
                                            .width(24.dp),
                                        painter = painterResource(id = R.drawable.un_read),
                                        contentDescription = "see",
                                        colorFilter = ColorFilter.tint(Color.White)
                                    )
                                } else {
                                    Image(
                                        modifier = Modifier
                                            .clickable {
                                                balanceState = true
                                            }
                                            .height(24.dp)
                                            .width(24.dp),
                                        painter = painterResource(id = R.drawable.ic_see),
                                        contentDescription = "see"
                                    )
                                }
                                Spacer(modifier = Modifier.width(12.dp))
                                if (uiState.value.cardsResponse.isNotEmpty()) {

                                    if (balanceState) {
                                        Text(
                                            text = "*******",
                                            textAlign = TextAlign.Center,
                                            color = Color.White,
                                            fontSize = 26.sp,
                                            fontFamily = FontFamily(Font(R.font.nata_semibold))
                                        )
                                    } else {
                                        Text(
                                            text = uiState.value.cardsResponse[index].amount.addCommasToNumber(),
                                            color = Color.White,
                                            fontSize = 26.sp,
                                            fontFamily = FontFamily(Font(R.font.nata_semibold))
                                        )
                                    }
                                } else {
                                    Text(
                                        text = "",
                                        color = Color.White,
                                        fontSize = 26.sp,
                                        fontFamily = FontFamily(Font(R.font.nata_semibold))
                                    )
                                }

                            }
                            Row(
                                modifier = Modifier.weight(1f),
                                verticalAlignment = Alignment.CenterVertically
                            ) {
                                Image(modifier = Modifier
                                    .clickable {
                                        onEventDispatcher.invoke(HomeContract.Intent.OpenAddCart)
                                    }
                                    .width(36.dp)
                                    .height(36.dp),
                                    painter = painterResource(id = R.drawable.ic_add),
                                    contentDescription = "")
                                Spacer(modifier = Modifier.width(8.dp))
                                if (uiState.value.cardsResponse.isEmpty()) {

                                    Text(
                                        modifier = Modifier.clickable {
                                            onEventDispatcher.invoke(HomeContract.Intent.OpenAddCart)
                                        },
                                        text = stringResource(R.string.add_card_text),
                                        color = Color.White,
                                        fontSize = 18.sp,
                                        fontFamily = FontFamily(Font(R.font.nata_semibold))
                                    )
                                } else {
                                    Text(
                                        modifier = Modifier.clickable {
                                            onEventDispatcher.invoke(HomeContract.Intent.OpenCardsScreen)
                                        },
                                        text = stringResource(R.string.move_card),
                                        color = Color.White,
                                        fontSize = 18.sp,
                                        fontFamily = FontFamily(Font(R.font.nata_semibold))
                                    )
                                }
                                Spacer(modifier = Modifier.weight(1f))
                                Image(
                                    modifier = Modifier
                                        .clickable {
                                            onEventDispatcher.invoke(HomeContract.Intent.Refresh)
                                        }
                                        .padding(end = 8.dp)
                                        .width(32.dp)
                                        .height(32.dp),
                                    painter = painterResource(id = R.drawable.ic_reload),
                                    contentDescription = ""
                                )


                            }
                            Spacer(modifier = Modifier.height(24.dp))


                        }


                    }

                }

                Column(
                    modifier = Modifier
                        .verticalScroll(rememberScrollState())
                        .fillMaxWidth()
                        .weight(1f)
                ) {

                    Row(
                        modifier = Modifier.padding(start = 16.dp, end = 16.dp, top = 8.dp)
                    ) {
                        Item1(
                            modifier = Modifier
                                .weight(1f)
                                .height(64.dp),
                            name = "Keshbek",
                            number = "0",
                            currency = "ball",
                            dotsClick = {},
                            imageClick = {},
                            image = R.drawable.ic_cash
                        )
                        Spacer(
                            modifier = Modifier.width(16.dp)
                        )
                        Item1(
                            modifier = Modifier
                                .weight(1f)
                                .height(60.dp),
                            name = "Keshbek",
                            number = "0",
                            currency = "ball",
                            dotsClick = {},
                            imageClick = {},
                            image = R.drawable.ic_chest
                        )

                    }
                    Row(
                        modifier = Modifier.padding(start = 16.dp, end = 16.dp, top = 8.dp)
                    ) {
                        Item1(
                            modifier = Modifier
                                .weight(1f)
                                .height(64.dp),
                            name = "Keshbek",
                            number = "0",
                            currency = "ball",
                            dotsClick = {},
                            imageClick = {},
                            image = R.drawable.ic_debit_card
                        )
                        Spacer(
                            modifier = Modifier.width(16.dp)
                        )
                        Item1(
                            modifier = Modifier
                                .weight(1f)
                                .height(60.dp),
                            name = "Keshbek",
                            number = "0",
                            currency = "ball",
                            dotsClick = {},
                            imageClick = {},
                            image = R.drawable.ic_credit_time
                        )

                    }
                    Row(
                        modifier = Modifier.padding(
                            start = 16.dp, end = 16.dp, top = 16.dp, bottom = 16.dp
                        )
                    ) {
                        Text(
                            text = "Takliflar",
                            color = TextColor,
                            fontFamily = FontFamily(Font(R.font.nata_regular)),
                            fontSize = 11.sp
                        )
                        Spacer(modifier = Modifier.weight(1f))
                        Text(
                            text = "Barchasi",
                            color = TextColor,
                            fontFamily = FontFamily(Font(R.font.nata_regular)),
                            fontSize = 11.sp
                        )

                    }
                    LazyRow {
                        list.forEach { data ->
                            item {
                                Item2(image = data.image, msg = data.msg)
                            }
                        }
                    }
                    Row(
                        modifier = Modifier.padding(
                            start = 16.dp, end = 16.dp, top = 16.dp, bottom = 16.dp
                        )
                    ) {
                        Text(
                            text = "Tezkor to'lovlar",
                            color = TextColor,
                            fontFamily = FontFamily(Font(R.font.nata_regular)),
                            fontSize = 11.sp
                        )
                        Spacer(modifier = Modifier.weight(1f))
                        Text(
                            text = "Barchasi",
                            color = TextColor,
                            fontFamily = FontFamily(Font(R.font.nata_regular)),
                            fontSize = 11.sp
                        )

                    }
                    Card(
                        modifier = Modifier
                            .padding(start = 16.dp)
                            .width(90.dp)
                            .height(90.dp),
                        backgroundColor = ItemBgColor,
                        shape = RoundedCornerShape(8.dp)
                    ) {
                        Image(
                            modifier = Modifier
                                .fillMaxSize()
                                .padding(30.dp),
                            painter = painterResource(id = R.drawable.ic_plus),
                            contentDescription = ""
                        )
                    }
                    Row(
                        modifier = Modifier.padding(
                            start = 16.dp, end = 16.dp, top = 16.dp, bottom = 16.dp
                        )
                    ) {
                        Text(
                            text = "Valyuta kursi",
                            color = TextColor,
                            fontFamily = FontFamily(Font(R.font.nata_regular)),
                            fontSize = 11.sp
                        )
                        Spacer(modifier = Modifier.weight(1f))
                        Text(
                            text = "Barchasi",
                            color = TextColor,
                            fontFamily = FontFamily(Font(R.font.nata_regular)),
                            fontSize = 11.sp
                        )

                    }
                    LazyRow {
                        for (i in 0 until 20) {

                            item { CurrencyItem() }
                        }
                    }
                    Spacer(modifier = Modifier.height(64.dp))

                }


            }

        }

    }


}

@Composable
@Preview
fun HomePreview() {
    HomeContent(remember {
        mutableStateOf(HomeContract.UIState())
    }) {}

}


val list = arrayListOf(
    Item2Data(R.drawable.img_hadj, "Haj to'lovi"),
    Item2Data(R.drawable.img_dizayn, "Individual dizayn"),
    Item2Data(R.drawable.img_sello, "Sello "),
    Item2Data(R.drawable.ic_visa_direct, "Visa Direct"),
    Item2Data(R.drawable.ic_visual_card, "Visa "),
    Item2Data(R.drawable.img_hadj, "Haj to'lovi"),
    Item2Data(R.drawable.img_dizayn, "Individual dizayn"),
    Item2Data(R.drawable.img_sello, "Sello "),
    Item2Data(R.drawable.ic_visa_direct, "Visa Direct"),
    Item2Data(R.drawable.ic_visual_card, "Visa "),

    )

@Composable
fun CustomLogOutDialog(
    onConfirm: () -> Unit,
    onDismiss: () -> Unit,
) {

    Dialog(
        onDismissRequest = { onDismiss() },

        ) {
        Column(

            modifier = Modifier

                .fillMaxWidth()
                .padding(16.dp)
                .background(MainBgColor, shape = RoundedCornerShape(12.dp)),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                painter = painterResource(id = R.drawable.image_log_out),
                contentDescription = null,
                modifier = Modifier
                    .fillMaxWidth()
                    .height(180.dp)
                    .padding(8.dp)
            )
            Text(
                text = "Ishonchiz komilli akkountdan chiqishingizga",
                fontSize = 16.sp,
                textAlign = TextAlign.Center,
                fontWeight = FontWeight.Normal,
                color = Color.Black,
                modifier = Modifier.padding(vertical = 8.dp)
            )
            Row(
                horizontalArrangement = Arrangement.SpaceEvenly,
                modifier = Modifier.fillMaxWidth()
            ) {
                Button(
                    onClick = { onConfirm.invoke() },
                    colors = ButtonDefaults.buttonColors(GreenColor),
                    shape = RoundedCornerShape(12.dp),
                    modifier = Modifier
                        .height(56.dp)
                        .weight(1f)
                        .padding(8.dp)
                ) {
                    Text(text = "Ha", color = Color.White)
                }
                Button(
                    onClick = { onDismiss.invoke() },
                    colors = ButtonDefaults.buttonColors(Color.White),
                    shape = RoundedCornerShape(12.dp),
                    modifier = Modifier
                        .height(56.dp)
                        .weight(1f)
                        .padding(8.dp)
                ) {
                    Text(text = "Yuq", color = Color.Black)
                }
            }
        }
    }

}