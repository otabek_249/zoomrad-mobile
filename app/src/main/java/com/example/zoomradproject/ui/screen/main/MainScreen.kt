package com.example.zoomradproject.ui.screen.main

import android.annotation.SuppressLint
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.navigator.tab.CurrentTab
import cafe.adriel.voyager.navigator.tab.LocalTabNavigator
import cafe.adriel.voyager.navigator.tab.Tab
import cafe.adriel.voyager.navigator.tab.TabDisposable
import cafe.adriel.voyager.navigator.tab.TabNavigator
import com.example.zoomradproject.ui.screen.main.pages.help.HelpPage
import com.example.zoomradproject.ui.screen.main.pages.home.HomePage
import com.example.zoomradproject.ui.screen.main.pages.more.MorePage
import com.example.zoomradproject.ui.screen.main.pages.payment.PaymentPage
import com.example.zoomradproject.ui.screen.main.pages.transfer.TransferPage

import com.example.zoomradproject.ui.theme.BottomColor
import com.example.zoomradproject.ui.theme.GreenColor

class MainScreen : Screen {
    @Composable
    override fun Content() {
        MainContent()
    }
}

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun MainContent() {
    TabNavigator(tab = HomePage, tabDisposable = {
        TabDisposable(
            navigator = it,
            tabs = listOf(HomePage, PaymentPage, TransferPage, HelpPage, MorePage)
        )
    }) {
        Scaffold(content = {
            CurrentTab()
        }, bottomBar = {

            BottomNavigation(
                backgroundColor = BottomColor,
            ) {
                TabNavigatorItem(tab = HomePage)
                TabNavigatorItem(tab = PaymentPage)
                TabNavigatorItem(tab = TransferPage)
                TabNavigatorItem(tab = HelpPage)
                TabNavigatorItem(tab = MorePage)
            }

        })
    }
}

@Composable
fun RowScope.TabNavigatorItem(tab: Tab) {
    val tabNavigator = LocalTabNavigator.current

    BottomNavigationItem(
        modifier = Modifier
            .background(Color(0xFFFEFEFE)),

        selectedContentColor = GreenColor,
        selected = tabNavigator.current == tab, onClick = {
            tabNavigator.current = tab


        }, label = {
            Text(
                text = tab.options.title,
                modifier = Modifier.padding(vertical = 8.dp),
                fontSize = 11.sp,
                color = Color.Black
            )
        }, icon = {
            Icon(
                modifier = Modifier
                    .height(24.dp)
                    .width(24.dp),
                painter = tab.options.icon!!, contentDescription = tab.options.title
            )
        })
}



