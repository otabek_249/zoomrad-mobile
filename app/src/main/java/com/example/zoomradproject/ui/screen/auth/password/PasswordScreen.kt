package com.example.zoomradproject.ui.screen.auth.password

import android.annotation.SuppressLint
import android.content.Context
import android.widget.Toast
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.hilt.getViewModel
import com.example.zoomradproject.R
import com.example.zoomradproject.data.pref.MyPref
import com.example.zoomradproject.ui.screen.auth.password2.Password2Contract
import com.example.zoomradproject.ui.theme.MainBgColor
import com.example.zoomradproject.ui.theme.TextColor
import com.example.zoomradproject.utils.CustomCircleOne
import com.example.zoomradproject.utils.CustomCircleTwo
import com.example.zoomradproject.utils.NumberText
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.orbitmvi.orbit.compose.collectAsState

class PasswordScreen : Screen {
    @Composable
    override fun Content() {
        val model = getViewModel<PasswordViewModel>()
        val uiState = model.collectAsState().value
        PasswordContent(
            uiState,
            model::onEventDispatcher
        )
    }
}

@SuppressLint("SuspiciousIndentation", "CoroutineCreationDuringComposition")
@Composable
fun PasswordContent(
    uiState: PasswordContract.UIState, onEventDispatcher: (PasswordContract.Intent) -> Unit = {}
) {
    val context = LocalContext.current
    var password by remember { mutableStateOf("") }
    var password2 by remember { mutableStateOf("") }
    val myPref = MyPref.getInstance()
    val listBoolean = remember { mutableStateListOf<Boolean>().apply { repeat(4) { this.add(false) } } }
    var index by remember { mutableIntStateOf(0) }
    var index2 by remember { mutableIntStateOf(0) }
    var boolean by remember { mutableStateOf(true) }

    if (password.length == 4 && password == password2) {
        myPref.setPassword(password)
        onEventDispatcher.invoke(PasswordContract.Intent.OpenMainScreen)
    }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(MainBgColor)
    ) {
        Box(
            modifier = Modifier
                .weight(0.3f)
                .fillMaxWidth(),
            contentAlignment = Alignment.BottomCenter
        ) {
            Image(
                painter = painterResource(id = R.drawable.ic_zoomrad),
                contentDescription = "",
                modifier = Modifier
                    .height(100.dp)
                    .width(100.dp)
            )
        }

        Column(
            modifier = Modifier
                .weight(1f)
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Spacer(modifier = Modifier.height(48.dp))

            Text(
                text = "Pin Kod ni tasdiqlang",
                fontSize = 18.sp,
                color = TextColor,
                fontFamily = FontFamily(Font(R.font.nata_bold)),
                fontWeight = FontWeight(600)
            )

            Spacer(modifier = Modifier.height(24.dp))
            Row(
                modifier = Modifier
                    .height(32.dp)
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {
                for (i in 0 until 4) {
                    if (i < index) {
                        CustomCircleTwo()
                    } else {
                        CustomCircleOne()
                    }
                }
            }
            Spacer(modifier = Modifier.weight(1f))
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(4f)
            ) {
                Row(
                    modifier = Modifier
                        .padding(horizontal = 48.dp)
                        .fillMaxWidth()
                        .weight(1f),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    NumberText(number = "1") {
                        if (index >= 4) return@NumberText
                        index += 1
                        password += "1"
                        Toast.makeText(context, "1", Toast.LENGTH_SHORT).show()
                        boolean = false
                        println("Password: $password")
                    }
                    NumberText(number = "2") {
                        if (index >= 4) return@NumberText
                        index += 1
                        password += "2"
                        println("Password: $password")
                    }
                    NumberText(number = "3") {
                        if (index >= 4) return@NumberText
                        index += 1
                        password += "3"
                        println("Password: $password")
                    }
                }
                Row(
                    modifier = Modifier
                        .padding(horizontal = 48.dp)
                        .fillMaxWidth()
                        .weight(1f),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    NumberText(number = "4") {
                        if (index >= 4) return@NumberText
                        index += 1
                        password += "4"
                        println("Password: $password")
                    }
                    NumberText(number = "5") {
                        if (index >= 4) return@NumberText
                        index += 1
                        password += "5"
                        println("Password: $password")
                    }
                    NumberText(number = "6") {
                        if (index >= 4) return@NumberText
                        index += 1
                        password += "6"
                        println("Password: $password")
                    }
                }
                Row(
                    modifier = Modifier
                        .padding(horizontal = 48.dp)
                        .fillMaxWidth()
                        .weight(1f),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    NumberText(number = "7") {
                        if (index >= 4) return@NumberText
                        index += 1
                        password += "7"
                        println("Password: $password")
                    }
                    NumberText(number = "8") {
                        if (index >= 4) return@NumberText
                        index += 1
                        password += "8"
                        println("Password: $password")
                    }
                    NumberText(number = "9") {
                        if (index >= 4) return@NumberText
                        index += 1
                        password += "9"
                        println("Password: $password")
                    }
                }
                Row(
                    modifier = Modifier
                        .padding(horizontal = 48.dp)
                        .fillMaxWidth()
                        .weight(1f),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Spacer(
                        modifier = Modifier
                            .height(48.dp)
                            .width(48.dp)
                    )

                    NumberText(number = "0") {
                        if (index >= 4) return@NumberText
                        index += 1
                        listBoolean[index] = true
                        password += "0"
                        println("Password: $password")
                    }
                    Image(painter = painterResource(id = R.drawable.ic_number_delete),
                        contentDescription = "",
                        modifier = Modifier
                            .clickable {
                                if (password.isEmpty()) return@clickable
                                index--
                                password = password.substring(0, password.length - 1)
                                println("Password after delete: $password")
                            }
                            .height(48.dp)
                            .width(48.dp)
                            .padding(8.dp))
                }
                Spacer(modifier = Modifier.height(16.dp))
            }
        }
    }

    val coroutineScope = rememberCoroutineScope()
    coroutineScope.launch {
        delay(500L)
    }

    if (password.length == 4) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .background(MainBgColor)
        ) {
            Box(
                modifier = Modifier
                    .weight(0.3f)
                    .fillMaxWidth(),
                contentAlignment = Alignment.BottomCenter
            ) {
                Image(
                    painter = painterResource(id = R.drawable.ic_zoomrad),
                    contentDescription = "",
                    modifier = Modifier
                        .height(100.dp)
                        .width(100.dp)
                )
            }

            Column(
                modifier = Modifier
                    .weight(1f)
                    .fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Spacer(modifier = Modifier.height(48.dp))

                Text(
                    text = "Pin kodni qayta kiriting",
                    fontSize = 18.sp,
                    color = TextColor,
                    fontFamily = FontFamily(Font(R.font.nata_bold)),
                    fontWeight = FontWeight(600)
                )

                Spacer(modifier = Modifier.height(24.dp))
                Row(
                    modifier = Modifier
                        .height(32.dp)
                        .fillMaxWidth(),
                    horizontalArrangement = Arrangement.Center,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    for (i in 0 until 4) {
                        if (i < index2) {
                            CustomCircleTwo()
                        } else {
                            CustomCircleOne()
                        }
                    }
                }
                Spacer(modifier = Modifier.weight(1f))
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .weight(4f)
                ) {
                    Row(
                        modifier = Modifier
                            .padding(horizontal = 48.dp)
                            .fillMaxWidth()
                            .weight(1f),
                        horizontalArrangement = Arrangement.SpaceBetween,
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        NumberText(number = "1") {
                            if (index2 >= 4) return@NumberText
                            index2 += 1
                            password2 += "1"
                            Toast.makeText(context, "1", Toast.LENGTH_SHORT).show()
                            boolean = false
                            println("Password2: $password2")
                        }
                        NumberText(number = "2") {
                            if (index2 >= 4) return@NumberText
                            index2 += 1
                            password2 += "2"
                            println("Password2: $password2")
                        }
                        NumberText(number = "3") {
                            if (index2 >= 4) return@NumberText
                            index2 += 1
                            password2 += "3"
                            println("Password2: $password2")
                        }
                    }
                    Row(
                        modifier = Modifier
                            .padding(horizontal = 48.dp)
                            .fillMaxWidth()
                            .weight(1f),
                        horizontalArrangement = Arrangement.SpaceBetween,
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        NumberText(number = "4") {
                            if (index2 >= 4) return@NumberText
                            index2 += 1
                            password2 += "4"
                            println("Password2: $password2")
                        }
                        NumberText(number = "5") {
                            if (index2 >= 4) return@NumberText
                            index2 += 1
                            password2 += "5"
                            println("Password2: $password2")
                        }
                        NumberText(number = "6") {
                            if (index2 >= 4) return@NumberText
                            index2 += 1
                            password2 += "6"
                            println("Password2: $password2")
                        }
                    }
                    Row(
                        modifier = Modifier
                            .padding(horizontal = 48.dp)
                            .fillMaxWidth()
                            .weight(1f),
                        horizontalArrangement = Arrangement.SpaceBetween,
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        NumberText(number = "7") {
                            if (index2 >= 4) return@NumberText
                            index2 += 1
                            password2 += "7"
                            println("Password2: $password2")
                        }
                        NumberText(number = "8") {
                            if (index2 >= 4) return@NumberText
                            index2 += 1
                            password2 += "8"
                            println("Password2: $password2")
                        }
                        NumberText(number = "9") {
                            if (index2 >= 4) return@NumberText
                            index2 += 1
                            password2 += "9"
                            println("Password2: $password2")
                        }
                    }
                    Row(
                        modifier = Modifier
                            .padding(horizontal = 48.dp)
                            .fillMaxWidth()
                            .weight(1f),
                        horizontalArrangement = Arrangement.SpaceBetween,
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Spacer(
                            modifier = Modifier
                                .height(48.dp)
                                .width(48.dp)
                        )

                        NumberText(number = "0") {
                            if (index2 >= 4) return@NumberText
                            index2 += 1
                            listBoolean[index2] = true
                            password2 += "0"
                            println("Password2: $password2")
                        }
                        Image(painter = painterResource(id = R.drawable.ic_number_delete),
                            contentDescription = "",
                            modifier = Modifier
                                .clickable {
                                    if (password2.isEmpty()) return@clickable
                                    index2--
                                    password2 = password2.substring(0, password2.length - 1)
                                    println("Password2 after delete: $password2")
                                }
                                .height(48.dp)
                                .width(48.dp)
                                .padding(8.dp))
                    }
                    Spacer(modifier = Modifier.height(16.dp))
                }
            }
        }
    }
}

private fun authenticateBiometric(context: Context) {
    val biometricManager = BiometricManager.from(context)

    if (biometricManager.canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_STRONG) == BiometricManager.BIOMETRIC_SUCCESS) {
        val executor = ContextCompat.getMainExecutor(context)
        val biometricPrompt = BiometricPrompt(context as FragmentActivity,
            executor, object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationError(
                    errorCode: Int, errString: CharSequence
                ) {
                    super.onAuthenticationError(errorCode, errString)
                    Toast.makeText(context, "Authentication error: $errString", Toast.LENGTH_SHORT).show()
                }

                override fun onAuthenticationSucceeded(
                    result: BiometricPrompt.AuthenticationResult
                ) {
                    super.onAuthenticationSucceeded(result)
                    Toast.makeText(context, "Authentication succeeded!", Toast.LENGTH_SHORT).show()
                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    Toast.makeText(context, "Authentication failed", Toast.LENGTH_SHORT).show()
                }
            })

        val promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle("Biometric login for my app")
            .setSubtitle("Log in using your biometric credential")
            .setNegativeButtonText("Use account password")
            .build()

        biometricPrompt.authenticate(promptInfo)
    }
}
