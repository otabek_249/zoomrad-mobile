package com.example.zoomradproject.ui.screen.main.pages.more

import androidx.lifecycle.ViewModel
import com.example.zoomradproject.data.pref.MyPref
import com.example.zoomradproject.repository.main.impl.AppRepositoryImpl
import com.example.zoomradproject.ui.screen.main.pages.home.HomeContract
import dagger.hilt.android.lifecycle.HiltViewModel
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.syntax.simple.postSideEffect
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject


@HiltViewModel
class MoreViewModelImpl  @Inject
constructor(
    private val  repositoryImpl: AppRepositoryImpl,
    private val direction: MoreDirection
):MoreContract.MoreViewModel,ViewModel(){

    val  myPref = MyPref.getInstance()


    override val container =container<MoreContract.UIState,MoreContract.SideEffect>(MoreContract.UIState())


    override fun onEventDispatcher(intent: MoreContract.Intent) = intent {
        when(intent){
           is   MoreContract.Intent.OpenDrawer->{
                direction.openDrawer(intent.index)
            }
            MoreContract.Intent.DialogButtonYes->{
                myPref.setAccessToke("otabek")
                myPref.setRefreshToken("otabek")
                myPref.setCardIndex(0)
                myPref.setPassword("")
                direction.logOut()
            }

            MoreContract.Intent.OpenDialog->{
                postSideEffect(MoreContract.SideEffect.OpenDialog)
            }
        }
    }


}