package com.example.zoomradproject.ui.screen.auth.verify

import com.example.zoomradproject.ui.screen.auth.password.PasswordScreen

import com.example.zoomradproject.utils.navigation.AppNavigator
import javax.inject.Inject


interface VerifyDirection {
    suspend fun openPasswordScreen()
}

class VerifyDirectionImpl @Inject constructor(
    private val navigator: AppNavigator
) : VerifyDirection {
    override suspend fun openPasswordScreen() {
        navigator.replaceAll(PasswordScreen())
    }
}