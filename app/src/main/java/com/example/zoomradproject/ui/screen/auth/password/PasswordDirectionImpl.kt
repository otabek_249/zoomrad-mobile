package com.example.zoomradproject.ui.screen.auth.password

import com.example.zoomradproject.ui.screen.main.MainScreen
import com.example.zoomradproject.utils.navigation.AppNavigator
import javax.inject.Inject


interface PasswordDirection {
    suspend fun openMainScreen()
}

class PasswordDirectionImpl @Inject constructor(
    private val appNavigator: AppNavigator
) : PasswordDirection {
    override suspend fun openMainScreen() {
        appNavigator.replaceAll(MainScreen())
    }

}