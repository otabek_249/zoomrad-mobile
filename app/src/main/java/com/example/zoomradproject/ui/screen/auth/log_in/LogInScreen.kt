package com.example.zoomradproject.ui.screen.auth.log_in

import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.hilt.getViewModel
import com.example.zoomradproject.R
import com.example.zoomradproject.data.remote.request.LogInRequest
import com.example.zoomradproject.ui.theme.DefaultButtonColor
import com.example.zoomradproject.ui.theme.EditTextBgColor
import com.example.zoomradproject.ui.theme.GreenColor
import com.example.zoomradproject.ui.theme.MainBgColor
import org.orbitmvi.orbit.compose.collectAsState
import org.orbitmvi.orbit.compose.collectSideEffect
import kotlin.String
import kotlin.String as String1

class LogInScreen : Screen {
    @Composable
    override fun Content() {
        val model: LogInContract.LogInViewModel = getViewModel<LogInViewModelImpl>()
        val uiState = model.collectAsState().value


        val context = LocalContext.current
        model.collectSideEffect {
            when(it){
                is LogInContract.SideEffect.TextToast->{
                  Toast.makeText(context,it.msg,Toast.LENGTH_SHORT).show()
                }
            }
        }
        LogInContent(uiState, model::onEventDispatcher)
    }
}

@Composable
fun LogInContent(
    uiState: LogInContract.UIState,
    onEventDispatcher: (LogInContract.Intent) -> Unit = {}
) {
    var phoneNumber by remember { mutableStateOf("") }
    val password = remember { mutableStateOf("") }

    var boolean by remember {
        mutableStateOf(false)
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MainBgColor),
        horizontalAlignment = Alignment.CenterHorizontally
    )
    {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .weight(0.3f),
            contentAlignment = Alignment.BottomCenter
        ) {
            Image(
                painter = painterResource(id = R.drawable.ic_zoomrad),
                contentDescription = "",
                modifier = Modifier
                    .height(100.dp)
                    .width(100.dp)
            )

        }
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            Spacer(modifier = Modifier.height(32.dp))
            OutlinedTextField(
                modifier = Modifier
                    .padding(start = 24.dp, end = 24.dp)

                    .height(52.dp)
                    .fillMaxWidth(),
                shape = RoundedCornerShape(12.dp),
                textStyle = TextStyle(
                    fontFamily = FontFamily(Font(R.font.nata_regular)),
                    fontSize = 18.sp,
                    textAlign = TextAlign.Start,
                ),
                singleLine = true,
                value = phoneNumber,
                onValueChange = {


                    if (it.length <= 13) {
                        phoneNumber = it
                        boolean = phoneNumber.startsWith("+998") && phoneNumber.length == 13 && password.value.length >= 6
                    }
                },


                placeholder = {
                    Text(
                        text = stringResource(R.string.enter_phonenumber)
                    )
                },

                leadingIcon = {
                    Image(
                        modifier = Modifier
                            .height(24.dp)
                            .width(24.dp)
                            .padding(1.dp),
                        painter = painterResource(id = R.drawable.ic_telephone),
                        contentDescription ="" )
                },


              /*  isError = uiState.state,*/

                colors = TextFieldDefaults.textFieldColors(
                    errorCursorColor = Color.Red,

                    backgroundColor = EditTextBgColor,
                    cursorColor = Color.Black,
                    focusedIndicatorColor = GreenColor,
                    unfocusedIndicatorColor = EditTextBgColor,
                    placeholderColor = Color(0xFFB2B2B2)
                ),
                maxLines = 1,

                )
            Spacer(modifier = Modifier.height(12.dp))

            OutlinedTextField(

                modifier = Modifier
                    .padding(start = 24.dp, end = 24.dp)

                    .height(52.dp)
                    .fillMaxWidth(),
                shape = RoundedCornerShape(12.dp),
                singleLine = true,
                textStyle = TextStyle(
                    fontFamily = FontFamily(Font(R.font.nata_regular)),
                    fontSize = 18.sp,
                    textAlign = TextAlign.Start,
                ),


                value = password.value,
                onValueChange = {
                    boolean = phoneNumber.startsWith("+998") && phoneNumber.length >= 13 && password.value.length >= 6
                    if (it.length<=14 ){
                        password.value = it
                    }




                },
                placeholder = {
                    Text(
                        text = stringResource(R.string.enter_password)
                    )
                },
                leadingIcon = {
                    Image(
                        modifier = Modifier
                            .height(24.dp)
                            .width(24.dp),
                        painter = painterResource(id = R.drawable.ic_padlock),
                        contentDescription ="",
                        colorFilter = ColorFilter.tint(Color.Black))
                },




                colors = TextFieldDefaults.textFieldColors(
                    errorLabelColor = Color.Red,
                    backgroundColor = EditTextBgColor,
                    focusedIndicatorColor = GreenColor,
                    cursorColor = Color.Black,
                    unfocusedIndicatorColor = EditTextBgColor,
                    placeholderColor = Color(0xFFB2B2B2)
                ),
                maxLines = 1,

                )

            Button(
                enabled = boolean,
                shape = RoundedCornerShape(12.dp),
                modifier = Modifier
                    .padding(start = 24.dp, end = 24.dp, top = 16.dp)
                    .height(48.dp)
                    .fillMaxWidth(),
                onClick = {
                    onEventDispatcher.invoke(LogInContract.Intent.PhoneNumber(phoneNumber))
                    onEventDispatcher.invoke(
                        LogInContract.Intent.ClickButton(
                            LogInRequest(
                                phone = phoneNumber,
                                password = password.value
                            )
                        )
                    )
                },
                colors = ButtonDefaults.buttonColors(

                    containerColor = GreenColor,
                    disabledContainerColor = DefaultButtonColor


                )
            ) {
                Text("Save")

            }
            Row {
                Text(modifier = Modifier
                    .padding(top = 16.dp, end = 4.dp),
                    text = stringResource(R.string.dont_have_an_account),
                    fontWeight = FontWeight(700),
                    fontFamily = FontFamily(Font(R.font.nata_semibold)),
                    fontSize = 14.sp,
                    color = Color.Black)

                Text(modifier = Modifier
                    .clickable {
                        onEventDispatcher.invoke(LogInContract.Intent.ClickTextOpenSignUpScreen)
                    }
                    .padding(top = 14.dp),
                    text = stringResource(R.string.sign_up),
                    fontFamily = FontFamily(Font(R.font.nata_semibold)),
                    fontWeight = FontWeight(700),
                    fontSize = 16.sp,
                    color = GreenColor)

            }

        }


    }
}



@Composable
@Preview
fun LogInPreview() {
    LogInContent(LogInContract.UIState(""))
}

fun String1.formatPhoneNumber2(): String1 {
    val formattedNumber = StringBuilder()
    for ((index, char) in this.withIndex()) {
        when (index) {
            0 -> formattedNumber.append("+998 ")
            1, 2 -> formattedNumber.append("($char)")
            3 -> formattedNumber.append(" $char")
            4, 5, 6 -> formattedNumber.append(char)
            7 -> formattedNumber.append("-$char")
            8 -> formattedNumber.append("-$char")
            9 -> formattedNumber.append("-$char")
            else -> formattedNumber.append(char)
        }
    }
    return formattedNumber.toString()
}
fun String1.formatPhoneNumber(): String1 {
    if(this.length<4) return this

    val countryCode = this.substring(0, 4)
    val areaCode = this.substring(4, 6)
    val firstPart = this.substring(6, 9)
    val secondPart =this.substring(9,11)
    val thirdPart = this.substring(11)

    return "$countryCode ($areaCode)-$firstPart-$secondPart-$thirdPart"
}

@Composable
fun ErrorMessage(errorMessage: String1) {
    Row(
        modifier = Modifier
            .padding(start = 24.dp, end = 24.dp, top = 8.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Icon(
            painter = painterResource(id = R.drawable.ic_error),
            contentDescription = null,
            modifier = Modifier.size(16.dp),
            tint = Color.Red
        )
        Spacer(modifier = Modifier.width(8.dp))
        Text(
            text = errorMessage,
            style = TextStyle(
                color = Color.Red,
                fontSize = 14.sp
            )
        )
    }
}
/*

fun main() {
    val phoneNumber = "+998930720405"
    val formattedNumber = formatPhoneNumber(phoneNumber)
    println("Formatted number: $formattedNumber")
}
*/
fun String.phoneFormat():String{
    if (this.length == 13 && this.startsWith("+")) {
        return this.substring(0, 4) + " " +
                this.substring(4, 6) + " " +
                this.substring(6, 9) + " " +
                this.substring(9, 11) + " " +
                this.substring(11)
    }
    return ""
}