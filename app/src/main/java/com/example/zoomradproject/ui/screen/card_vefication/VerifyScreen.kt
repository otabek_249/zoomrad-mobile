package com.example.zoomradproject.ui.screen.card_vefication

import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.hilt.getViewModel
import com.example.zoomradproject.R
import com.example.zoomradproject.ui.theme.DefaultButtonColor
import com.example.zoomradproject.ui.theme.EditTextBgColor
import com.example.zoomradproject.ui.theme.GreenColor
import com.example.zoomradproject.ui.theme.MainBgColor
import org.orbitmvi.orbit.compose.collectAsState
import org.orbitmvi.orbit.compose.collectSideEffect

class VerifyCardScreen(val token: String) : Screen {
    @Composable
    override fun Content() {

        val model: VerifyCardContract.VerifyCardModel = getViewModel<VerifyCardViewModel>()
        val uiState = model.collectAsState().value
        val context = LocalContext.current

        model.collectSideEffect {
            when (it) {
                is VerifyCardContract.SideEffect.ToastSideEffect -> {
                    Toast.makeText(context,it.msg,Toast.LENGTH_SHORT).show()
                }
            }
        }
        VerifyContent(uiState, model::onEventDispatcher, token = token)
    }
}


@Composable
fun VerifyContent(
    uiState: VerifyCardContract.UIState,
    onEventDispatcher: (VerifyCardContract.Intent) -> Unit = {},
    token: String
) {
    val password = remember { mutableStateOf("") }
    val max = 5
    onEventDispatcher.invoke(VerifyCardContract.Intent.First)

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MainBgColor),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        Box(
            modifier = Modifier
                .fillMaxWidth()
                .weight(0.3f),
            contentAlignment = Alignment.BottomCenter
        ) {

            Image(
                painter = painterResource(id = R.drawable.ic_zoomrad),
                contentDescription = "",
                modifier = Modifier
                    .height(100.dp)
                    .width(100.dp)
            )

        }
        Spacer(modifier = Modifier.height(32.dp))



        Column(
            modifier = Modifier
                .weight(1f)
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally,
        )
        {
            Text(
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
                    .padding(horizontal = 24.dp),
                text = stringResource(id = R.string.send_code_for_number),
                fontSize = 16.sp,
                textAlign = TextAlign.Center,
                overflow = TextOverflow.Ellipsis,
                fontWeight = FontWeight(400)
            )
            Spacer(modifier = Modifier.height(8.dp))
            Text(
                text = uiState.phone,
                fontSize = 16.sp
            )
            Box(
                modifier = Modifier
                    .padding(24.dp)
                    .fillMaxWidth()
                    .height(48.dp)
                    .background(color = EditTextBgColor, RoundedCornerShape(12.dp)),
                contentAlignment = Alignment.Center

            ) {
                OutlinedTextField(
                    modifier = Modifier
                        .padding(start = 24.dp, end = 24.dp)
                        .width(100.dp)
                        .height(48.dp),
                    shape = RoundedCornerShape(12.dp),


                    value = password.value,
                    onValueChange = {
                        if (password.value.length <= max) {

                        }
                        password.value = it

                    },


                    colors = TextFieldDefaults.textFieldColors(
                        backgroundColor = EditTextBgColor,
                        focusedIndicatorColor = EditTextBgColor,
                        cursorColor = Color.Black,
                        unfocusedIndicatorColor = EditTextBgColor,
                        placeholderColor = Color(0xFFB2B2B2)
                    ),
                    maxLines = 1,

                    )
            }

            Button(
                enabled = password.value.length > 5,
                shape = RoundedCornerShape(12.dp),
                modifier = Modifier
                    .padding(start = 24.dp, end = 24.dp, top = 8.dp)
                    .height(48.dp)
                    .fillMaxWidth(),
                onClick = {
                    onEventDispatcher.invoke(
                        VerifyCardContract.Intent.ButtonClickSend(
                            code = password.value,
                            token = token
                        )
                    )
                },
                colors = ButtonDefaults.buttonColors(

                    containerColor = GreenColor,
                    disabledContainerColor = DefaultButtonColor


                )
            ) {
                Text(stringResource(R.string.send))

            }

        }


    }

}


@Preview
@Composable
fun VerifyPreview() {
    VerifyContent(VerifyCardContract.UIState(), token = "", onEventDispatcher = {})

}