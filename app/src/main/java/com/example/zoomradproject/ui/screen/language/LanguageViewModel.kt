package com.example.zoomradproject.ui.screen.language

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.syntax.simple.postSideEffect
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject


@HiltViewModel
class LanguageViewModel @Inject constructor(
    private val direction: LanguageDirection
) : LanguageContract.LanguageModel, ViewModel() {


    override val container =
        container<LanguageContract.UIState, LanguageContract.SideEffect>(LanguageContract.UIState.Display())

    override fun onEventDispatcher(intent: LanguageContract.Intent) = intent {
        when (intent) {
            LanguageContract.Intent.ClickEngButton -> {
                direction.openSignUpScreen()
            }

            LanguageContract.Intent.ClickRusButton -> {
                direction.openSignUpScreen()
            }

            LanguageContract.Intent.ClickUzButton -> {
                direction.openSignUpScreen()
            }

            is LanguageContract.Intent.ShowToast -> {
                postSideEffect(LanguageContract.SideEffect.ShowToast(intent.text))
            }

        }
    }
}