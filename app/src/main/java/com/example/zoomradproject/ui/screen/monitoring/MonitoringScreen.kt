package com.example.zoomradproject.ui.screen.monitoring

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableDoubleStateOf
import androidx.compose.runtime.mutableLongStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.collectAsLazyPagingItems
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.hilt.getViewModel
import com.example.zoomradproject.R
import com.example.zoomradproject.data.model.Transaction
import com.example.zoomradproject.ui.screen.add_card.formatToCard
import com.example.zoomradproject.ui.screen.monitoring.component.IncomeSmallItem
import com.example.zoomradproject.ui.screen.monitoring.component.ItemMonitoring
import com.example.zoomradproject.ui.screen.monitoring.component.OutcomeSmallItem
import com.example.zoomradproject.ui.theme.GreenColor
import com.example.zoomradproject.ui.theme.MainBgColor
import com.example.zoomradproject.utils.myLog
import kotlinx.coroutines.launch
import org.orbitmvi.orbit.compose.collectAsState

class MonitoringScreen : Screen {
    @Composable
    override fun Content() {
        val viewModel = getViewModel<MonitoringViewModelImpl>()
        val uiState = viewModel.collectAsState()
        val response = viewModel.transaction.collectAsLazyPagingItems()
        MonitoringContent(uiState = uiState, viewModel::onEventDispatcher, response)
    }
}


@Composable
fun MonitoringContent(
    uiState: State<MonitoringContract.UIState>,
    onEventDispatcher: (MonitoringContract.Intent) -> Unit,
    repo: LazyPagingItems<Transaction>
) {
    var income by remember {
        mutableDoubleStateOf(0.0)
    }
    var outcome by remember {
        mutableDoubleStateOf(0.0)
    }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MainBgColor)

    ) {
        Row(
            modifier = Modifier
                .padding(start = 16.dp, end = 16.dp, top = 24.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Image(
                modifier = Modifier
                    .clickable {

                        onEventDispatcher.invoke(MonitoringContract.Intent.Back)
                    }
                    .height(24.dp)
                    .width(24.dp)
                    .padding(3.dp),
                painter = painterResource(id = R.drawable.ic_back_green),
                contentDescription = "",
            )
            Spacer(modifier = Modifier.weight(1f))
            Text(
                text = stringResource(R.string.more_text),
                fontSize = 18.sp,
                color = Color(0xFFB7B7B7),
                fontFamily = FontFamily(Font(R.font.nata_bold)),
                fontWeight = FontWeight(300)
            )
            Spacer(modifier = Modifier.weight(1f))


            Image(
                modifier = Modifier
                    .width(24.dp)
                    .height(24.dp)
                    .padding(1.dp),
                painter = painterResource(id = R.drawable.ic_menu_list),
                contentDescription = ""
            )

        }
        Row(
            modifier = Modifier
                .padding(start = 16.dp, end = 16.dp, top = 16.dp)

        ) {

            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                IncomeSmallItem()
                Column(
                    modifier = Modifier
                        .padding(start = 8.dp)
                ) {
                    Text(
                        color = GreenColor,
                        fontFamily = FontFamily(Font(R.font.nata_bold)),
                        fontSize = 18.sp,
                        text = "+$income"
                    )
                    Spacer(modifier = Modifier.height(8.dp))
                    Text(text = "Tushumlar")
                }
            }
            Spacer(modifier = Modifier.weight(1f))
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                OutcomeSmallItem()
                Column(
                    modifier = Modifier
                        .padding(start = 8.dp)
                ) {
                    Text(
                        color = Color.Red,
                        fontFamily = FontFamily(Font(R.font.nata_bold)),
                        fontSize = 18.sp,
                        text = "-$outcome"
                    )
                    Spacer(modifier = Modifier.height(8.dp))
                    Text(text = "Chiqimlarr")
                }
            }

        }

        LazyColumn(
            modifier = Modifier
                .fillMaxWidth()
        ) {


            items(repo.itemCount) {
                val data = repo[it]
                data?.let { it1 ->
                    /*if(it1.type=="income"){
                       income+=it1.amount
                    }
                    else{
                        outcome+=it1.amount
                    }*/
                    ItemMonitoring(it1) }
            }

        }

    }
}


@Preview
@Composable
fun MonitoringPreview() {
    /*  MonitoringContent(remember { mutableStateOf(MonitoringContract.UIState())
      }){

      }*/
}