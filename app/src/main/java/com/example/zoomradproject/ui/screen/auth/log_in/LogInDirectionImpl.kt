package com.example.zoomradproject.ui.screen.auth.log_in

import com.example.zoomradproject.ui.screen.auth.sign_up.SignUpScreen
import com.example.zoomradproject.ui.screen.auth.verify.VerifyScreen
import com.example.zoomradproject.utils.navigation.AppNavigator
import javax.inject.Inject


interface LogInDirection {
    suspend fun openVerifyScreen()

    suspend fun openSignUpScreen()
}

class LogInDirectionImpl @Inject constructor(
    private val appNavigator: AppNavigator
) : LogInDirection {
    override suspend fun openVerifyScreen() {
        appNavigator.navigateTo(VerifyScreen())
    }

    override suspend fun openSignUpScreen() {
        appNavigator.replace(SignUpScreen())
    }

}