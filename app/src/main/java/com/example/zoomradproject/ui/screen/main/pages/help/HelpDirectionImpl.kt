package com.example.zoomradproject.ui.screen.main.pages.help

import cafe.adriel.voyager.core.screen.Screen
import com.example.zoomradproject.ui.screen.language.LanguageScreen
import com.example.zoomradproject.ui.screen.map.branch.BranchScreen
import com.example.zoomradproject.ui.screen.profile.ProfileScreen
import com.example.zoomradproject.ui.screen.settings.main.SettingsScreen
import com.example.zoomradproject.utils.navigation.AppNavigator
import javax.inject.Inject

interface HelpDirection{
    suspend fun screenMap()
    suspend fun logOut()

    suspend fun openDrawer(index:Int)
}
class HelpDirectionImpl   @Inject constructor(
    private val navigator: AppNavigator
):HelpDirection{
    override suspend fun logOut() {
        navigator.replaceAll(LanguageScreen())
    }

    override suspend fun openDrawer(index: Int) {
        when(index) {
            0 -> {
                navigator.navigateTo(ProfileScreen())
            }

            1 -> {
                // Settings
                navigator.navigateTo(SettingsScreen())
            }

            2 -> {
                // operatsiya tarixi
            }

            3 -> {
                // monitoring
            }

            4 -> {
                // Mening arizalarim
            }

            5 -> {
                // Yordam
                navigator.navigateTo(HelpPage as Screen)
            }

            6 -> {
                // programming share
            }

            else -> {
                // exit
            }
        }
    }

    override suspend fun screenMap() {
        navigator.navigateTo(BranchScreen())
    }
}